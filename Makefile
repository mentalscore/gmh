# ==== Makefile to build docker image

# ==== Variables
NAME := mentalscore

.PHONY: result
build: docker.nix
	nix-build $^

.PHONY: load
load: build
	docker load < result

.PHONY: run
run: load
	docker run -t $(NAME)

.PHONY: exec
exec: 
	docker exec -t $(docker ps | awk '/mentalscore/ {print $NF}') echo



.PHONY=watch
watch: 
	@find -name '*.nix'  | entr $(MAKE)


.PHONY: clean
clean:
	-rm result
