{ pkgs ? import <nixpkgs> {} }:

rec {
  nginx = let
    nginxPort = "80";
    nginxConf = pkgs.writeText "nginx.conf" ''
      user nginx nginx;
      daemon off;
      error_log /dev/stdout info;
      pid /dev/null;
      events {}
      http {
        access_log /dev/stdout;
        server {
          listen 80;
          index index.html;
          location / {
            root ${nginxWebRoot};
          }
        }
      }
    '';
    nginxWebRoot = pkgs.writeTextDir "index.html" ''
      <html><body><h1>Hello from NGINX</h1></body></html>
    '';
  in
    pkgs.dockerTools.buildImage {
      name = "mentalscore";
      tag = "latest";
      contents = [pkgs.nginx];

      extraCommands = ''
      # nginx still tries to read this directory even if error_log
      # directive is specifying another file :/
      mkdir -p var/log/nginx
      mkdir -p var/cache/nginx
    '';
      runAsRoot = ''
          #!${pkgs.stdenv.shell}
          ${pkgs.dockerTools.shadowSetup}
          groupadd --system nginx
          useradd --system --gid nginx nginx
        '';

      config = {
        Cmd = [ "nginx" "-c" nginxConf ];
        ExposedPorts = {
          "80/tcp" = {
            "0.0.0.0" = "80";
          };
          "443/tcp" = {};
        };
      };
    };
}
