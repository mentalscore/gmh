{ nixpkgs ? import <nixpkgs> {} }:
let
  inherit (nixpkgs) pkgs;
  inherit (pkgs) haskellPackages;

  project = import backend/release.nix;

in
pkgs.stdenv.mkDerivation {
  name = "shell";
  buildInputs =
    # project.env.nativeBuildInputs ++
    [
    pkgs.elmPackages.elm
    pkgs.nodePackages.live-server
    pkgs.nodePackages.uglify-js
    pkgs.git
    pkgs.nodejs
    pkgs.ghcid
    pkgs.zlib.dev
    pkgs.haskell.compiler.ghc8107
    haskellPackages.postgresql-libpq
    haskellPackages.cabal-install
    haskellPackages.ormolu
  ];
}
