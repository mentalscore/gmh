{

      "krisajenkins/remotedata" = {
        sha256 = "0m5bk0qhsjv14vajqrkph386696pnhj5rn51kgma8lwyvvx9ihw1";
        version = "6.0.1";
      };

      "lattyware/elm-fontawesome" = {
        sha256 = "14lgk64168dp5sbrv0k3hlqkp3r5d68qmvjzfvkb6kgx1sz9hah6";
        version = "4.0.0";
      };

      "mdgriffith/elm-ui" = {
        sha256 = "0ffcqv4a4ad400hwp824m3qq4jy82cqp5ghmhp1m0q7n004z6kgv";
        version = "1.1.8";
      };

      "elm/json" = {
        sha256 = "0kjwrz195z84kwywaxhhlnpl3p251qlbm5iz6byd6jky2crmyqyh";
        version = "1.1.3";
      };

      "elm/html" = {
        sha256 = "1n3gpzmpqqdsldys4ipgyl1zacn0kbpc3g4v3hdpiyfjlgh8bf3k";
        version = "1.0.0";
      };

      "panthershark/email-parser" = {
        sha256 = "1fx4839hyrd8r0z3jri25ld83hf73r4yrm315h6xma07ah2h50y3";
        version = "1.0.2";
      };

      "elm-community/random-extra" = {
        sha256 = "1dg2nz77w2cvp16xazbdsxkkw0xc9ycqpkd032faqdyky6gmz9g6";
        version = "3.1.0";
      };

      "elm/svg" = {
        sha256 = "1cwcj73p61q45wqwgqvrvz3aypjyy3fw732xyxdyj6s256hwkn0k";
        version = "1.0.1";
      };

      "elm/browser" = {
        sha256 = "0nagb9ajacxbbg985r4k9h0jadqpp0gp84nm94kcgbr5sf8i9x13";
        version = "1.0.2";
      };

      "elm-community/string-extra" = {
        sha256 = "014l3lkglaniizwvr5pqbi4z7bb0piq0pp3fdifyd4rdp53ac23f";
        version = "4.0.1";
      };

      "elm/core" = {
        sha256 = "19w0iisdd66ywjayyga4kv2p1v9rxzqjaxhckp8ni6n8i0fb2dvf";
        version = "1.0.5";
      };

      "elm/url" = {
        sha256 = "0av8x5syid40sgpl5vd7pry2rq0q4pga28b4yykn9gd9v12rs3l4";
        version = "1.0.0";
      };

      "elm-community/list-extra" = {
        sha256 = "1rvr1c8cfb3dwf3li17l9ziax6d1fshkliasspnw6rviva38lw34";
        version = "8.2.4";
      };

      "elm/random" = {
        sha256 = "138n2455wdjwa657w6sjq18wx2r0k60ibpc4frhbqr50sncxrfdl";
        version = "1.0.0";
      };

      "elm/http" = {
        sha256 = "008bs76mnp48b4dw8qwjj4fyvzbxvlrl4xpa2qh1gg2kfwyw56v1";
        version = "2.0.0";
      };

      "elm/time" = {
        sha256 = "0vch7i86vn0x8b850w1p69vplll1bnbkp8s383z7pinyg94cm2z1";
        version = "1.0.0";
      };

      "NoRedInk/elm-json-decode-pipeline" = {
        sha256 = "0y25xn0yx1q2xlg1yx1i0hg4xq1yxx6yfa99g272z8162si75hnl";
        version = "1.0.0";
      };

      "elm/bytes" = {
        sha256 = "02ywbf52akvxclpxwj9n04jydajcbsbcbsnjs53yjc5lwck3abwj";
        version = "1.0.8";
      };

      "elm/file" = {
        sha256 = "1rljcb41dl97myidyjih2yliyzddkr2m7n74x7gg46rcw4jl0ny8";
        version = "1.0.5";
      };

      "elm/regex" = {
        sha256 = "0lijsp50w7n1n57mjg6clpn9phly8vvs07h0qh2rqcs0f1jqvsa2";
        version = "1.0.0";
      };

      "rtfeldman/elm-hex" = {
        sha256 = "1y0aa16asvwdqmgbskh5iba6psp43lkcjjw9mgzj3gsrg33lp00d";
        version = "1.0.0";
      };

      "folkertdev/elm-sha2" = {
        sha256 = "17rs2f8mx84kwi6mfcy3v3m4aplzxjwal1yqnv6a0licgfwgi1qc";
        version = "1.0.0";
      };

      "elm/parser" = {
        sha256 = "0a3cxrvbm7mwg9ykynhp7vjid58zsw03r63qxipxp3z09qks7512";
        version = "1.1.0";
      };

      "owanturist/elm-union-find" = {
        sha256 = "13gm7msnp0gr1lqia5m7m4lhy3m6kvjg37d304whb3psn88wqhj5";
        version = "1.0.0";
      };

      "danfishgold/base64-bytes" = {
        sha256 = "1zwc8hbs0vqbn8bpmawwf37523x70pb4cpldblvi58gg6486axfg";
        version = "1.0.3";
      };

      "elm/virtual-dom" = {
        sha256 = "0q1v5gi4g336bzz1lgwpn5b1639lrn63d8y6k6pimcyismp2i1yg";
        version = "1.0.2";
      };
}
