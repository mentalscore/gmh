{ nixpkgs ? <nixpkgs> , config ? {}}:

with (import nixpkgs config);

let
  app = pkgs.callPackage ./default.nix { } ;
  nginxPort = "8010";
  nginxConf = pkgs.writeText "nginx.conf" ''
      user nobody nobody;
      daemon off;
      error_log /dev/stdout info;
      pid /dev/null;
      events {}
      http {
        types {
            text/html html htm shtml;
            text/css css;
            text/xml xml;
            image/gif gif;
            image/jpeg jpeg jpg;
            application/javascript js;
            application/atom+xml atom;
            application/rss+xml rss;
            image/png png;
            image/svg+xml svg svgz;
            image/tiff tif tiff;
            image/vnd.wap.wbmp wbmp;
            image/webp webp;
            image/x-icon ico;
            image/x-jng jng;
            image/x-ms-bmp bmp;
            font/woff woff;
            font/woff2 woff2;
        }

        access_log /dev/stdout;
        server {
          listen ${nginxPort};
          index index.html;
          add_header Cache-Control "no-store, no-cache, must-revalidate";
          location / {
            root /data;
          }
        }
      }
    '';
in
pkgs.dockerTools.buildLayeredImage {
  name = "mentalscorexyz/app";
  tag = "latest";
  contents = [
    app
    pkgs.dockerTools.fakeNss
    pkgs.nginx
  ];

  extraCommands = ''
      # nginx still tries to read this directory even if error_log
      # directive is specifying another file :/
      mkdir -p var/log/nginx
      mkdir -p var/cache/nginx
    '';

  config = {
    Cmd = [ "nginx" "-c" nginxConf ];
    ExposedPorts = {
      "${nginxPort}/tcp" = {};
    };
  };
}
