{ nixpkgs ? <nixpkgs>
, config ? {}
}:

with (import nixpkgs config);

let
  mkDerivation =
    { srcs ? ./elm-srcs.nix
    , src
    , name
    , version
    , srcdir ? "./src"
    , targets ? []
    , registryDat ? ./registry.dat
    , outputJavaScript ? false
    , license
    }:
    stdenv.mkDerivation {
      inherit name src;

      buildInputs = [ elmPackages.elm ]
        ++ lib.optional outputJavaScript nodePackages.uglify-js;

      buildPhase = pkgs.elmPackages.fetchElmDeps {
        elmPackages = import srcs;
        elmVersion = "0.19.1";
        inherit registryDat;
      };

      installPhase = let
        elmfile = module: "${srcdir}/${builtins.replaceStrings ["."] ["/"] module}.elm";
        extension = if outputJavaScript then "js" else "html";
        prod_elm = "elm.${toString builtins.currentTime}.js";
        prod_js = "index.${toString builtins.currentTime}.js";
        prod_swjs = "sw.${toString builtins.currentTime}.js";
      in ''
        ${lib.concatStrings (map (module: ''
          echo "copying prod env"
          cp env/prod/Env.elm ./src/Env.elm
          echo "compiling ${elmfile module}"
          elm make ${elmfile module} --output $out/${module}.${extension}
          ${lib.optionalString outputJavaScript ''
            echo "minifying ${elmfile module}"
            uglifyjs $out/${module}.${extension} --compress 'pure_funcs="F2,F3,F4,F5,F6,F7,F8,F9,A2,A3,A4,A5,A6,A7,A8,A9",pure_getters,keep_fargs=false,unsafe_comps,unsafe' \
                | uglifyjs --mangle --output $out/elm.min.${extension}
          ''}
          rm $out/${module}.${extension}
        '') targets)}

        mv $out/elm.min.js $out/${prod_elm}
        mv ${srcdir}/index.html $out/index.html
        echo "copying ${prod_js}"
        mv ${srcdir}/index.js $out/${prod_js}
        mv ${srcdir}/sw.js $out/${prod_swjs}
        cp ${srcdir}/static/*.* $out/
        sed -i "s/elm.js/${prod_elm}/g" $out/index.html
        sed -i "s/index.js/${prod_js}/g" $out/index.html

        sed -i "s/sw.js/${prod_swjs}/g" $out/${prod_js}

        sed -i "s/elm.js/${prod_elm}/g" $out/${prod_swjs}
        sed -i "s/index.js/${prod_js}/g" $out/${prod_swjs}

        mkdir $out/data
        cp $out/*.* $out/data
      '';
    };
in mkDerivation {
  name = "app";
  version = "0.1.0.0";
  srcs = ./elm-srcs.nix;
  src = ./.;
  targets = ["Main"];
  srcdir = "./src";
  outputJavaScript = true;
  license = stdenv.lib.licenses.gpl3;
}

