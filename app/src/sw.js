const CACHE_NAME = "V1";
const STATIC_CACHE_URLS = [
  "/app/",
  "/app/index.html",
  "/app/index.js",
  "/app/elm.js",
  "/app/favicon-16x16.png",
  "/app/favicon-32x32.png",
  "/app/android-chrome-512x512.png",
  "/app/android-chrome-192x192.png",
  "/app/apple-touch-icon.png",
  "/app/mstile-150x150.png",
];

self.addEventListener("install", (event) => {
  console.log("Service Worker installing.");
  event.waitUntil(
    caches.open(CACHE_NAME).then((cache) => cache.addAll(STATIC_CACHE_URLS))
  );
});

self.addEventListener("fetch", (event) => {
  // Network falling back to the cache
  event.respondWith(
    fetch(event.request).catch(function () {
      return caches.match(event.request);
    })
  );
});
