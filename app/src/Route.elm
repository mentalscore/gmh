module Route exposing (Route(..), fromUrl, replaceUrl, toString)

import Browser.Navigation as Nav
import Data.Indication as Indication exposing (Indication)
import Url.Parser as Parser exposing ((</>), (<?>), Parser, oneOf, s, string)
import Url.Parser.Query as Query



-- ROUTING


type Route
    = Home
    | Login
    | Register
    | UpdatePassword String
    | Logout
    | Lusher
    | Welcome
    | Screeners
    | Doctor
    | Settings
    | Tools
    | Profile
    | Indication Indication
    | Subscription


parser : Parser (Route -> a) a
parser =
    oneOf
        [ Parser.map Home Parser.top
        , Parser.map Login (s "login")
        , Parser.map Register (s "register")
        , Parser.map UpdatePassword (s "update-password" </> string)
        , Parser.map Logout (s "logout")
        , Parser.map Lusher (s "lusher")
        , Parser.map Welcome (s "welcome")
        , Parser.map Screeners (s "screeners")
        , Parser.map Doctor (s "doctor")
        , Parser.map Settings (s "settings")
        , Parser.map Tools (s "tools")
        , Parser.map Profile (s "profile")
        , Parser.map Indication (s "indication" </> Indication.urlParser)
        , Parser.map Subscription (s "subscription")
        ]



-- PUBLIC HELPERS
-- href : Route -> Attribute msg
-- href targetRoute =
--     href (routeToString targetRoute)


replaceUrl : Nav.Key -> Route -> Cmd msg
replaceUrl key route =
    Nav.pushUrl key (toString route)


fromUrl url =
    -- The RealWorld spec treats the fragment like a path.
    -- This makes it *literally* the path, so we can proceed
    -- with parsing as if it had been a normal path all along.
    { url | path = Maybe.withDefault "" url.fragment, fragment = Nothing }
        |> Parser.parse parser



-- INTERNAL


toString : Route -> String
toString page =
    "#/" ++ String.join "/" (routeToPieces page)


routeToPieces : Route -> List String
routeToPieces page =
    case page of
        Home ->
            []

        Login ->
            [ "login" ]

        Register ->
            [ "register" ]

        UpdatePassword _ ->
            [ "update-password" ]

        Logout ->
            [ "logout" ]

        Lusher ->
            [ "lusher" ]

        Welcome ->
            [ "welcome" ]

        Screeners ->
            [ "screeners" ]

        Doctor ->
            [ "doctor" ]

        Settings ->
            [ "settings" ]

        Tools ->
            [ "tools" ]

        Profile ->
            [ "profile" ]

        Indication indication ->
            [ "indication", Indication.toString indication ]

        Subscription ->
            [ "subscription" ]
