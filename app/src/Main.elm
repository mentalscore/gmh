module Main exposing (main)

import Browser exposing (Document)
import Browser.Navigation as Nav
import Data.Indication exposing (Indication)
import Element as El
import Html
import Json.Decode as JD exposing (Value)
import Localizations.Languages as Languages exposing (Language)
import Page exposing (Page)
import Page.Blank as Blank
import Page.Doctor as Doctor
import Page.Home as Home
import Page.Indication as Indication
import Page.Login as Login
import Page.Lusher as Lusher
import Page.NotFound as NotFound
import Page.Profile as Profile
import Page.Register as Register
import Page.Screeners as Screeners
import Page.Settings as Settings
import Page.Subscription as Subscription
import Page.Tools as Tools
import Page.UpdatePassword as UpdatePassword
import Page.Welcome as Welcome
import Ports
import Route exposing (Route)
import Session exposing (Session)
import Url exposing (Url)
import Viewer exposing (Viewer)



-- MAIN


main : Program Value Model Msg
main =
    Browser.application
        { init = init
        , onUrlChange = ChangedUrl
        , onUrlRequest = ClickedLink
        , subscriptions = subscriptions
        , update = update
        , view = view
        }



-- MODEL


type Model
    = Login Login.Model
    | Register Register.Model
    | UpdatePassword UpdatePassword.Model
    | Redirect Session
    | NotFound Session
    | Home Home.Model
    | Lusher Lusher.Model
    | Welcome Welcome.Model
    | Screeners Screeners.Model
    | Doctor Doctor.Model
    | Settings Settings.Model
    | Tools Tools.Model
    | Profile Profile.Model
    | Indication Indication Indication.Model
    | Subscription Subscription.Model


init : Value -> Url -> Nav.Key -> ( Model, Cmd Msg )
init flags url navKey =
    let
        language =
            Languages.fromFlag flags

        maybeViewer : Result Language Viewer
        maybeViewer =
            flags
                |> JD.decodeValue Viewer.decoder
                |> Result.mapError (\_ -> language)

        loginModel =
            Login.makeInitModel navKey language
    in
    case Debug.log "session: " <| Session.fromViewer navKey maybeViewer of
        Just session ->
            changeRouteTo (Route.fromUrl url) (Redirect session) session

        Nothing ->
            case Route.fromUrl url of
                Just Route.Register ->
                    Register.init navKey language
                        |> updateWith Register GotRegisterMsg (Register <| Register.initModel navKey language)

                Just (Route.UpdatePassword key) ->
                    language
                        |> UpdatePassword.init key navKey
                        |> updateWith UpdatePassword GotUpdatePasswordMsg (UpdatePassword <| Tuple.first <| UpdatePassword.init key navKey language)

                _ ->
                    language
                        |> Login.init navKey
                        |> updateWith Login GotLoginMsg (Login loginModel)


changeRouteToNew : Maybe Route -> Model -> ( Model, Cmd Msg )
changeRouteToNew route model =
    case toSession model of
        Just session ->
            changeRouteTo route model session

        Nothing ->
            case model of
                Register m ->
                    m.language
                        |> Login.init m.navKey
                        |> updateWith Login GotLoginMsg (Register m)

                UpdatePassword m ->
                    m.language
                        |> Login.init m.navKey
                        |> updateWith Login GotLoginMsg (UpdatePassword m)

                Login m ->
                    Register.init m.navKey m.language
                        |> updateWith Register
                            GotRegisterMsg
                            (Register <|
                                Register.initModel m.navKey m.language
                            )

                _ ->
                    ( model, Cmd.none )


changeRouteTo : Maybe Route -> Model -> Session -> ( Model, Cmd Msg )
changeRouteTo route model session =
    case route of
        Just Route.Home ->
            Home.init session
                |> updateWith Home GotHomeMsg model

        Just Route.Lusher ->
            Lusher.init session
                |> updateWith Lusher GotLusherMsg model

        Just Route.Welcome ->
            Welcome.init session
                |> updateWith Welcome GotWelcomeMsg model

        Just Route.Screeners ->
            Screeners.init session
                |> updateWith Screeners GotScreenersMsg model

        Just Route.Doctor ->
            Doctor.init session
                |> updateWith Doctor GotDoctorMsg model

        Just Route.Settings ->
            Settings.init session
                |> updateWith Settings GotSettingsMsg model

        Just Route.Tools ->
            Tools.init session
                |> updateWith Tools GotToolsMsg model

        Just Route.Profile ->
            Profile.init session
                |> updateWith Profile GotProfileMsg model

        Just (Route.Indication indication) ->
            Indication.init session indication
                |> updateWith (Indication indication) GotIndicationMsg model

        Just Route.Subscription ->
            Subscription.init session
                |> updateWith Subscription GotSubscriptionMsg model

        Just Route.Logout ->
            ( model
            , Cmd.batch
                [ Nav.reload
                , Viewer.logout
                ]
            )

        -- shouldn't happen
        Just Route.Login ->
            Home.init session
                |> updateWith Home GotHomeMsg model

        -- shouldn't happen
        Just Route.Register ->
            Home.init session
                |> updateWith Home GotHomeMsg model

        -- shouldn't happen
        Just (Route.UpdatePassword _) ->
            Home.init session
                |> updateWith Home GotHomeMsg model

        Nothing ->
            ( NotFound session, Cmd.none )


toSession : Model -> Maybe Session
toSession page =
    case page of
        Redirect session ->
            Just <| session

        NotFound session ->
            Just <| session

        Home home ->
            Just <| Home.toSession home

        Lusher lusher ->
            Just <| Lusher.toSession lusher

        Welcome survey ->
            Just <| Welcome.toSession survey

        Screeners screeners ->
            Just <| Screeners.toSession screeners

        Doctor doctor ->
            Just <| Doctor.toSession doctor

        Settings settings ->
            Just <| Settings.toSession settings

        Tools tools ->
            Just <| Tools.toSession tools

        Profile profile ->
            Just <| Profile.toSession profile

        Indication indication indicationModel ->
            Just <| Indication.toSession indicationModel

        Subscription subscription ->
            Just <| Subscription.toSession subscription

        Login login ->
            Login.toSession login

        Register register ->
            Register.toSession register

        UpdatePassword updatePassword ->
            UpdatePassword.toSession updatePassword



-- UPDATE


type Msg
    = ChangedUrl Url
    | ClickedLink Browser.UrlRequest
    | ToggleMenu
    | GotLoginMsg Login.Msg
    | GotRegisterMsg Register.Msg
    | GotUpdatePasswordMsg UpdatePassword.Msg
    | GotHomeMsg Home.Msg
    | GotLusherMsg Lusher.Msg
    | GotWelcomeMsg Welcome.Msg
    | GotScreenersMsg Screeners.Msg
    | GotDoctorMsg Doctor.Msg
    | GotSettingsMsg Settings.Msg
    | GotToolsMsg Tools.Msg
    | GotProfileMsg Profile.Msg
    | GotIndicationMsg Indication.Msg
    | GotSubscriptionMsg Subscription.Msg
    | GotSession Session


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case ( msg, model ) of
        ( ClickedLink urlRequest, _ ) ->
            case urlRequest of
                Browser.Internal url ->
                    case url.fragment of
                        Nothing ->
                            -- If we got a link that didn't include a fragment,
                            -- it's from one of those (href "") attributes that
                            -- we have to include to make the RealWorld CSS work.
                            --
                            -- In an application doing path routing instead of
                            -- fragment-based routing, this entire
                            -- `case url.fragment of` expression this comment
                            -- is inside would be unnecessary.
                            ( model, Cmd.none )

                        Just _ ->
                            ( model
                            , toSession model
                                |> Maybe.map .navKey
                                |> Maybe.map (\s -> Nav.pushUrl s (Url.toString url))
                                |> Maybe.withDefault Cmd.none
                            )

                Browser.External href ->
                    ( model
                    , Nav.load href
                    )

        ( ChangedUrl url, m ) ->
            changeRouteToNew (Route.fromUrl url) model

        ( GotLoginMsg subMsg, Login login ) ->
            Login.update subMsg login
                |> updateWith Login GotLoginMsg model

        ( GotRegisterMsg subMsg, Register register ) ->
            Register.update subMsg register
                |> updateWith Register GotRegisterMsg model

        ( GotUpdatePasswordMsg subMsg, UpdatePassword updatePassword ) ->
            UpdatePassword.update subMsg updatePassword
                |> updateWith UpdatePassword GotUpdatePasswordMsg model

        ( GotHomeMsg subMsg, Home home ) ->
            Home.update subMsg home
                |> updateWith Home GotHomeMsg model

        ( GotLusherMsg subMsg, Lusher lusher ) ->
            Lusher.update subMsg lusher
                |> updateWith Lusher GotLusherMsg model

        ( GotWelcomeMsg subMsg, Welcome survey ) ->
            Welcome.update subMsg survey
                |> updateWith Welcome GotWelcomeMsg model

        ( GotScreenersMsg subMsg, Screeners screeners ) ->
            Screeners.update subMsg screeners
                |> updateWith Screeners GotScreenersMsg model

        ( GotDoctorMsg subMsg, Doctor doctor ) ->
            Doctor.update subMsg doctor
                |> updateWith Doctor GotDoctorMsg model

        ( GotSettingsMsg subMsg, Settings settings ) ->
            Settings.update subMsg settings
                |> updateWith Settings GotSettingsMsg model

        ( GotToolsMsg subMsg, Tools tools ) ->
            Tools.update subMsg tools
                |> updateWith Tools GotToolsMsg model

        ( GotProfileMsg subMsg, Profile settings ) ->
            Profile.update subMsg settings
                |> updateWith Profile GotProfileMsg model

        ( GotIndicationMsg subMsg, Indication indication indicationModel ) ->
            Indication.update subMsg indicationModel
                |> updateWith (Indication indication) GotIndicationMsg model

        ( GotSubscriptionMsg subMsg, Subscription settings ) ->
            Subscription.update subMsg settings
                |> updateWith Subscription GotSubscriptionMsg model

        ( GotSession session, Redirect _ ) ->
            ( Redirect session
            , Route.replaceUrl (Session.getNavKey session) Route.Home
            )

        ( _, _ ) ->
            -- Disregard messages that arrived for the wrong page.
            ( model, Cmd.none )


updateWith : (subModel -> Model) -> (subMsg -> Msg) -> Model -> ( subModel, Cmd subMsg ) -> ( Model, Cmd Msg )
updateWith toModel toMsg model ( subModel, subCmd ) =
    ( toModel subModel
    , Cmd.map toMsg subCmd
    )



-- VIEW


view : Model -> Document Msg
view model =
    let
        viewPage page toMsg config =
            let
                { title, body } =
                    Page.view page config
            in
            { title = title
            , body = List.map (Html.map toMsg) body
            }
    in
    case model of
        Redirect _ ->
            Page.view Page.Other Blank.view

        NotFound _ ->
            Page.view Page.Other NotFound.view

        Login login ->
            viewPage Page.Other GotLoginMsg (Login.view login)

        Register register ->
            viewPage Page.Other GotRegisterMsg (Register.view register)

        UpdatePassword updatePassword ->
            viewPage Page.Other GotUpdatePasswordMsg (UpdatePassword.view updatePassword)

        Home home ->
            viewPage Page.Other GotHomeMsg (Home.view home)

        Lusher lusher ->
            viewPage Page.Other GotLusherMsg (Lusher.view lusher)

        Welcome survey ->
            viewPage Page.Other GotWelcomeMsg (Welcome.view survey)

        Screeners screeners ->
            viewPage Page.Other GotScreenersMsg (Screeners.view screeners)

        Doctor doctor ->
            viewPage Page.Other GotDoctorMsg (Doctor.view doctor)

        Settings settings ->
            viewPage Page.Other GotSettingsMsg (Settings.view settings)

        Tools tools ->
            viewPage Page.Other GotToolsMsg (Tools.view tools)

        Profile profile ->
            viewPage Page.Other GotProfileMsg (Profile.view profile)

        Indication indication indicationModel ->
            viewPage Page.Other GotIndicationMsg (Indication.view indicationModel)

        Subscription subscription ->
            viewPage Page.Other GotSubscriptionMsg (Subscription.view subscription)



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    case model of
        NotFound _ ->
            Sub.none

        Redirect _ ->
            -- Session.changes GotSession (Session.getNavKey (toSession model))
            Sub.none

        Login _ ->
            Sub.none

        Register _ ->
            Sub.none

        UpdatePassword _ ->
            Sub.none

        Home m ->
            Home.subscriptions m
                |> Sub.map GotHomeMsg

        Lusher lusher ->
            -- Sub.none
            Sub.map GotLusherMsg (Lusher.subscriptions lusher)

        Welcome welcome ->
            Sub.none

        Screeners screeners ->
            Sub.none

        Doctor doctor ->
            Sub.none

        Settings settings ->
            Settings.subscriptions settings
                |> Sub.map GotSettingsMsg

        Tools tools ->
            Sub.none

        Profile profile ->
            Sub.none

        Indication indication indicationModel ->
            Sub.none

        Subscription subscription ->
            Sub.none
