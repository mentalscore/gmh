-- Deprecated


module Localization exposing (..)


type alias Translation =
    { english : String
    , russian : String
    , dutch : String
    }


type Id
    = -- Home Page
      HomePageTitle
    | Diagnosis
    | Disclaimer
    | DisclaimerParagraph1
    | DisclaimerParagraph2
    | DisclaimerAccept
    | LinkToLusherTest
    | GeneralError
      -- Luser
    | LusherPageTitle
    | LusherPleaseWait
      -- Surver
    | Survey
    | SurveyPageTitle
    | SurveyNamePlaceholder
    | SurveyNameLabel
    | SurveyAgeLabel
    | SurveySexLabel
    | SurveySexMale
    | SurveySexFemale
    | SurveyAnxietyLabel
    | SurveyAnxietyPersonal
    | SurveyAnxietyPsychosomatics
    | SurveyAnxietyFamily
    | SurveyAnxietyMood
    | SurveyAnxietyFriends
    | SurveyAnxietyWork
    | SurveyAnxietyCommunication
    | SurveyAnxietyOther
    | SurveyTreatmentLabel
    | SurveySelfDiagnosis
    | SurveySelftTests
    | SurveyMobileApp
    | SurveyPsychotherapist
    | SurveyNeedHelpLabel
    | SurveyNeedHelpYes
    | SurveyNeedHelpNo
    | SurveyNeedHelpNotSure
    | Submit


type Language
    = English
    | Russian
    | Dutch


fromString : String -> Language
fromString str =
    case String.left 2 str of
        "en" ->
            English

        "ru" ->
            Russian

        "nl" ->
            Dutch

        _ ->
            English


toString : Language -> String
toString lang =
    case lang of
        English ->
            "English"

        Russian ->
            "Russian"

        Dutch ->
            "Dutch"


translate : Language -> Id -> String
translate lang =
    case lang of
        English ->
            .english << translation

        Russian ->
            .russian << translation

        Dutch ->
            .dutch << translation


translation : Id -> Translation
translation id =
    case id of
        HomePageTitle ->
            Translation
                "Home"
                "Главная"
                "hoofdp"

        Diagnosis ->
            Translation
                "You are healthy"
                "Вы абсолютно здоровы"
                "You are healthy"

        Disclaimer ->
            Translation
                "Disclaimer"
                "Отказ от ответсвтенности"
                "Ontkenning"

        DisclaimerParagraph1 ->
            Translation
                "This mental health application is not intended to be a replacement for treatment nor any sort of medical intervention. Treatment is, by far, the best way to overcome clinical depression or other mental illnesses. Therapy and antidepressants have been shown to effectively treat clinical depression and anxiety. This application will only aid you on your path to recovery from depression or anxiety. In addition, if you are not in treatment, talk to your doctor or therapist or use this app to find resources that can connect you to treatment."
                ""
                ""

        DisclaimerParagraph2 ->
            Translation
                "While App is designed as a self-help mental health application to help people suffering from clinical depression, it can also assist people without mental illness or people suffering from other mental disorders / mental illnesses such as anxiety, post-traumatic stress disorder (PTSD), bipolar disorder, seasonal affective disorder, dysthymia, obsessive-compulsive disorder (OCD), panic disorder, generalized anxiety disorder, or schizophrenia"
                ""
                ""

        DisclaimerAccept ->
            Translation
                "ACCEPT"
                ""
                ""

        -- Luser
        LusherPageTitle ->
            Translation
                "Lusher test"
                ""
                ""

        LusherPleaseWait ->
            Translation
                "Please Wait"
                ""
                ""

        LinkToLusherTest ->
            Translation
                "Lusher Link"
                ""
                ""

        GeneralError ->
            Translation
                "oops something went wrong"
                ""
                ""

        -- Survey
        Survey ->
            Translation
                "Survey"
                ""
                ""

        SurveyPageTitle ->
            Translation
                "Survey"
                ""
                ""

        SurveyNamePlaceholder ->
            Translation
                "Name"
                ""
                ""

        SurveyNameLabel ->
            Translation
                "What's your name"
                ""
                ""

        SurveyAgeLabel ->
            Translation
                "Age"
                ""
                ""

        SurveySexLabel ->
            Translation
                "Sex"
                ""
                ""

        SurveySexMale ->
            Translation
                "Male"
                ""
                ""

        SurveySexFemale ->
            Translation
                "Female"
                ""
                ""

        SurveyAnxietyLabel ->
            Translation
                "Worries you"
                ""
                ""

        SurveyAnxietyPersonal ->
            Translation
                "Personal"
                ""
                ""

        SurveyAnxietyPsychosomatics ->
            Translation
                "Psychosomatics"
                ""
                ""

        SurveyAnxietyFamily ->
            Translation
                "Family"
                ""
                ""

        SurveyAnxietyMood ->
            Translation
                "Mood"
                ""
                ""

        SurveyAnxietyFriends ->
            Translation
                "Friends"
                ""
                ""

        SurveyAnxietyWork ->
            Translation
                "Work"
                ""
                ""

        SurveyAnxietyCommunication ->
            Translation
                "Communication"
                ""
                ""

        SurveyAnxietyOther ->
            Translation
                "Other"
                ""
                ""

        SurveyTreatmentLabel ->
            Translation
                "Treatment"
                ""
                ""

        SurveySelfDiagnosis ->
            Translation
                "Diagnosis"
                ""
                ""

        SurveySelftTests ->
            Translation
                "SelftTests"
                ""
                ""

        SurveyMobileApp ->
            Translation
                "MobileApp"
                ""
                ""

        SurveyPsychotherapist ->
            Translation
                "Psychotherapist"
                ""
                ""

        SurveyNeedHelpLabel ->
            Translation
                "Do you need help?"
                ""
                ""

        SurveyNeedHelpYes ->
            Translation
                "Yes"
                ""
                ""

        SurveyNeedHelpNo ->
            Translation
                "No"
                ""
                ""

        SurveyNeedHelpNotSure ->
            Translation
                "NotSure"
                ""
                ""

        Submit ->
            Translation
                "Submit"
                ""
                ""
