module Page.Screeners exposing (..)

import Dict exposing (Dict)
import Element as El exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Env exposing (endpoint)
import FontAwesome.Solid exposing (check)
import Http
import Json.Decode as JD
import Json.Encode as JE
import Localizations.Languages exposing (..)
import Localizations.Screeners as Ln exposing (translate)
import Route
import Session exposing (Session)
import UI.Button as Button
import UI.Color as Color
import UI.Page as Page


{-| <https://www.phqscreeners.com/>
-}



-- MODEL


type alias Model =
    { session : Session
    , screener : Maybe Screener
    , phq : FullQuestionnaire
    , phq15 : Questionnaire
    , phq9 : Questionnaire
    , phq8 : Questionnaire
    , gad7 : Questionnaire
    }


type Screener
    = PHQ ( Int, Int )
    | PHQ15 Int
    | PHQ9 Int
    | PHQ8 Int
    | GAD7 Int


init : Session -> ( Model, Cmd msg )
init session =
    ( { session = session
      , screener = Nothing --Just (PHQ9 0)
      , phq = phq
      , phq15 = phq15
      , phq9 = phq9
      , phq8 = phq8
      , gad7 = gad7
      }
    , Cmd.none
    )


type alias Response =
    { label : String
    , score : Int
    }


type alias FullQuestionnaire =
    { id : String
    , name : String
    , explanation : String
    , questionnaires : Dict Int Questionnaire
    }


type alias Question =
    { title : String
    , response : Maybe Response
    }


initQuestion : String -> Question
initQuestion title =
    { title = title
    , response = Nothing
    }


type alias Questionnaire =
    { id : String
    , name : String
    , explanation : String
    , questions : Dict Int Question
    , possible : List ( String, Int )
    }


response4 : List ( String, Int )
response4 =
    [ ( "Not at all", 0 )
    , ( "Several days", 1 )
    , ( "More than half the days", 2 )
    , ( "Nearly every day ", 3 )
    ]


response3 : List ( String, Int )
response3 =
    [ ( "Not bothered at all", 0 )
    , ( "Bothered a little", 1 )
    , ( "Bothered", 2 )
    ]


response2 : List ( String, Int )
response2 =
    [ ( "No", 0 )
    , ( "Yes", 1 )
    ]


phq : FullQuestionnaire
phq =
    { id = "PHQ"
    , name = "Patient Health Questionnaire"
    , explanation = "This questionnaire is an important part of providing you with the best health care possible.  Your answers will help in understanding problems that you may have.  Please answer every question to the best of your ability unless you are requested to skip over a question."
    , questionnaires =
        [ phq15, phq9 ]
            |> List.indexedMap Tuple.pair
            |> Dict.fromList
    }


phq9 : Questionnaire
phq9 =
    { name = "PATIENT HEALTH QUESTIONNAIRE-9"
    , id = "PHQ-9"
    , explanation = "Over the last 2 weeks, how often have you been bothered by any of the following problems? "
    , questions =
        [ "Little interest or pleasure in doing things"
        , "Feeling down, depressed, or hopeless"
        , "Trouble falling or staying asleep, or sleeping too much"
        , "Feeling tired or having little energy"
        , "Poor appetite or overeating"
        , "Feeling bad about yourself — or that you are a failure or have let yourself or your family down"
        , "Trouble concentrating on things, such as reading the newspaper or watching television"
        , "Moving or speaking so slowly that other people could have noticed? Or the opposite — being so fidgety or restless that you have been moving around a lot more than usual"
        , "Thoughts that you would be better off dead or of hurting yourself in some way"
        ]
            |> List.map initQuestion
            |> List.indexedMap Tuple.pair
            |> Dict.fromList
    , possible = response4
    }


gad7 : Questionnaire
gad7 =
    { name = "Generalized Anxiety Disorder subscale"
    , id = "GAD-7"
    , explanation = "Over the last 2 weeks, how often have you been bothered by any of the following problems? "
    , questions =
        [ "Feeling nervous, anxious or on edge"
        , "Not being able to stop or control worrying"
        , "Worrying too much about different things"
        , "Trouble relaxing"
        , "Being so restless that it is hard to sit still"
        , "Becoming easily annoyed or irritable"
        , "Feeling afraid as if something awful might happen"
        ]
            |> List.map initQuestion
            |> List.indexedMap Tuple.pair
            |> Dict.fromList
    , possible = response4
    }


phq8 : Questionnaire
phq8 =
    { name = "PATIENT HEALTH QUESTIONNAIRE-8"
    , id = "PHQ-8"
    , explanation = "Over the last 2 weeks, how often have you been bothered by any of the following problems? "
    , questions =
        [ "Little interest or pleasure in doing things"
        , "Feeling down, depressed, or hopeless"
        , "Trouble falling or staying asleep, or sleeping too much"
        , "Feeling tired or having little energy"
        , "Poor appetite or overeating"
        , "Feeling bad about yourself — or that you are a failure orhave let yourself or your family down"
        , "Trouble concentrating on things, such as reading thenewspaper or watching television"
        , "Moving or speaking so slowly that other people could havenoticed. Or the opposite — being so fidgety or restless thatyou have been moving around a lot more than usual"
        ]
            |> List.map initQuestion
            |> List.indexedMap Tuple.pair
            |> Dict.fromList
    , possible = response4
    }


phq15 : Questionnaire
phq15 =
    { name = "PHYSICAL SYMPTOMS"
    , id = "PHQ-15"
    , explanation = "During the past 4 weeks, how much have you been bothered by any of the following problems? "
    , questions =
        [ "Stomach pain"
        , "Back pain"
        , "Pain in your arms, legs, or joints (knees, hips, etc.)"
        , "Menstrual cramps or other problems with your periods"
        , "Headaches"
        , "Chest pain"
        , "Dizziness"
        , "Fainting spells"
        , "Feeling your heart pound or race"
        , "Shortness of breath"
        , "Pain or problems during sexual intercourse"
        , "Constipation, loose bowels, or diarrhea"
        , "Nausea, gas, or indigestion"
        , "Feeling tired or having low energy"
        , "Trouble sleeping"
        ]
            |> List.map initQuestion
            |> List.indexedMap Tuple.pair
            |> Dict.fromList
    , possible = response3
    }



-- UPDATE


type Msg
    = Set Screener
    | SetPHQ Int Int Response
    | SetPHQ15 Int Response
    | SetPHQ9 Int Response
    | GotPHQ9 (Result Http.Error Bool)
    | SetPHQ8 Int Response
    | SetGAD7 Int Response
    | AnxietyScoreUpdated Int


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Set screener ->
            ( { model | screener = Just screener }, Cmd.none )

        SetPHQ questionnaireId questionId response ->
            let
                nextQuestion =
                    questionId + 1

                next =
                    model.phq.questionnaires
                        |> Dict.get questionnaireId
                        |> Maybe.andThen (Dict.get nextQuestion << .questions)
                        |> Maybe.map (\_ -> ( questionnaireId, nextQuestion ))
                        |> Maybe.withDefault ( questionnaireId + 1, 0 )
            in
            ( { model
                | phq = updateFullQuestionnaire questionnaireId questionId response model.phq
                , screener = Just (PHQ next)
              }
            , Cmd.none
            )

        SetPHQ15 id response ->
            ( { model
                | phq15 = updateQuestionnaire id response model.phq15
                , screener = Just (PHQ15 (id + 1))
              }
            , Cmd.none
            )

        SetPHQ9 id response ->
            let
                phq9New =
                    updateQuestionnaire id response model.phq9
            in
            ( { model
                | phq9 = phq9New
                , screener = Just (PHQ9 (id + 1))
              }
            , if allAnswered phq9New then
                Session.uuid model.session
                    |> Maybe.map (createPHQ9 phq9New)
                    |> Maybe.withDefault Cmd.none

              else
                Cmd.none
            )

        GotPHQ9 (Ok _) ->
            ( model, Cmd.none )

        GotPHQ9 (Err _) ->
            -- TODO: Think about an error
            ( model, Cmd.none )

        SetPHQ8 id response ->
            ( { model
                | phq8 = updateQuestionnaire id response model.phq8
                , screener = Just (PHQ8 (id + 1))
              }
            , Cmd.none
            )

        SetGAD7 id response ->
            ( { model
                | gad7 = updateQuestionnaire id response model.gad7
                , screener = Just (GAD7 (id + 1))
              }
            , Cmd.none
            )

        AnxietyScoreUpdated int ->
            ( model, Route.replaceUrl (.navKey model.session) Route.Home )


createPHQ9 : Questionnaire -> String -> Cmd Msg
createPHQ9 questionnaire uuid =
    Http.post
        { url = endpoint ++ "tests/phq9"
        , body =
            Http.jsonBody
                (encodeQuestions uuid questionnaire.questions)
        , expect =
            Http.expectJson
                GotPHQ9
                (JD.field "saved" JD.bool)
        }


encodeQuestions : String -> Dict Int Question -> JE.Value
encodeQuestions uuid questions =
    ( "uuid", JE.string uuid )
        :: (questions
                |> Dict.map (\k v -> Maybe.withDefault 0 <| Maybe.map .score v.response)
                |> Dict.toList
                |> List.map (\( k, v ) -> ( String.fromInt k, JE.int v ))
           )
        |> JE.object


updateFullQuestionnaire : Int -> Int -> Response -> FullQuestionnaire -> FullQuestionnaire
updateFullQuestionnaire questionnaireId questionId response complexQuestionnaire =
    { complexQuestionnaire
        | questionnaires =
            complexQuestionnaire.questionnaires
                |> Dict.update questionnaireId (Maybe.map (updateQuestionnaire questionId response))
    }


updateQuestionnaire : Int -> Response -> Questionnaire -> Questionnaire
updateQuestionnaire id response questionnaire =
    { questionnaire
        | questions =
            questionnaire.questions
                |> Dict.update id (Maybe.map (updateQuestion response))
    }


allAnswered : Questionnaire -> Bool
allAnswered questionnaire =
    questionnaire.questions
        |> Dict.values
        |> List.map .response
        |> List.member Nothing
        |> not


updateQuestion : Response -> Question -> Question
updateQuestion response question =
    { question | response = Just response }



-- VIEW


view : Model -> { title : String, content : Element Msg }
view model =
    let
        translate =
            Ln.translate English
    in
    { title = translate Ln.Title
    , content = content translate model
    }


content : (Ln.Id -> String) -> Model -> Element Msg
content l10n model =
    El.column
        [ El.width El.fill
        , El.height El.fill
        , Font.color Color.black
        , Font.size 14
        ]
        (viewMain l10n model)


phq9Measure : Int -> Measure
phq9Measure p =
    { header = "Your Depression Measure"
    , points = p
    , max = 27
    , level = phq9CutPoint p
    , levels =
        [ ( Minimal, "0-5" )
        , ( Mild, "5-10" )
        , ( Moderate, "10-15" )
        , ( ModeratelySevere, "15-20" )
        , ( Severe, "20-27" )
        ]
    }


phq9CutPoint : Int -> Level
phq9CutPoint p =
    if p >= 0 && p < 5 then
        Minimal

    else if p >= 5 && p < 10 then
        Mild

    else if p >= 10 && p < 15 then
        Moderate

    else if p >= 15 && p < 20 then
        ModeratelySevere

    else
        Severe


type alias Measure =
    { header : String
    , points : Int
    , max : Int
    , level : Level
    , levels : List ( Level, String )
    }


type Level
    = Minimal
    | Mild
    | Moderate
    | ModeratelySevere
    | Severe


levelToString : Level -> String
levelToString level =
    case level of
        Minimal ->
            "Minimal"

        Mild ->
            "Mild"

        Moderate ->
            "Moderate"

        ModeratelySevere ->
            "Moderately Severe"

        Severe ->
            "Severe"


levelToDescription : Level -> String
levelToDescription level =
    case level of
        Minimal ->
            "Your results indicate that you may not be experiencing symptoms of depression."

        Mild ->
            "Your results indicate that you may be experiencing symptoms of mild depression."

        Moderate ->
            "Your results indicate that you may be experiencing symptoms of moderate depression."

        ModeratelySevere ->
            "Your results indicate that you may be experiencing symptoms of moderate severe depression."

        Severe ->
            "Your results indicate that you may be experiencing symptoms of severe depression."


viewMeasure : Measure -> Element Msg
viewMeasure measure =
    column
        [ width fill
        , height fill
        , Background.color Color.primaryBright
        , Font.color Color.white
        ]
        [ headerContrast measure.header
        , column [ centerX, El.padding 16 ]
            [ el [ centerX, El.padding 8, Font.size 14 ] (text "YOUR RESULT")
            , el [ centerX, El.padding 8, Font.size 24 ] (text (levelToString measure.level))
            ]
        , el
            [ centerX
            , El.padding 32
            , El.width (px 200)
            , El.height (px 200)
            , Border.rounded 200
            , Border.color Color.white
            , Border.width 2
            ]
            (column [ centerX, centerY ] [ el [ centerX, Font.bold, Font.size 50, padding 16 ] (text (String.fromInt measure.points)), el [ centerX, Font.size 16 ] (text ("out of " ++ String.fromInt measure.max)) ])
        , paragraph [ Font.size 16, padding 16 ]
            [ el []
                (text (levelToDescription measure.level))
            ]
        , measure.levels
            |> List.map
                (\( level, str ) ->
                    row [ width fill ] [ text (levelToString level ++ " Depression"), el [ alignRight ] (text str) ]
                )
            |> column [ Font.size 14, padding 16, spacing 8, width (px 300) ]
        , el [ width fill, El.padding 16, alignBottom ]
            (Button.primary "Next" check (AnxietyScoreUpdated 10))
        ]


headerContrast : String -> Element msg
headerContrast str =
    El.row
        [ El.width El.fill
        , Background.color Color.primary
        , Font.color Color.white
        , El.paddingEach
            { bottom = 16
            , top = 28
            , left = 0
            , right = 0
            }
        , Font.size 22
        ]
        [ El.el
            [ Font.bold, El.centerX ]
            (El.text str)
        ]



-- el [ centerY, centerX ]
--     (text
--         ("Score for "
--             ++ "asdf"
--             ++ ": "
--             ++ "10"
--             ++ " points"
--         )
--     )


viewMain : (Ln.Id -> String) -> Model -> List (Element Msg)
viewMain l10n model =
    case model.screener of
        Just (PHQ ( questionnaireId, questionId )) ->
            [ viewFullQuestionnaire model.phq ( questionnaireId, questionId ) (SetPHQ questionnaireId questionId) ]

        Just (PHQ15 id) ->
            [ viewQuestionnaire model.phq15 id (SetPHQ15 id) ]

        Just (PHQ9 id) ->
            [ viewQuestionnaire model.phq9 id (SetPHQ9 id) ]

        Just (PHQ8 id) ->
            [ viewQuestionnaire model.phq8 id (SetPHQ8 id) ]

        Just (GAD7 id) ->
            [ viewQuestionnaire model.gad7 id (SetGAD7 id) ]

        Nothing ->
            [ column [ width fill, height fill ]
                [ El.el [ centerX, El.padding 16 ]
                    (Page.header (l10n Ln.Header))
                , el [ width fill, El.padding 16 ]
                    (Button.primary "Depression Screening" check (Set (PHQ9 0)))

                -- , el [ width fill, El.padding 16 ]
                --     (Button.primary (l10n Ln.PHQTitle) check (Set (PHQ ( 0, 0 ))))
                -- , el [ width fill, El.padding 16 ]
                --     (Button.primary "GAD-7" check (Set (GAD7 0)))
                -- , el [ width fill, El.padding 16 ]
                --     (Button.primary "PHQ-8" check (Set (PHQ8 0)))
                -- , el [ width fill, El.padding 16 ]
                --     (Button.primary "PHQ-15" check (Set (PHQ15 0)))
                ]
            , Page.bottomBar Page.Screeners
            ]


viewFullQuestionnaire : FullQuestionnaire -> ( Int, Int ) -> (Response -> Msg) -> Element Msg
viewFullQuestionnaire complexQuestionnaire ( questionnaireId, questionId ) msg =
    case Dict.get questionnaireId complexQuestionnaire.questionnaires of
        Just questionnaire ->
            viewQuestionnaire
                { questionnaire
                    | id = complexQuestionnaire.id
                    , name = complexQuestionnaire.name
                }
                questionId
                msg

        Nothing ->
            complexQuestionnaire.questionnaires
                |> Dict.values
                |> List.map (\questionnaire -> result questionnaire.id questionnaire.questions)
                |> column [ width fill, height fill, padding 16, spacing 16 ]


viewQuestionnaire : Questionnaire -> Int -> (Response -> Msg) -> Element Msg
viewQuestionnaire questionnaire id msg =
    case
        questionnaire.questions
            |> Dict.get id
            |> Maybe.map (viewQuestion questionnaire.possible msg id)
    of
        Just html ->
            column
                [ width fill, height fill, padding 16, spacing 16 ]
                [ el [ centerX ] (Page.header questionnaire.id)
                , Page.h4 questionnaire.name
                , paragraph [] [ el [] (text questionnaire.explanation) ]
                , html
                    |> el [ width fill, height fill ]
                ]

        Nothing ->
            result questionnaire.id questionnaire.questions


result : String -> Dict Int Question -> Element Msg
result id responses =
    responses
        |> Dict.values
        |> List.filterMap .response
        |> List.foldl (\x -> (+) x.score) 0
        |> phq9Measure
        |> viewMeasure


viewQuestion : List ( String, Int ) -> (Response -> msg) -> Int -> Question -> Element msg
viewQuestion possible msg id question =
    let
        viewAnswer ( response, score ) =
            Button.secondary response (msg (Response response score))
    in
    column [ width fill, height fill ]
        [ el [ paddingXY 0 16, alignTop ]
            (paragraph [] [ Page.h2 question.title ])
        , column [ width fill, spacing 16, paddingXY 0 16, centerX, alignBottom ]
            (List.map viewAnswer possible)
        ]



-- EXPORT


toSession : Model -> Session
toSession model =
    model.session
