module Page.Indication exposing (..)

import Data.Indication as Indication exposing (..)
import Data.Level exposing (..)
import Data.Measure exposing (..)
import Data.Score as Score
import Element as El exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Events exposing (onClick)
import Element.Font as Font
import Env exposing (endpoint)
import Http
import Localizations.Indication as Ln exposing (Ln)
import Localizations.Screeners as LnScreeners
import RemoteData as RD exposing (..)
import Route
import Session exposing (Session)
import String.Extra as String
import Time exposing (Month(..))
import UI.Button
import UI.Color as Color
import UI.Input
import UI.Page as Page
import UI.Scale as Scale



-- MODEL


type alias Model =
    { session : Session
    , status : Status
    , scores : RemoteData Http.Error (List Score.Score)
    }


type alias Status =
    { indication : Indication
    , tab : Tab
    }


type Tab
    = Current
    | History


tabToString : Tab -> String
tabToString tab =
    case tab of
        Current ->
            "Current Status"

        History ->
            "History"


init : Session -> Indication -> ( Model, Cmd Msg )
init session indication =
    ( { session = session
      , status = initStatus indication
      , scores = Loading
      }
    , session.profile.id
        |> Score.get GotScore
    )


initStatus : Indication -> Status
initStatus indication =
    { indication = indication
    , tab = Current
    }



-- UPDATE


type Msg
    = ChangedTab Tab
    | GotScore (Result Http.Error (List Score.Score))
    | Redirect Route.Route


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        ChangedTab tab ->
            ( { model
                | status = updateStatusTab (\_ -> tab) model.status
              }
            , Cmd.none
            )

        GotScore (Ok scores) ->
            ( { model | scores = Success scores }
            , Cmd.none
            )

        GotScore (Err err) ->
            ( { model | scores = Failure err }
            , Cmd.none
            )

        Redirect route ->
            ( model
            , Route.replaceUrl model.session.navKey route
            )


updateStatusTab : (Tab -> Tab) -> Status -> Status
updateStatusTab toTab status =
    { status | tab = toTab status.tab }



-- VIEW


view : Model -> { title : String, content : Element Msg }
view model =
    { title = Ln.translate model.session.profile.language Ln.Title
    , content = content model
    }


content : Model -> Element Msg
content model =
    El.column
        [ El.width El.fill
        , El.height El.fill
        , Font.color Color.black
        , Font.size 14
        , paddingEach { bottom = 16, left = 16, right = 16, top = 16 }
        ]
        (viewBody model)


viewBody : Model -> List (Element Msg)
viewBody model =
    [ Page.header ((String.toSentenceCase << toString << .indication) model.status)
    , viewTabs model.status.tab
    , case model.status.tab of
        Current ->
            model.scores
                |> RD.map List.head
                |> viewScore model.status.indication

        History ->
            model.scores
                |> viewHistory model.status.indication
    ]


viewHistory : Indication -> RemoteData Http.Error (List Score.Score) -> Element Msg
viewHistory indication rd =
    case rd of
        NotAsked ->
            text ""

        Loading ->
            el [] (text "loading")

        Failure err ->
            text "err"

        Success scores ->
            column
                [ width fill
                , height fill
                , spacing 16
                ]
                [ viewHistorySuccess indication scores
                , el [ width fill, alignBottom ]
                    closeButton
                ]


closeButton : Element Msg
closeButton =
    UI.Button.secondary "Close" (Redirect Route.Home)


screenersButton : Element Msg
screenersButton =
    UI.Button.secondary "Check" (Redirect Route.Screeners)


viewHistorySuccess : Indication -> List Score.Score -> Element Msg
viewHistorySuccess indication scores =
    let
        isEqual : Score.Score -> Score.Score -> Bool
        isEqual s1 s2 =
            case indication of
                Score ->
                    False

                Depression ->
                    s1.depressionScale == s2.depressionScale

                Anxiety ->
                    s1.anxietyScale == s2.anxietyScale

                Psychosomatic ->
                    s1.somaticScale == s2.somaticScale

        f : Score.Score -> List Score.Score -> List Score.Score
        f next list =
            case list of
                [] ->
                    [ next ]

                x :: xs ->
                    if isEqual next x then
                        x :: xs

                    else
                        next :: x :: xs
    in
    scores
        |> List.foldr f []
        |> List.map
            (\x ->
                -- el [] (text <| "points" ++ String.fromInt x)
                case toPoints indication x of
                    Just int ->
                        column [ width fill ]
                            [ el [ paddingXY 16 0, width fill ]
                                (text (posixToDate x.createdAt))
                            , el [ width fill ]
                                (html <| Scale.verticalBar int)
                            ]

                    Nothing ->
                        none
            )
        |> column
            [ width fill
            , height fill
            , paddingXY 16 32
            , spacing 10
            ]


posixToDate createdAt =
    let
        date =
            Time.millisToPosix (round <| toFloat createdAt / 1000000)
    in
    toMonth (Time.toMonth Time.utc date)
        ++ " "
        ++ String.fromInt (Time.toDay Time.utc date)


toMonth : Time.Month -> String
toMonth month =
    case month of
        Jan ->
            "Janaury"

        Feb ->
            "February"

        Mar ->
            "March"

        Apr ->
            "April"

        May ->
            "May"

        Jun ->
            "Jun"

        Jul ->
            "July"

        Aug ->
            "August"

        Sep ->
            "September"

        Oct ->
            "October"

        Nov ->
            "Nov"

        Dec ->
            "December"


viewScore : Indication -> RemoteData Http.Error (Maybe Score.Score) -> Element Msg
viewScore indication rd =
    case rd of
        NotAsked ->
            row [] [ text "" ]

        Loading ->
            row [ width fill, paddingXY 0 32, spaceEvenly ]
                [ el [ width fill, height fill ]
                    (el
                        [ centerX
                        , width (fill |> maximum 150)
                        ]
                        (html (Scale.view "" Nothing))
                    )
                , el [ width fill, height fill ]
                    (el [ centerX, centerY ] (text ""))
                ]

        Failure err ->
            column [ width fill, height fill ]
                [ el [ width fill, alignBottom ] closeButton ]

        Success Nothing ->
            -- row [] [ text "err" ]
            column [ width fill, height fill ]
                [ el [ width fill, alignBottom ] screenersButton ]

        Success (Just score) ->
            viewIndication indication score



-- (toMeasure indication int)


toMeasure : Indication -> Int -> Measure
toMeasure indication =
    case indication of
        Score ->
            scoreMeasure

        Depression ->
            phq9Measure

        Anxiety ->
            gad7Measure

        Psychosomatic ->
            phq15Measure


viewIndication : Indication -> Score.Score -> Element Msg
viewIndication indication score =
    case toPoints indication score of
        Just int ->
            column [ height fill, width fill, spacing 32 ]
                [ row [ width fill, paddingXY 0 32, spaceEvenly ]
                    [ toMax indication
                        |> Scale.viewIndication int
                        |> html
                        |> el
                            [ centerX
                            , width (fill |> maximum 150)
                            ]
                        |> el [ width fill, height fill ]
                    , el [ width fill, height fill ]
                        (paragraph [ centerX, centerY, Font.bold, Font.center ]
                            [ text (toShort indication int) ]
                        )
                    ]
                , el [ width fill, paddingXY 0 16 ]
                    (paragraph []
                        [ text (toDescription indication int)
                        ]
                    )
                , el [ width fill, alignBottom ]
                    closeButton
                ]

        Nothing ->
            el [ width fill, alignBottom ]
                screenersButton


toPoints : Indication -> Score.Score -> Maybe Int
toPoints indication =
    case indication of
        Score ->
            Just << .score

        Depression ->
            Maybe.map (\x -> 100 - x) << .depressionScale

        Anxiety ->
            Maybe.map (\x -> 100 - x) << .anxietyScale

        Psychosomatic ->
            Maybe.map (\x -> 100 - x) << .somaticScale


viewTabs : Tab -> Element Msg
viewTabs status =
    row [ width fill, paddingXY 16 8, spaceEvenly ]
        (List.map (viewTab status) [ Current, History ])


viewTab : Tab -> Tab -> Element Msg
viewTab current tab =
    let
        attrs =
            if current == tab then
                [ Background.color Color.secondary
                , Font.color Color.primary
                , Border.color Color.primary
                ]

            else
                [ Border.color Color.white
                , onClick (ChangedTab tab)
                ]
    in
    el
        ([ centerX
         , width fill
         , padding 8
         , Font.center
         , Border.solid
         , Border.widthEach
            { bottom = 1
            , left = 0
            , right = 0
            , top = 0
            }
         ]
            ++ attrs
        )
        (text (tabToString tab))



-- EXPORT


toSession : Model -> Session
toSession model =
    model.session
