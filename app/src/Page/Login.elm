module Page.Login exposing (..)

import Auth exposing (..)
import Browser exposing (Document, application)
import Browser.Navigation as Nav
import Element as El exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as EInput
import Element.Region as Region
import Email exposing (EmailAddress)
import Env exposing (endpoint)
import FontAwesome.Attributes as IconAttr
import FontAwesome.Brands exposing (google)
import FontAwesome.Icon as Icon exposing (Icon)
import FontAwesome.Solid exposing (arrowLeft, stethoscope)
import Http
import Image
import Json.Decode as JD
import Json.Encode as JE
import Localizations.Languages as Languages exposing (Language)
import Localizations.Login as Ln
import Profile exposing (Profile)
import RemoteData exposing (RemoteData(..))
import Route
import Session exposing (Session)
import UI.Color as Color
import UI.Input as Input
import UI.Page as Page
import Url exposing (Protocol(..), Url)
import Url.Parser.Query as Query
import Viewer



-- Model


type alias Model =
    { -- redirectUri : Url
      -- ,
      error : Maybe String

    -- , token : Maybe OAuth.Token
    , language : Language
    , navKey : Nav.Key

    -- , state : String
    -- , session : Session
    , email : String
    , password : String
    , rememberMe : Bool
    , user : RemoteData Error Profile
    , isResetPassword : Bool
    , resetPassword : RemoteData ResetPasswordError Bool
    }


type ResetPasswordError
    = ResetPasswordValidation
    | ResetPasswordHttpError


type Error
    = CreateGuestError
    | CreateAccountError String
    | CreateAccountHttpError


makeInitModel : Nav.Key -> Language -> Model
makeInitModel navKey language =
    { -- redirectUri = { origin | query = Nothing, fragment = Nothing }
      -- ,
      error = Nothing

    -- , token = Nothing
    , language = language
    , navKey = navKey

    -- , state = bytes
    -- , session = session
    , email = ""
    , password = ""
    , rememberMe = False
    , user = NotAsked
    , isResetPassword = False
    , resetPassword = NotAsked
    }


resetModel : Model -> Model
resetModel
    { -- redirectUri,
      navKey
    , language
    }
    =
    { -- redirectUri = redirectUri
      -- ,
      error = Nothing

    -- , token = Nothing
    , language = language
    , navKey = navKey

    -- , state = state
    -- , session = session
    , email = ""
    , password = ""
    , rememberMe = False
    , user = NotAsked
    , isResetPassword = False
    , resetPassword = NotAsked
    }



--
-- Init
--


init : Nav.Key -> Language -> ( Model, Cmd Msg )
init navKey language =
    let
        model =
            makeInitModel navKey language
    in
    ( model, Cmd.none )



--
-- View
--


view : Model -> { title : String, content : Element Msg }
view model =
    { title = Ln.translate model.language Ln.Title
    , content = content model
    }


content : Model -> Element Msg
content model =
    -- let
    --     googleLogin =
    --         SignInRequested <| configurationFor Google
    -- in
    let
        ln =
            Ln.translate model.language
    in
    El.column
        [ El.width El.fill
        , El.height El.fill
        , Font.color Color.black
        , Font.size 14
        , inFront (viewNotification model)
        , padding 16
        ]
        [ Page.viewHeaderLayout
            (viewBack model.isResetPassword)
            (ln Ln.Header)
            none
        , if model.isResetPassword then
            viewResetPassword ln model

          else
            viewMain ln model
        ]


viewBack : Bool -> Element Msg
viewBack isResetPassword =
    if isResetPassword then
        Page.viewArrow arrowLeft ToggleResetPassword

    else
        none


viewResetPassword : (Ln.Id -> String) -> Model -> Element Msg
viewResetPassword ln model =
    case model.resetPassword of
        NotAsked ->
            viewResetPasswordNotAsked ln model none

        Success _ ->
            column [ width fill, height fill ]
                [ paragraph [ centerY ] [ text (ln Ln.CheckYourEmail) ]
                , el [ width fill, alignBottom ] (Input.buttonPrimary (ln Ln.GoBackToTheLoginPage) Nothing ToggleResetPassword)
                ]

        Loading ->
            Page.loading

        Failure err ->
            (case err of
                ResetPasswordHttpError ->
                    ln Ln.HttpError

                ResetPasswordValidation ->
                    ln Ln.EmailIsNotValid
            )
                |> text
                |> List.singleton
                |> paragraph [ width fill, Font.color Color.alert ]
                |> viewResetPasswordNotAsked ln model


viewResetPasswordNotAsked : (Ln.Id -> String) -> Model -> Element Msg -> Element Msg
viewResetPasswordNotAsked ln model viewError =
    column
        [ width fill
        , height fill
        , spacing 16
        ]
        [ column [ padding 16, width fill, centerY, spacing 16 ]
            [ paragraph [ width fill, centerY ] [ text (ln Ln.ResetPasswordLabel) ]
            , Input.email (ln Ln.Email) model.email ChangedEmail
            , viewError
            ]
        , el
            [ El.width El.fill, alignBottom ]
            (Input.buttonPrimary (ln Ln.ResetPasswordButton) Nothing (ResetPasswordClicked model.email))
        ]


viewMain : (Ln.Id -> String) -> Model -> Element Msg
viewMain ln model =
    El.column
        [ El.width El.fill
        , El.padding 16
        , spacing 16
        ]
        [ el [ Font.bold, centerX ]
            (text (ln Ln.Description))
        , El.column
            [ El.width El.fill
            , spacing 16
            ]
            [ Input.buttonPrimary (ln Ln.QuickDiagnosis) (Just stethoscope) DiagnosisAsGuest

            -- , Input.buttonSecondary "CONTINUE WITH GOOGLE" (Just google) googleLogin
            ]
        , row [ width fill, height (px 30) ]
            [ middleBorder
            , el
                [ height fill, width (px 50) ]
                (el
                    [ centerY, centerX ]
                    (text (ln Ln.OR))
                )
            , middleBorder
            ]
        , El.column
            [ El.width El.fill
            , spacing 16
            ]
            [ Input.email (ln Ln.Email) model.email ChangedEmail
            , Input.currentPassword (ln Ln.Password) model.password ChangedPassword
            ]
        , El.row
            [ El.width El.fill
            , spacing 16
            ]
            [ --  Input.checkbox "Remember Me" model.rememberMe ToggledRememberMe
              -- ,
              Input.buttonPrimary (ln Ln.Login) Nothing Login
            ]
        , el [ centerX ]
            (Input.buttonSecondaryLink ToggleResetPassword (ln Ln.ForgotPassword))
        , Page.border
        , El.column
            [ width fill
            , spacing 8
            ]
            [ el [ Font.bold, centerX ] (text (ln Ln.DontHave))
            , Input.buttonSecondary (ln Ln.Join) Nothing ChangeToRegister
            ]
        , Page.border

        -- , paragraph [ Font.size 10 ]
        --     [ text "If you click \"Log in with Google\" and are not a MentalScore user, you will be registered and you agree to MentalScore's "
        --     , el [ Font.color Color.primary ] (text "Terms & Conditions")
        --     , text "and"
        --     , el [ Font.color Color.primary ] (text "Privacy Policy.")
        --     ]
        ]


viewNotification : Model -> Element Msg
viewNotification model =
    case model.user of
        Failure error ->
            case error of
                CreateGuestError ->
                    Page.generalError Reset DiagnosisAsGuest

                CreateAccountHttpError ->
                    Page.generalError Reset Login

                CreateAccountError message ->
                    Page.notification message

        Loading ->
            Page.loading

        NotAsked ->
            none

        Success _ ->
            none


middleBorder : Element msg
middleBorder =
    el
        [ width fill
        , height (px 1)
        , Border.color Color.gray
        , Border.widthEach
            { bottom = 0
            , left = 0
            , right = 0
            , top = 1
            }
        ]
        (text "")



--
-- Msg
--


type Msg
    = NoOp
      -- | SignInRequested OAuthConfiguration
      -- | SignOutRequested
      -- | GotUserInfo (Result Http.Error Profile)
    | DiagnosisAsGuest
    | Login
      -- | Register
    | ChangedEmail String
    | ChangedPassword String
    | ToggledRememberMe Bool
    | GotGuest (Result Http.Error Profile)
    | GotAccount (Result Http.Error (Result String Profile))
    | Reset
    | ChangeToRegister
    | ToggleResetPassword
    | ResetPasswordClicked String
    | GotResetPassword (Result Http.Error Bool)



--
-- Update
--


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        -- SignInRequested { clientId, authorizationEndpoint, provider, scope } ->
        --     let
        --         auth =
        --             { clientId = clientId
        --             , redirectUri = model.redirectUri
        --             , scope = scope
        --             , state = Just (makeState model.state provider)
        --             , url = authorizationEndpoint
        --             }
        --     in
        --     ( model
        --     , auth |> OAuth.Implicit.makeAuthorizationUrl |> Url.toString |> Navigation.load
        --     )
        DiagnosisAsGuest ->
            ( { model | user = Loading }
            , createGuest model.language
            )

        Login ->
            case Email.parse model.email of
                Ok email ->
                    ( { model | user = Loading }
                    , login email model.password
                    )

                Err _ ->
                    ( model, Cmd.none )

        -- Register ->
        --     ( model
        --     , Route.replaceUrl (.navKey model.session) Route.Register
        --     )
        GotGuest (Ok profile) ->
            let
                modelNew =
                    { model | user = Success profile }
            in
            ( modelNew
            , Cmd.batch
                [ Route.replaceUrl model.navKey Route.Home
                , toSession modelNew
                    |> Maybe.map Viewer.store
                    |> Maybe.withDefault Cmd.none
                ]
            )

        GotGuest (Err data) ->
            ( { model | user = Failure CreateGuestError }
            , Cmd.none
            )

        GotAccount (Ok (Ok profile)) ->
            let
                modelNew =
                    { model | user = Success profile }
            in
            ( modelNew
            , Cmd.batch
                [ Route.replaceUrl model.navKey Route.Home
                , toSession modelNew
                    |> Maybe.map Viewer.store
                    |> Maybe.withDefault Cmd.none
                ]
            )

        GotAccount (Ok (Err message)) ->
            ( { model | user = Failure (CreateAccountError message) }
            , Cmd.none
            )

        GotAccount (Err data) ->
            ( { model | user = Failure CreateAccountHttpError }
            , Cmd.none
            )

        -- SignOutRequested ->
        --     ( model
        --     , Nav.load (Url.toString model.redirectUri)
        --     )
        -- GotUserInfo res ->
        --     case res of
        --         Err err ->
        --             ( { model | error = Just "Unable to fetch user profile ¯\\_(ツ)_/¯" }
        --             , Cmd.none
        --             )
        --         Ok profile ->
        --             ( { model | profile = Just profile }
        --             , Cmd.none
        --             )
        ChangedEmail email ->
            ( { model | email = email }, Cmd.none )

        ChangedPassword password ->
            ( { model | password = password }, Cmd.none )

        ToggledRememberMe bool ->
            ( { model | rememberMe = bool }, Cmd.none )

        Reset ->
            ( resetModel model, Cmd.none )

        ChangeToRegister ->
            ( model
            , Route.replaceUrl model.navKey Route.Register
            )

        ToggleResetPassword ->
            ( { model | isResetPassword = not model.isResetPassword }
            , Cmd.none
            )

        ResetPasswordClicked str ->
            case Email.parse model.email of
                Ok email ->
                    ( { model | resetPassword = Loading }
                    , resetPassword email
                    )

                Err _ ->
                    ( { model | resetPassword = Failure ResetPasswordValidation }, Cmd.none )

        GotResetPassword (Ok result) ->
            ( { model | resetPassword = Success result }, Cmd.none )

        GotResetPassword (Err error) ->
            ( { model | resetPassword = Failure ResetPasswordHttpError }, Cmd.none )


createGuest : Language -> Cmd Msg
createGuest language =
    Http.post
        { url = endpoint ++ "guests"
        , body =
            Http.jsonBody
                (JE.object [ ( "language", Languages.encode language ) ])
        , expect = Http.expectJson GotGuest Profile.guestDecoder
        }


login : EmailAddress -> String -> Cmd Msg
login email password =
    Http.post
        { url = endpoint ++ "signin"
        , body =
            Http.jsonBody
                (JE.object
                    [ ( "email", JE.string (Email.toString email) )
                    , ( "password", JE.string password )
                    ]
                )
        , expect = Http.expectJson GotAccount Profile.accountDecoder
        }


resetPassword : EmailAddress -> Cmd Msg
resetPassword email =
    Http.post
        { url = endpoint ++ "account/reset-password"
        , body =
            Http.jsonBody
                (JE.object
                    [ ( "email", JE.string (Email.toString email) ) ]
                )
        , expect =
            Http.expectJson
                GotResetPassword
                (JD.field "saved" JD.bool)
        }



--
-- Helpers
--
-- errorResponseToString : { error : OAuth.ErrorCode, errorDescription : Maybe String } -> String
-- errorResponseToString { error, errorDescription } =
--     let
--         code =
--             OAuth.errorCodeToString error
--         desc =
--             errorDescription
--                 |> Maybe.withDefault ""
--                 |> String.replace "+" " "
--     in
--     code ++ ": " ++ desc


oauthProviderToString : OAuthProvider -> String
oauthProviderToString provider =
    case provider of
        Google ->
            "google"


makeState : String -> OAuthProvider -> String
makeState suffix provider =
    oauthProviderToString provider ++ "." ++ suffix


randomBytesFromState : String -> String
randomBytesFromState str =
    str
        |> stringDropLeftUntil (\c -> c == ".")


stringDropLeftUntil : (String -> Bool) -> String -> String
stringDropLeftUntil predicate str =
    let
        ( h, q ) =
            ( String.left 1 str, String.dropLeft 1 str )
    in
    if q == "" || predicate h then
        q

    else
        stringDropLeftUntil predicate q



-- EXPORT


toSession : Model -> Maybe Session
toSession model =
    model.user
        |> RemoteData.toMaybe
        |> Maybe.map (Session.login model.navKey)
