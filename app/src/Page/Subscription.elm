module Page.Subscription exposing (..)

import Browser.Navigation as Nav
import Data.Plan as Plan exposing (Plan)
import Data.YKPayment as YKPayment exposing (YKPayment)
import Dict exposing (Dict)
import Element as El exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Events exposing (onClick)
import Element.Font as Font
import Element.Input as EInput
import Env exposing (endpoint)
import FontAwesome.Icon as Icon exposing (Icon)
import FontAwesome.Solid
    exposing
        ( angry
        , calendarAlt
        , check
        , cog
        , comment
        , fileAlt
        , signOutAlt
        , signature
        , spinner
        , stickyNote
        , theaterMasks
        )
import Html
import Html.Attributes
import Http
import Json.Decode as JD
import Json.Encode as JE
import Localizations.Subscription as Ln exposing (Ln)
import Profile
import RemoteData as RD exposing (..)
import Route
import Session exposing (Session)
import Task
import Time exposing (Posix)
import UI.Button as Button
import UI.Color as Color
import UI.Page as Page
import Viewer



-- MODEL


type alias Model =
    { session : Session
    , selected : Plan.Type
    , plans : List Plan.Plan
    , ykPayments : Dict String (RemoteData Http.Error (List YKPayment))
    , now : Maybe Time.Posix
    }


init : Session -> ( Model, Cmd Msg )
init session =
    ( { session = session
      , selected = Plan.MonthlyRecurring
      , plans = []
      , ykPayments = Dict.empty
      , now = Nothing
      }
    , [ getPlans session.profile.id GotResponseInitPlans
      , Task.perform GotTimeNow Time.now
      ]
        |> Cmd.batch
    )


enddate : List Plan -> List YKPayment -> Maybe Time.Posix
enddate plans ykPayments =
    ykPayments
        |> List.filter (\p -> p.status == YKPayment.Succeeded)
        |> List.map
            (\p ->
                (round <| toFloat p.createdAt / 1000000)
                    + (plans
                        |> List.filter (\x -> x.uuid == p.planUUID)
                        |> List.head
                        |> Maybe.map (Plan.toSeconds << .type_)
                        |> Maybe.withDefault 0
                        |> (*) 1000
                      )
            )
        |> List.maximum
        |> Maybe.map Time.millisToPosix



-- UPDATE


type Msg
    = NoOp
    | GotTimeNow Time.Posix
    | Redirect Route.Route
    | Toggled Plan.Type
    | GotResponseInitPlans (Result Http.Error (List Plan.Plan))
    | GotResponseInitYKPayments String (Result Http.Error (List YKPayment))
    | GotResponsePlans (Result Http.Error (List Plan.Plan))
    | GotResponseNewPlan (Result Http.Error Plan.Plan)
    | GotResponseYKPayments (Result Http.Error (List YKPayment.YKPayment))
    | GotResponseYKPayment (Result Http.Error YKPayment)
    | NextButtonClicked


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case Debug.log "msg: " msg of
        NoOp ->
            ( model
            , Cmd.none
            )

        GotTimeNow psx ->
            ( { model | now = Just psx }, Cmd.none )

        Toggled planType ->
            ( { model | selected = planType }, Cmd.none )

        NextButtonClicked ->
            ( model, getPlans model.session.profile.id GotResponsePlans )

        GotResponseInitPlans (Ok plans) ->
            ( { model
                | plans = plans
                , ykPayments =
                    plans
                        |> List.map .uuid
                        |> List.map (\x -> ( x, Loading ))
                        |> Dict.fromList
              }
            , plans
                |> List.map .uuid
                |> List.map (\uuid -> getYKPayments (GotResponseInitYKPayments uuid) uuid)
                |> Cmd.batch
            )

        GotResponseInitPlans (Err _) ->
            ( model, Cmd.none )

        GotResponseInitYKPayments uuid (Ok ykps) ->
            let
                ykPayments =
                    Dict.insert uuid (Success ykps) model.ykPayments

                model_ =
                    { model | ykPayments = ykPayments }
            in
            ( model_
            , if List.all isSuccess <| Dict.values ykPayments then
                let
                    enddate_ =
                        Dict.values ykPayments
                            |> List.filterMap RD.toMaybe
                            |> List.concat
                            |> enddate model_.plans
                in
                case ( enddate_, model_.now ) of
                    ( Just ed, Just n ) ->
                        if Time.posixToMillis ed > Time.posixToMillis n then
                            [ Viewer.store (toSession model_)
                            , Route.replaceUrl model.session.navKey Route.Doctor
                            ]
                                |> Cmd.batch

                        else
                            Cmd.none

                    _ ->
                        Cmd.none

              else
                Cmd.none
            )

        GotResponseInitYKPayments _ (Err _) ->
            ( model
            , Cmd.none
            )

        GotResponsePlans (Ok plans) ->
            ( { model | plans = plans }
            , case existingPlan model.selected plans of
                Just p ->
                    getYKPayments GotResponseYKPayments p.uuid

                Nothing ->
                    createPlan model.session.profile.id model.selected
            )

        GotResponsePlans (Err _) ->
            ( model, Cmd.none )

        GotResponseNewPlan (Ok plan) ->
            ( { model | plans = plan :: model.plans }
            , createYKPayment plan
            )

        GotResponseNewPlan (Err _) ->
            ( model, Cmd.none )

        GotResponseYKPayments (Ok ykPayments) ->
            ( model
            , case pendingYKPayments ykPayments of
                Just p ->
                    getYKPayment p.uuid

                Nothing ->
                    model.plans
                        |> existingPlan model.selected
                        |> Maybe.map createYKPayment
                        |> Maybe.withDefault Nav.reload
            )

        GotResponseYKPayments (Err err) ->
            ( model, Cmd.none )

        GotResponseYKPayment (Ok ykPayment) ->
            case ykPayment.status of
                YKPayment.Succeeded ->
                    ( model, Nav.reload )

                YKPayment.Pending ->
                    ( model, Nav.load ykPayment.confirmationURL )

                _ ->
                    ( model, Cmd.none )

        GotResponseYKPayment (Err err) ->
            ( model, Nav.reload )

        Redirect route ->
            ( model
            , Route.replaceUrl model.session.navKey route
            )


getPlans : String -> (Result Http.Error (List Plan.Plan) -> msg) -> Cmd msg
getPlans uuid toMsg =
    Http.get
        { url = endpoint ++ "accounts/" ++ uuid ++ "/plans"
        , expect = Http.expectJson toMsg <| JD.list Plan.decode
        }


getYKPayments : (Result Http.Error (List YKPayment) -> msg) -> String -> Cmd msg
getYKPayments toMsg uuid =
    Http.get
        { url = endpoint ++ "plans/" ++ uuid ++ "/yk-payments"
        , expect = Http.expectJson toMsg <| JD.list YKPayment.decode
        }


getYKPayment : String -> Cmd Msg
getYKPayment uuid =
    Http.get
        { url = endpoint ++ "yk-payments/" ++ uuid
        , expect = Http.expectJson GotResponseYKPayment <| YKPayment.decode
        }


createPlan : String -> Plan.Type -> Cmd Msg
createPlan uuid planType =
    Http.post
        { url = endpoint ++ "plans"
        , body =
            Http.jsonBody <|
                JE.object
                    [ ( "uuid", JE.string uuid )
                    , ( "type", Plan.encodeType planType )
                    ]
        , expect = Http.expectJson GotResponseNewPlan Plan.decode
        }


createYKPayment : Plan.Plan -> Cmd Msg
createYKPayment plan =
    Http.post
        { url = endpoint ++ "yk-payments"
        , body =
            Http.jsonBody <|
                JE.object
                    [ ( "uuid", JE.string plan.uuid )
                    , ( "type", Plan.encodeType plan.type_ )
                    ]
        , expect =
            Http.expectJson GotResponseYKPayment YKPayment.decode
        }


existingPlan : Plan.Type -> List Plan.Plan -> Maybe Plan.Plan
existingPlan planType =
    List.filter (\p -> p.type_ == planType)
        >> List.head


pendingYKPayments : List YKPayment -> Maybe YKPayment
pendingYKPayments xs =
    List.filter (\p -> p.status == YKPayment.Pending) xs
        |> List.head



-- VIEW


view : Model -> { title : String, content : Element Msg }
view model =
    let
        ln =
            Ln.translate model.session.profile.language
    in
    { title = ln Ln.Title
    , content = viewContent ln model
    }


viewContent : (Ln.Id -> String) -> Model -> Element Msg
viewContent ln model =
    El.column
        [ El.width El.fill
        , El.height El.fill
        , Font.color Color.black
        , Font.size 14
        , padding 16
        ]
        [ el [ width fill ] (Page.headerWithBack (Redirect Route.Profile) (ln Ln.Header))
        , -- case model.subscription of
          --   Just plan ->
          --       viewSecondStep ln plan
          --   Nothing ->
          viewFirstStep ln model
        ]


viewFirstStep : (Ln.Id -> String) -> Model -> Element Msg
viewFirstStep ln model =
    column [ spacing 16, width fill, height fill ]
        [ paragraph []
            [ el [ Font.bold ] (text (ln Ln.Step))
            , el [] (text " 1 ")
            , el [ Font.bold ] (text (ln Ln.Of))
            , el [] (text " 2")
            ]
        , paragraph [] [ Page.h3 (ln Ln.ChoosePlanTitle) ]
        , paragraph [] [ text (ln Ln.ChoosePlanDescription) ]
        , Plan.types
            |> List.map (viewPlan ln model.selected)
            |> row [ width fill, spacing 32 ]
        , el [ centerX, Font.size 12 ] (paragraph [] [ text (ln Ln.MonthlyPrice) ])
        , Plan.types
            |> List.map (viewPrice ln model.selected)
            |> row [ width fill, spacing 32 ]
        , Page.border
        , el [ centerX, Font.size 12, Font.center ] (paragraph [] [ text (ln Ln.IndividualSelfHelpProgram) ])
        , Plan.types
            |> List.map (viewTick model.selected)
            |> row [ width fill, spacing 32 ]
        , Page.border
        , el [ centerX, Font.size 12, Font.center ] (paragraph [] [ text (ln Ln.FullMentalHealthReport) ])
        , Plan.types
            |> List.map (viewTick model.selected)
            |> row [ width fill, spacing 32 ]
        , Page.border
        , el [ centerX, Font.size 12, Font.center ] (paragraph [] [ text (ln Ln.VideoConsultation) ])
        , Plan.types
            |> List.map (viewTick model.selected)
            |> row [ width fill, spacing 32 ]

        -- , Page.border
        -- , el [ centerX, Font.size 12 ] (paragraph [] [ text (ln Ln.VideoConsultation) ])
        -- , Plan.types
        --     |> List.map (viewVideoCons ln model.selected)
        --     |> row [ width fill, spacing 32 ]
        , Page.border
        , if List.any isLoading <| Dict.values model.ykPayments then
            viewNextLoading

          else
            viewNextButton (ln Ln.Continue)
        ]


viewNextLoading : Element Msg
viewNextLoading =
    el [ width fill, alignBottom ]
        (Button.view
            { label = ""
            , icon = Just spinner
            , onPress = NextButtonClicked
            , style = Button.Primary
            }
        )


viewNextButton : String -> Element Msg
viewNextButton label =
    el [ width fill, alignBottom ]
        (Button.view
            { label = label
            , icon = Nothing
            , onPress = NextButtonClicked
            , style = Button.Primary
            }
        )


viewPlan : (Ln.Id -> String) -> Plan.Type -> Plan.Type -> Element Msg
viewPlan ln active plan =
    column
        [ alpha
            (if active == plan then
                1

             else
                0.6
            )
        , width fill
        , height fill
        , onClick (Toggled plan)
        ]
        [ el
            [ width fill
            , height (px 65)
            , Background.color Color.primary
            , Font.color Color.white
            , Font.size 16
            , Font.bold
            , Border.rounded 5
            ]
            (el
                [ centerX, centerY ]
                (text (ln (Ln.PlanName plan)))
            )
        , if active == plan then
            triangle

          else
            none
        ]


triangle : Element msg
triangle =
    el
        [ centerX
        , htmlAttribute <| Html.Attributes.style "border-style" "solid"
        , htmlAttribute <| Html.Attributes.style "border-width" "15px 15px 0 15px"
        , htmlAttribute <| Html.Attributes.style "border-color" "#5db075 transparent transparent transparent"
        ]
        none


viewPrice : (Ln.Id -> String) -> Plan.Type -> Plan.Type -> Element Msg
viewPrice ln active plan =
    el
        [ if active == plan then
            Font.color Color.primary

          else
            Font.color Color.darkGray
        , width fill
        , height fill
        , onClick (Toggled plan)
        ]
        (el
            [ width fill
            , height fill
            , Font.size 16
            , Font.bold
            ]
            (el
                [ centerX, centerY ]
                (text (ln (Ln.PlanPrice plan)))
            )
        )


viewTick : Plan.Type -> Plan.Type -> Element Msg
viewTick active plan =
    el [ width fill, height fill, onClick (Toggled plan) ]
        (el
            [ width (px 18)
            , centerX
            , centerY
            , if active == plan then
                Font.color Color.primary

              else
                Font.color Color.darkGray
            ]
            (Icon.viewIcon check
                |> html
            )
        )


viewVideoCons : (Ln.Id -> String) -> Plan.Type -> Plan.Type -> Element Msg
viewVideoCons ln active plan =
    el
        [ if active == plan then
            Font.color Color.primary

          else
            Font.color Color.darkGray
        , width fill
        , height fill
        , onClick (Toggled plan)
        ]
        (el
            [ width fill
            , height fill
            , Font.size 16
            , Font.bold
            ]
            (el
                [ centerX, centerY ]
                (text (ln (Ln.PlanVideCons plan)))
            )
        )


viewSecondStep : (Ln.Id -> String) -> Plan.Type -> Element Msg
viewSecondStep ln plan =
    column [ spacing 16, width fill, height fill ]
        [ paragraph []
            [ el [ Font.bold ] (text (ln Ln.Step))
            , el [] (text " 2 ")
            , el [ Font.bold ] (text (ln Ln.Of))
            , el [] (text " 2")
            ]
        , paragraph [] [ Page.h3 (ln Ln.SetUpYourPayment) ]
        , paragraph [] [ text (ln Ln.PaymentDescription) ]
        , html <| Html.node "stripe-checkout" [] []
        ]



-- EXPORT


toSession : Model -> Session
toSession model =
    let
        x =
            Dict.values model.ykPayments
                |> List.filterMap RD.toMaybe
                |> List.concat
                |> enddate model.plans
    in
    case x of
        Just enddate_ ->
            model.session
                |> Session.updateProfile (Profile.setSubscriptionEnd enddate_)

        Nothing ->
            model.session
