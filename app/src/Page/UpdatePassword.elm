module Page.UpdatePassword exposing (..)

import Browser.Navigation as Nav
import Element as El exposing (..)
import Element.Font as Font
import Element.Input as EInput
import Env exposing (endpoint)
import FontAwesome.Icon as Icon exposing (Icon)
import FontAwesome.Solid exposing (arrowLeft)
import Http
import Json.Decode as JD
import Json.Encode as JE
import Localizations.Languages as Languages exposing (Language)
import Localizations.UpdatePassword as Ln
import RemoteData exposing (RemoteData(..))
import Route
import Session exposing (Session)
import UI.Color as Color
import UI.Input as Input
import UI.Page as Page



-- MODEL


type alias Model =
    { password : String
    , key : String
    , navKey : Nav.Key
    , language : Language
    , updatePassword : RemoteData Http.Error Bool
    }


init : String -> Nav.Key -> Language -> ( Model, Cmd Msg )
init key navKey language =
    ( { password = ""
      , key = key
      , navKey = navKey
      , language = language
      , updatePassword = NotAsked
      }
    , Cmd.none
    )



-- UPDATE


type Msg
    = ChangedPassword String
    | PasswordUpdated String
    | ChangeToLogin
    | GotUpdatePassword (Result Http.Error Bool)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        ChangedPassword string ->
            ( { model | password = string }, Cmd.none )

        PasswordUpdated pwd ->
            ( { model | updatePassword = Loading }, updatePassword model.key pwd )

        ChangeToLogin ->
            ( model
            , Route.replaceUrl model.navKey Route.Login
            )

        GotUpdatePassword (Ok bool) ->
            ( { model | updatePassword = Success bool }, Cmd.none )

        GotUpdatePassword (Err error) ->
            ( { model | updatePassword = Failure error }, Cmd.none )


updatePassword : String -> String -> Cmd Msg
updatePassword key password =
    Http.post
        { url = endpoint ++ "account/update-password"
        , body =
            Http.jsonBody
                (JE.object
                    [ ( "uuid", JE.string key )
                    , ( "password", JE.string password )
                    ]
                )
        , expect =
            Http.expectJson
                GotUpdatePassword
                (JD.field "saved" JD.bool)
        }



-- VIEW


view : Model -> { title : String, content : Element Msg }
view model =
    { title = Ln.translate model.language Ln.Title
    , content = content model
    }


content : Model -> Element Msg
content model =
    let
        ln =
            Ln.translate model.language
    in
    El.column
        [ El.width El.fill
        , El.height El.fill
        , Font.color Color.black
        , Font.size 14
        , padding 16
        ]
        [ Page.viewHeaderLayout
            (EInput.button
                [ width (px 16)
                , height (px 16)
                , Font.color Color.gray
                , alignLeft
                ]
                { onPress = Just ChangeToLogin
                , label =
                    arrowLeft
                        |> Icon.viewIcon
                        |> html
                }
            )
            (ln Ln.Title)
            none
        , viewBody ln model
        ]


viewBody : (Ln.Id -> String) -> Model -> Element Msg
viewBody ln model =
    case model.updatePassword of
        NotAsked ->
            viewBodyNotAsked ln model

        Loading ->
            Page.loading

        Success True ->
            column [ width fill, height fill ]
                [ paragraph [ centerY ] [ text (ln Ln.PasswordHasBeenChanged) ]
                , el [ width fill, alignBottom ] (Input.buttonPrimary (ln Ln.GoBackToTheLoginPage) Nothing ChangeToLogin)
                ]

        Success False ->
            column [ width fill, height fill ]
                [ paragraph [ centerY ] [ text (ln Ln.PasswordHasNotUpdated) ]
                , el [ width fill, alignBottom ] (Input.buttonPrimary (ln Ln.GoBackToTheLoginPage) Nothing ChangeToLogin)
                ]

        Failure _ ->
            column [ width fill, height fill ]
                [ paragraph [ centerY ] [ text (ln Ln.GeneralError) ] ]


viewBodyNotAsked : (Ln.Id -> String) -> Model -> Element Msg
viewBodyNotAsked ln model =
    column [ width fill, height fill ]
        [ El.column
            [ El.width El.fill
            , El.padding 16
            , spacing 16
            , centerY
            ]
            [ el [ Font.bold, centerX ]
                (text (ln Ln.Label))
            , El.column
                [ El.width El.fill
                , spacing 16
                ]
                [ Input.currentPassword "" model.password ChangedPassword
                ]
            ]
        , El.row
            [ El.width El.fill
            , padding 16
            , alignBottom
            ]
            [ Input.buttonPrimary (ln Ln.Submit) Nothing (PasswordUpdated model.password) ]
        ]



-- SESSION


toSession : Model -> Maybe Session
toSession model =
    Nothing
