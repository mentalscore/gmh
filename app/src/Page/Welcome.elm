module Page.Welcome exposing (..)

import Data.Screeners as Screeners
import Dict exposing (Dict)
import Element as El exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Env exposing (endpoint)
import FontAwesome.Attributes as IconAttr
import FontAwesome.Brands exposing (google)
import FontAwesome.Icon as Icon exposing (Icon)
import FontAwesome.Solid exposing (bars, check)
import Html
import Http
import Json.Decode as JD
import Json.Encode as JE
import List.Extra as List
import Localizations.Welcome as Ln exposing (Ln)
import Profile as Profile exposing (Profile)
import Random exposing (generate)
import Route
import Session exposing (Session)
import Set exposing (Set)
import Task
import Test.Lusher as Lusher exposing (Color, Lusher)
import Time exposing (Posix)
import UI.Button as Button
import UI.Color as Color
import UI.Input as Input
import UI.Page as Page
import Url
import Viewer


{-| -- Примерный вариант списка вопросов к клиенту:
-- Что беспокоит и варианты ответа: 1. Личные проблемы 2.психосоматика 3.конфликты в семье 4. Плохое настроение 5. Проблемы с друзьями 6. Проблемы на работе 7. Проблемы с общением 8. Другое
-- Обращался ли за помощью 1. Самодиагностика 2.тесты 3.другое приложение 4.психолог 5. Психотерапевт
-- Есть ли потребность консультации специалиста и контроля за решением проблемы 1. Да 2. Нет 3. Не знаю
-}



-- MODEL


type alias Model =
    { session : Session
    , survey : Survey
    , colorSet1 : ( Set String, List Color )
    , colorSet2 : ( Set String, List Color )
    , random : Int
    , status : Status
    }


type Status
    = AnsweringSet1
    | AnsweringSurvey
    | AnsweringSet2


initSurvey : Survey
initSurvey =
    { questions =
        [ simple "name" "Will Barrow"
        , simple "age" "40"
        , radioOther "gender"
            [ "Male", "Female", "Prefer Not to Answer" ]
            "Not Listed"
        , radioOther "anxiety"
            [ "Personal life"
            , "Psychosomatics"
            , "Family"
            , "Mood"
            , "Friends"
            , "Work"
            , "Communication"
            , "Other Anxiety"
            ]
            "OtherAnxiety"
        , radioOther "treatment"
            [ "Self Diagnosis"
            , "Selft Tests"
            , "Mobile App"
            , "Psychotherapist"
            ]
            "Other"
        , radio "needHelp"
            [ "Yes"
            , "No"
            , "Not Sure"
            ]
        ]
    , answers = []
    , answer = Nothing
    }


toLabel : Ln -> String -> String
toLabel ln str =
    case str of
        "name" ->
            ln Ln.Name

        "age" ->
            ln Ln.Age

        "gender" ->
            ln Ln.Gender

        "anxiety" ->
            ln Ln.Anxiety

        "treatment" ->
            ln Ln.Treatment

        "needHelp" ->
            ln Ln.NeedHelp

        _ ->
            ""


simple : String -> String -> Question
simple str placeholder =
    { id = str
    , type_ = Input placeholder
    }


radio : String -> List String -> Question
radio str options =
    { id = str
    , type_ = TypeRadio (Radio options Nothing)
    }


radioOther : String -> List String -> String -> Question
radioOther str options other =
    { id = str
    , type_ = TypeRadio (Radio options (Just other))
    }


type alias Survey =
    { questions : List Question
    , answers : List ( Question, Answer )
    , answer : Maybe Answer
    }


surveySize { questions, answers } =
    List.length questions + List.length answers


surveyAnswered { answers } =
    List.length answers


type alias Question =
    { id : String
    , type_ : Type
    }


type Type
    = Input String
    | TypeRadio Radio


type alias Slider =
    { min : Float
    , max : Float
    }


type alias Radio =
    { options : List String
    , other : Maybe String
    }


type alias Answer =
    String


type Sex
    = Male
    | Female


type Anxiety
    = Personal
    | Psychosomatics
    | Family
    | Mood
    | Friends
    | Work
    | Communication
    | OtherAnxiety


type Treatment
    = SelfDiagnosis
    | SelftTests
    | MobileApp
    | Psychotherapist


type Help
    = Yes
    | No
    | NotSure


init : Session -> ( Model, Cmd Msg )
init session =
    ( { session = session
      , survey = initSurvey
      , colorSet1 = ( Lusher.colorsSet, [] )
      , colorSet2 = ( Lusher.colorsSet, [] )
      , random = 0
      , status = AnsweringSet1
      }
    , Random.int 1 8
        |> generate Random
    )


seconds : Int
seconds =
    5



-- UPDATE


type Msg
    = ChangedValue String
    | SetValue
    | SetValueWith String
    | Select1 Color
    | Select2 Color
    | Random Int
    | GotLusher (Result Http.Error Bool)
    | GotSurvey (Result Http.Error Bool)


changedValue survey value =
    { survey | answer = Just value }


setValue survey =
    case survey.questions of
        [] ->
            survey

        q :: qs ->
            case survey.answer of
                Just a ->
                    { questions = qs
                    , answers = ( q, a ) :: survey.answers
                    , answer = Nothing
                    }

                Nothing ->
                    survey


setValueWith a survey =
    case survey.questions of
        [] ->
            survey

        q :: qs ->
            { questions = qs
            , answers = ( q, a ) :: survey.answers
            , answer = Nothing
            }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        ChangedValue value ->
            ( { model | survey = changedValue model.survey value }, Cmd.none )

        SetValue ->
            let
                newModel =
                    { model | survey = setValue model.survey }
            in
            case newModel.survey.questions of
                [] ->
                    ( { newModel | status = AnsweringSet2 }
                    , Session.uuid newModel.session
                        |> savedSurvey newModel.survey
                    )

                _ ->
                    ( newModel, Cmd.none )

        SetValueWith answer ->
            let
                newModel =
                    { model | survey = setValueWith answer model.survey }
            in
            case newModel.survey.questions of
                [] ->
                    ( { newModel | status = AnsweringSet2 }
                    , Session.uuid newModel.session
                        |> savedSurvey newModel.survey
                    )

                _ ->
                    ( newModel, Cmd.none )

        Select1 color ->
            let
                newModel =
                    { model
                        | colorSet1 = updateColorSet color model.colorSet1
                    }
            in
            ( if isColorSetFinished newModel.colorSet1 then
                { newModel | status = AnsweringSurvey }

              else
                newModel
            , Cmd.none
            )

        Select2 color ->
            let
                newModel =
                    { model | colorSet2 = updateColorSet color model.colorSet2 }
            in
            if isColorSetFinished newModel.colorSet2 then
                ( newModel
                , Session.uuid model.session
                    |> createLusher
                        (Lusher.create
                            newModel.colorSet1
                            newModel.colorSet2
                        )
                )

            else
                ( newModel, Cmd.none )

        GotLusher _ ->
            -- TODO: Think about an error
            ( model
            , Cmd.batch
                [ Route.replaceUrl (.navKey model.session) Route.Screeners
                , Viewer.store (toSession model)
                ]
            )

        GotSurvey (Ok _) ->
            ( model, Cmd.none )

        GotSurvey (Err _) ->
            -- TODO: Think about an error
            ( model, Cmd.none )

        Random int ->
            ( { model | random = int }, Cmd.none )


updateColorSet : Color -> ( Set String, List Color ) -> ( Set String, List Color )
updateColorSet color ( set, list ) =
    ( set, color :: list )


createLusher : Lusher.Lusher -> String -> Cmd Msg
createLusher lusher uuid =
    Http.post
        { url = endpoint ++ "tests/lushers"
        , body =
            Http.jsonBody
                (JE.object
                    [ ( "uuid", JE.string uuid )
                    , ( "first", JE.string (String.join " , " lusher.first) )
                    , ( "second", JE.string (String.join " , " lusher.second) )
                    ]
                )
        , expect =
            Http.expectJson
                GotLusher
                (JD.field "saved" JD.bool)
        }


savedSurvey : Survey -> String -> Cmd Msg
savedSurvey survey uuid =
    let
        surveListEncode : List ( String, JE.Value )
        surveListEncode =
            List.map (\( { id }, answer ) -> ( id, JE.string answer )) survey.answers
    in
    Http.post
        { url = endpoint ++ "surveys/introductions"
        , body =
            Http.jsonBody
                (JE.object
                    (( "uuid", JE.string uuid ) :: surveListEncode)
                )
        , expect =
            Http.expectJson
                GotSurvey
                (JD.field "saved" JD.bool)
        }



-- VIEW


view : Model -> { title : String, content : Element Msg }
view model =
    { title = Ln.translate model.session.profile.language Ln.Title
    , content = content model
    }


content : Model -> Element Msg
content model =
    El.column
        [ El.width El.fill
        , El.height El.fill
        , Font.color Color.black
        , Font.size 14
        , padding 16
        ]
        (viewBody model)


isColorSetFinished : ( a, List b ) -> Bool
isColorSetFinished ( a, xs ) =
    List.length xs > 7


viewBody : Model -> List (Element Msg)
viewBody model =
    let
        translate =
            Ln.translate model.session.profile.language
    in
    case model.status of
        AnsweringSet1 ->
            viewLusher translate model.random Select1 1 model.colorSet1

        AnsweringSet2 ->
            viewLusher translate model.random Select2 2 model.colorSet2

        AnsweringSurvey ->
            viewBodySurvey model


viewLusher : Ln -> Int -> (Color -> msg) -> Int -> ( Set String, List Color ) -> List (Element msg)
viewLusher translate random msg partNumber colorSet =
    [ Page.header <| translate Ln.Header
    , column [ width fill, height fill ]
        [ column [ width fill ]
            [ paragraph []
                [ text <| translate Ln.ColorTest
                , el [ Font.color Color.primary ]
                    (text (String.fromInt partNumber))
                , text "/"
                , text "2"
                ]
            , paragraph [ paddingXY 0 16 ] [ text (translate Ln.LusherDescription) ]
            ]
        , el [ width fill, alignBottom ] (viewColorSet random msg partNumber colorSet)
        ]
    ]


viewColorSet : Int -> (Color -> msg) -> Int -> ( Set String, List Color ) -> Element msg
viewColorSet random msg partNumber ( set, list ) =
    set
        |> Set.toList
        |> List.filterMap Lusher.fromString
        |> (if partNumber == 1 then
                List.reverse

            else
                identity
           )
        |> List.greedyGroupsOf 2
        |> List.map
            (\group ->
                row
                    [ width fill
                    , spacing 10
                    , height (fill |> maximum 110)
                    , spaceEvenly
                    ]
                    (List.map
                        (\x ->
                            if List.member x list then
                                square Nothing (rgb255 255 255 255)

                            else
                                square (Just (msg x)) (Lusher.toRGB x)
                        )
                        group
                    )
            )
        |> column
            [ spacing 8
            , paddingXY 0 8
            , width fill
            , height fill
            ]


shuffleColorSet : Int -> List a -> List a
shuffleColorSet int list =
    list
        |> List.take int
        |> List.reverse
        |> (++) list
        |> List.drop int


viewBodySurvey : Model -> List (Element Msg)
viewBodySurvey model =
    let
        ln =
            Ln.translate model.session.profile.language
    in
    [ Page.header (ln Ln.SurveyHeader)
    , column [ width fill, height fill, spaceEvenly ]
        [ paragraph []
            [ text (ln Ln.Question)
            , el [ Font.color Color.primary ]
                (text (String.fromInt (surveyAnswered model.survey + 1)))
            , text "/"
            , text (String.fromInt (surveySize model.survey))
            ]
        , viewSurvey ln model.survey
        ]
    ]


viewSurvey : Ln -> Survey -> Element Msg
viewSurvey ln survey =
    case List.head survey.questions of
        Just question ->
            viewQuestion ln question survey.answer

        Nothing ->
            text "Success"


viewQuestion : Ln -> Question -> Maybe Answer -> Element Msg
viewQuestion ln question answer =
    case question.type_ of
        Input placeholder ->
            column [ width fill, height fill ]
                [ el [ centerY, centerX ]
                    (Input.text
                        [ centerX
                        , spacing 16
                        , Font.size 26
                        ]
                        { onChange = ChangedValue
                        , text =
                            answer
                                |> Maybe.withDefault ""
                        , placeholder =
                            Input.placeholder [ centerX, centerY ] (text (ln (Ln.Placeholder placeholder)))
                                |> Just
                        , label = Input.labelAbove [ Font.size 14 ] (text (toLabel ln question.id))
                        }
                    )
                , el [ width fill, alignBottom ] (Input.buttonPrimary (ln Ln.Submit) Nothing SetValue)
                ]

        TypeRadio { options } ->
            viewQuestionRadio ln options SetValueWith question


viewQuestionRadio : Ln -> List String -> (String -> msg) -> Question -> Element msg
viewQuestionRadio ln possible msg question =
    let
        viewAnswer response =
            Button.secondary (ln (Ln.PossibleAnswer response)) (msg response)
    in
    column [ width fill, height fill ]
        [ el [ paddingXY 0 16, centerY ]
            (paragraph [] [ Page.h2 (toLabel ln question.id) ])
        , column [ width fill, spacing 16, paddingXY 0 16, centerX, alignBottom ]
            (List.map viewAnswer possible)
        ]


square : Maybe msg -> El.Color -> Element msg
square msg color =
    el [ width fill, centerX ]
        (Input.button
            [ El.height (px 110)
            , El.width (px 110)
            , centerX
            ]
            { label =
                El.el
                    [ height fill
                    , width fill
                    , Background.color color
                    , Border.rounded 5
                    ]
                    El.none
            , onPress = msg
            }
        )


toSession : Model -> Session
toSession model =
    let
        name : a -> String
        name _ =
            model.survey.answers
                |> List.filter (\( q, a ) -> q.id == "name")
                |> List.head
                |> Maybe.map Tuple.second
                |> Maybe.withDefault ""
    in
    model.session
        |> Session.setScreener Screeners.initSubjective
        |> Session.updateProfile (Profile.updateName name)



-- | TypeSlider Slider
-- | Checkbox (List String)
-- slider : String -> Float -> Float -> Question
-- slider str min max =
--     { id = str
--     , type_ = TypeSlider (Slider min max)
--     }
-- TypeSlider { min, max } ->
--     let
--         value : Float
--         value =
--             answer
--                 |> Maybe.andThen List.head
--                 |> Maybe.andThen String.toFloat
--                 |> Maybe.withDefault 35.0
--     in
--     Input.slider
--         [ El.height (El.px 30)
--         , El.behindContent
--             (El.el
--                 [ El.width El.fill
--                 , El.height (El.px 2)
--                 , El.centerY
--                 , Border.rounded 2
--                 , Background.color Color.gray
--                 ]
--                 El.none
--             )
--         ]
--         { onChange = String.fromFloat >> ChangedValue
--         , label =
--             Input.labelAbove []
--                 (text (toLabel question.id ++ ": " ++ String.fromFloat value))
--         , min = min
--         , max = max
--         , step = Just 1
--         , value = value
--         , thumb = Input.defaultThumb
--         }
