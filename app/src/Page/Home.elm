module Page.Home exposing (..)

import Auth exposing (..)
import Data.Activities as Activities exposing (..)
import Data.Score as Score exposing (Score)
import Dict
import Element as El exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Events exposing (onClick)
import Element.Font as Font
import Element.Input as Input
import Env exposing (endpoint)
import FontAwesome.Attributes as IconAttr
import FontAwesome.Brands exposing (google)
import FontAwesome.Icon as Icon exposing (Icon)
import FontAwesome.Solid exposing (check, comment, stickyNote)
import Html
import Html.Attributes
import Http
import Json.Decode as JD
import Json.Encode as JE
import List.Extra as List
import Localizations.Activities as LnActivities
import Localizations.Home as Ln exposing (Ln)
import Localizations.Indication as LnIndication
import Localizations.Languages exposing (..)
import Ports
import Profile exposing (Profile)
import Route
import Session exposing (Session)
import UI.Button as Button
import UI.Color as Color
import UI.Page as Page
import UI.Scale as Scale
import Url
import Viewer



-- MODEL


type alias Model =
    { session : Session
    , score : Maybe Score
    , day : Day
    , isInstallOpen : Bool
    }


type alias Day =
    { isOpen : Bool
    , activities : List UserActivity
    }


type alias UserActivity =
    { id : Maybe Int
    , activity : Activity
    , isSelected : Bool
    }


initUserActivity : Activity -> UserActivity
initUserActivity activity =
    { id = Nothing
    , activity = activity
    , isSelected = False
    }


type State
    = Main
    | Disclaimer String


init : Session -> ( Model, Cmd Msg )
init session =
    ( { session = session
      , score = Nothing
      , day = initDay
      , isInstallOpen = False
      }
    , session.profile.id
        |> Score.get GotScore
    )



-- UPDATE


type Msg
    = Accepted String
    | GotAccepted (Result Http.Error Bool)
    | GotScore (Result Http.Error (List Score))
    | ActivitySelected UserActivity
    | GotActivityResponse Activity (Result Http.Error Int)
    | ActivityUnselected UserActivity
    | OpendActivities
    | GotActivitiesResponse (Result Http.Error (List UserActivity))
    | GotActivityDelete (Result Http.Error ())
    | ClosedDay
    | AppInstallClicked
    | AppDismissClicked
    | GotEventIn (Result JD.Error (Maybe Ports.EventIn))


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Accepted uuid ->
            ( model, acceptDisclaimer uuid )

        GotAccepted (Ok True) ->
            let
                session =
                    Session.acceptedDisclaimer model.session
            in
            ( { model | session = session }
            , Cmd.batch
                [ Route.replaceUrl (.navKey model.session) Route.Welcome
                , Viewer.store session
                ]
            )

        GotAccepted _ ->
            ( model, Cmd.none )

        GotScore (Ok scores) ->
            ( { model | score = List.head scores }
            , Cmd.none
            )

        GotScore _ ->
            ( model, Cmd.none )

        OpendActivities ->
            ( { model | day = updateDayOpen (\_ -> True) model.day }
            , Session.uuid model.session
                |> getActivities
            )

        ClosedDay ->
            ( { model | day = updateDayOpen (\_ -> False) model.day }, Cmd.none )

        ActivitySelected activity ->
            let
                add =
                    List.updateIf ((==) activity) (\a -> { a | isSelected = True })
            in
            ( { model
                | day =
                    model.day
                        |> updateActivities add
              }
            , Session.uuid model.session
                |> addActivity activity.activity
            )

        ActivityUnselected activity ->
            let
                remove =
                    List.updateIf ((==) activity) (\a -> { a | isSelected = False })
            in
            ( { model
                | day =
                    model.day
                        |> updateActivities remove
              }
            , activity.id
                |> Maybe.map (deleteActivity (Session.uuid model.session))
                |> Maybe.withDefault Cmd.none
            )

        GotActivityResponse a (Ok id) ->
            ( { model
                | day =
                    model.day
                        |> updateActivities (addIndex a id)
              }
            , Cmd.none
            )

        GotActivityResponse activity (Err _) ->
            -- TODO: Think about an error
            ( model, Cmd.none )

        GotActivitiesResponse (Ok activities) ->
            ( { model
                | day =
                    model.day
                        |> updateActivities (combine activities)
              }
            , Cmd.none
            )

        GotActivitiesResponse (Err _) ->
            -- TODO: Think about an error
            ( model, Cmd.none )

        GotActivityDelete _ ->
            -- TODO: Think about an error
            ( model, Cmd.none )

        AppInstallClicked ->
            ( { model | isInstallOpen = False }
            , Ports.toEventOut Ports.AppInstallClicked
            )

        AppDismissClicked ->
            ( { model | isInstallOpen = False }, Cmd.none )

        GotEventIn (Ok (Just result)) ->
            case result of
                Ports.ShowInstallPromotion ->
                    ( { model | isInstallOpen = True }, Cmd.none )

                Ports.NotificationsStatusReturned _ ->
                    ( model, Cmd.none )

        GotEventIn (Ok Nothing) ->
            ( model, Cmd.none )

        GotEventIn (Err err) ->
            ( model, Cmd.none )


getActivities : String -> Cmd Msg
getActivities uuid =
    Http.get
        { url = endpoint ++ "accounts/" ++ uuid ++ "/activities"
        , expect = Http.expectJson GotActivitiesResponse (JD.list decodeUserActivities)
        }


decodeUserActivities : JD.Decoder UserActivity
decodeUserActivities =
    JD.map3 UserActivity
        (JD.field "activityId" (JD.maybe JD.int))
        (JD.field "activityType" Activities.decode)
        (JD.succeed True)


addActivity : Activities.Activity -> String -> Cmd Msg
addActivity activity uuid =
    Http.post
        { url = endpoint ++ "activities"
        , body =
            Http.jsonBody (Activities.encode uuid activity)
        , expect =
            Http.expectJson (GotActivityResponse activity)
                (JD.field "id" JD.int)
        }


deleteActivity : String -> Int -> Cmd Msg
deleteActivity uuid id =
    Http.request
        { method = "DELETE"
        , headers = []
        , url = endpoint ++ "activities/" ++ String.fromInt id
        , body = Http.emptyBody
        , expect = Http.expectWhatever GotActivityDelete
        , timeout = Nothing
        , tracker = Nothing
        }


combine : List UserActivity -> (List UserActivity -> List UserActivity)
combine new current =
    List.uniqueBy (\a -> Activities.toString a.activity)
        (new ++ current)


addIndex : Activity -> Int -> (List UserActivity -> List UserActivity)
addIndex activity id =
    List.updateIf (\a -> a.activity == activity)
        (\a ->
            { a
                | id = Just id
                , isSelected = True
            }
        )


initDay : Day
initDay =
    { isOpen = False
    , activities =
        Activities.activities
            |> List.map initUserActivity
    }


updateActivities : (List UserActivity -> List UserActivity) -> Day -> Day
updateActivities f day =
    { day | activities = f day.activities }


updateDayOpen : (Bool -> Bool) -> Day -> Day
updateDayOpen f day =
    { day | isOpen = f day.isOpen }


acceptDisclaimer : String -> Cmd Msg
acceptDisclaimer uuid =
    Http.post
        { url = endpoint ++ "acceptance-disclaimer"
        , body =
            Http.jsonBody
                (JE.object [ ( "uuid", JE.string uuid ) ])
        , expect =
            Http.expectJson GotAccepted
                (JD.field "disclaimer" JD.bool)
        }



-- View


view : Model -> { title : String, content : Element Msg }
view model =
    { title = Ln.translate model.session.profile.language Ln.HomePageTitle
    , content = content model
    }


content : Model -> Element Msg
content model =
    case model.session.profile.disclaimer of
        True ->
            viewMain model

        False ->
            viewDisclaimer (Ln.translate model.session.profile.language) model.session.profile.id


viewLoading : Element Msg
viewLoading =
    column
        [ centerY
        , centerX
        , spacing 20
        , padding 20
        , width fill
        ]
        [ el
            [ centerX, Font.size 32, Font.color Color.black ]
            (text "Loading")
        ]


viewError : Element Msg -> Element Msg
viewError message =
    column
        [ centerY
        , centerX
        , spacing 20
        , padding 20
        , width fill
        ]
        [ el
            [ centerX, Font.size 32, Font.color Color.black ]
            message
        ]


viewMain : Model -> Element Msg
viewMain model =
    let
        ln =
            Ln.translate model.session.profile.language

        lnIndication =
            LnIndication.translate model.session.profile.language
    in
    El.column
        [ width fill
        , height fill
        , inFront (viewMenuOrDialog ln (LnActivities.translateActivity model.session.profile.language) model.day)
        ]
        [ column
            [ width fill
            , height fill
            , paddingEach { bottom = 70, left = 16, right = 16, top = 16 }
            ]
            [ Page.header "MentalScore"
            , viewScores ln model.score
            , model.score
                |> Maybe.map (viewIndication lnIndication)
                |> Maybe.withDefault none
                |> el [ width fill, centerY ]
            , column
                [ width fill
                , alignBottom
                , spacing 16
                , paddingXY 0 16
                ]
                [ -- viewHowWasYourDay "Daily Notes" stickyNote
                  -- ,
                  Button.action (ln Ln.HowWasYourDay) comment OpendActivities
                ]
            ]
        , if model.isInstallOpen then
            viewInstall ln

          else
            none
        ]


viewInstall : (Ln.Id -> String) -> Element Msg
viewInstall ln =
    el
        [ htmlAttribute (Html.Attributes.style "position" "fixed")
        , htmlAttribute (Html.Attributes.style "bottom" "0")
        , htmlAttribute (Html.Attributes.style "left" "0")
        , htmlAttribute (Html.Attributes.style "z-index" "1")
        , width fill
        , Background.color Color.white
        , Border.roundEach
            { topLeft = 16
            , topRight = 16
            , bottomLeft = 0
            , bottomRight = 0
            }
        , Border.shadow
            { offset = ( 0, 0 )
            , size = 0
            , blur = 20
            , color = El.rgba 0 0 0 0.25
            }
        , Background.color Color.white
        , padding 16
        ]
        (column
            [ width fill
            , spacing 16
            ]
            [ column
                [ width fill, padding 16, spacing 16 ]
                [ el [ width fill, Font.center, Font.size 20 ]
                    (paragraph [] [ text <| ln Ln.InstallTitle ])
                , el [ width fill, Font.size 14 ]
                    (paragraph [] [ text <| ln Ln.InstallDescription ])
                ]
            , el [ width fill, Font.size 18 ]
                (Input.button [ width fill, Font.center, Font.color Color.darkGray ]
                    { onPress = Just AppDismissClicked
                    , label = text (ln Ln.InstallDismiss)
                    }
                )
            , el [ width fill ]
                (Button.primary (ln Ln.InstallOK) check AppInstallClicked)
            ]
        )


viewMenuOrDialog ln translate day =
    case day.isOpen of
        True ->
            viewActivitiesDialog ln translate day.activities

        False ->
            Page.bottomBar Page.Home


viewActivitiesDialog : Ln -> (Activity -> String) -> List UserActivity -> Element Msg
viewActivitiesDialog ln translate selected =
    column
        [ width fill
        , height fill
        , padding 16
        , Background.color Color.white
        ]
        [ Page.header (ln Ln.Activities)
        , viewActivities translate selected
        , el [ width fill, alignBottom ] (Button.secondary (ln Ln.ActivitiesOK) ClosedDay)
        ]


viewActivities : (Activity -> String) -> List UserActivity -> Element Msg
viewActivities translate activities =
    activities
        |> List.uniqueBy (Activities.toString << .activity)
        |> List.sortBy (Activities.toString << .activity)
        |> List.greedyGroupsOf 4
        |> List.map
            (\group ->
                row [ width fill, spacing 10, height fill ]
                    (List.map (viewActivity translate) group)
            )
        |> column
            [ paddingXY 0 16
            , spacing 16
            , width fill
            ]


viewActivity : (Activity -> String) -> UserActivity -> Element Msg
viewActivity translate activity =
    let
        color =
            if activity.isSelected then
                Color.primary

            else
                Color.gray

        msg =
            if activity.isSelected then
                ActivityUnselected activity

            else
                ActivitySelected activity
    in
    column
        [ width fill
        , height fill
        , onClick msg
        , Font.color color
        ]
        [ column
            [ padding 16, width fill ]
            [ toIcon activity.activity
                |> Icon.viewIcon
                |> html
                |> el
                    [ width (px 32)
                    , height (px 32)
                    , centerX
                    ]
            ]
        , el [ padding 4, width fill, centerX, centerY ]
            (paragraph
                [ Font.size 12
                , width fill
                , Font.center
                , Font.color Color.black
                ]
                [ text (translate activity.activity) ]
            )
        ]


viewIndication : (LnIndication.Id -> String) -> Score -> Element Msg
viewIndication ln score =
    el [ paddingXY 16 32, width fill ]
        (paragraph [ Font.size 14, Font.color Color.black ]
            [ el [] (text (ln (LnIndication.IndicationGeneral score.score)))
            ]
        )


viewScores : Ln -> Maybe Score -> Element Msg
viewScores ln score =
    let
        row_ =
            row [ height fill, width fill, spacing 16, centerY ]
    in
    column [ paddingXY 16 0, width fill, spacing 16 ]
        [ el
            [ centerX
            , width (fill |> maximum 150)
            ]
            (score
                |> Maybe.map .score
                |> Scale.view (ln Ln.Points)
                |> html
            )
        , row_
            [ viewScoreEl (ln Ln.Depression) .depressionScale score
            , viewScoreEl (ln Ln.Anxiety) .anxietyScale score
            ]
        , row_
            [ viewScoreEl (ln Ln.Psychosomatic) .somaticScale score
            , viewScoreEl (ln Ln.Subjective) .subjectiveScale score
            ]
        ]


viewScoreEl : String -> (Score -> Maybe Int) -> Maybe Score -> Element Msg
viewScoreEl label toPoints score =
    score
        |> Maybe.andThen toPoints
        |> Scale.bar label
        |> html
        |> el [ height (px 35), width (px 150), centerX, centerY ]


viewDisclaimer : (Ln.Id -> String) -> String -> Element Msg
viewDisclaimer translate uuid =
    Page.container
        [ Page.header (translate Ln.Disclaimer)
        , column [ width fill, height fill ]
            [ column [ spacing 16 ]
                [ paragraph [] [ (text << translate) Ln.DisclaimerParagraph1 ]
                , paragraph [] [ (text << translate) Ln.DisclaimerParagraph2 ]
                ]
            , el [ width fill, alignBottom ]
                (Button.primary (translate Ln.DisclaimerAccept) check (Accepted uuid))
            ]
        ]



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch [ Ports.sub GotEventIn ]



-- EXPORT


toSession : Model -> Session
toSession model =
    model.session
