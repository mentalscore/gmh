module Page.Lusher exposing (..)

import Dict
import Element as El exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import FontAwesome.Attributes as IconAttr
import FontAwesome.Icon as Icon exposing (Icon)
import FontAwesome.Solid exposing (bars)
import Html
import Http
import Localization as T
import Route
import Session exposing (Session)
import Set exposing (Set)
import Task
import Test.Lusher as Lusher exposing (Lusher)
import Time exposing (Posix)
import UI.Color as Color
import UI.Page as Page
import Url
import Viewer



-- MODEL


type alias Model =
    { session : Session
    , countdown : Maybe Int
    , colorSet1 : ( Set String, List Lusher.Color )
    , colorSet2 : ( Set String, List Lusher.Color )
    }


init : Session -> ( Model, Cmd msg )
init session =
    ( { session = session
      , colorSet1 = ( Lusher.colorsSet, [] )
      , countdown = Nothing
      , colorSet2 = ( Lusher.colorsSet, [] )
      }
    , Cmd.none
    )



-- Wait


seconds : Int
seconds =
    5



-- UPDATE


type Msg
    = Select1 Lusher.Color
    | Select2 Lusher.Color
    | Tick


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Select1 color ->
            let
                colorSet1 =
                    updateColorSet color model.colorSet1
            in
            if (List.length <| Tuple.second colorSet1) > 7 then
                ( { model
                    | colorSet1 = colorSet1
                    , countdown = Just seconds
                  }
                , Cmd.none
                )

            else
                ( { model | colorSet1 = colorSet1 }, Cmd.none )

        Select2 color ->
            let
                colorSet2 =
                    updateColorSet color model.colorSet2
            in
            if (List.length <| Tuple.second colorSet2) > 7 then
                ( { model
                    | colorSet2 = colorSet2
                  }
                , Cmd.batch
                    [ Route.replaceUrl (.navKey model.session) Route.Home
                    ]
                )

            else
                ( { model | colorSet2 = colorSet2 }
                , Cmd.none
                )

        Tick ->
            ( { model | countdown = Maybe.map (\t -> t - 1) model.countdown }
            , Cmd.none
            )


updateColorSet : Lusher.Color -> ( Set String, List Lusher.Color ) -> ( Set String, List Lusher.Color )
updateColorSet color ( set, list ) =
    ( set, color :: list )



-- View


view : Model -> { title : String, content : Element Msg }
view model =
    let
        translate =
            T.translate T.English
    in
    { title = translate T.LusherPageTitle
    , content = content (El.text << translate) model
    }


content : (T.Id -> Element Msg) -> Model -> Element Msg
content translate model =
    El.column
        [ El.width El.fill
        , El.height El.fill
        , Font.color Color.black
        , Font.size 14
        , padding 16
        ]
        (viewBody translate model)


viewBody : (T.Id -> Element Msg) -> Model -> List (Element Msg)
viewBody translate model =
    case model.countdown of
        Just int ->
            if int > 0 then
                countdown translate int

            else
                viewLusher 2 Select2 2 model.colorSet2

        Nothing ->
            viewLusher 1 Select1 1 model.colorSet1


viewLusher : Int -> (Lusher.Color -> msg) -> Int -> ( Set String, List Lusher.Color ) -> List (Element msg)
viewLusher random msg partNumber colorSet =
    [ Page.header "Lüscher"
    , column [ width fill, height fill ]
        [ column [ width fill ]
            [ paragraph []
                [ text "Color Test: "
                , el [ Font.color Color.primary ]
                    (text (String.fromInt partNumber))
                , text "/"
                , text "2"
                ]
            , paragraph [ paddingXY 0 16 ] [ text "Choose the color you like the most at the moment. Do not associate it with objects, your surrounding or your favorite color – just the color you like the most." ]
            ]
        , viewColorSet random msg partNumber colorSet
        ]
    ]


viewColorSet : Int -> (Lusher.Color -> msg) -> Int -> ( Set String, List Lusher.Color ) -> Element msg
viewColorSet random msg partNumber colorSet =
    El.wrappedRow [ El.spacing 16 ]
        (colorSet
            |> (\( set, list ) ->
                    set
                        |> Set.toList
                        |> List.filterMap Lusher.fromString
                        |> (if partNumber == 1 then
                                List.reverse

                            else
                                identity
                           )
                        |> List.map
                            (\x ->
                                if List.member x list then
                                    square Nothing Color.white

                                else
                                    square (Just (msg x)) (Lusher.toRGB x)
                            )
               )
        )


countdown : (T.Id -> Element Msg) -> Int -> List (Element Msg)
countdown translate sec =
    [ El.column
        [ El.centerY, El.centerX ]
        [ El.el
            [ El.centerX
            , Font.size 32
            , Font.color Color.black
            ]
            (El.text (String.fromInt sec ++ "sec"))
        , El.el
            [ Font.size 22, El.centerX, Font.color Color.black ]
            (translate T.LusherPleaseWait)
        ]
    ]


square : Maybe msg -> Color -> Element msg
square msg color =
    Input.button
        [ El.height (El.px 125)
        , El.width (El.px 125)
        , El.focused [ Border.glow (El.rgb 0 0 0) 0 ]
        ]
        { label =
            El.el
                [ El.height fill
                , El.width fill
                , El.spacing 15
                , Background.color color
                ]
                El.none
        , onPress = msg
        }



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    case model.countdown of
        Just _ ->
            Time.every 1000 (always Tick)

        Nothing ->
            Sub.none



-- EXPORT


toSession : Model -> Session
toSession model =
    model.session
