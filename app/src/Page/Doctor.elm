module Page.Doctor exposing (..)

import Element as El exposing (..)
import Element.Border as Border
import Element.Font as Font
import Env exposing (endpoint)
import FontAwesome.Solid exposing (diagnoses, idCardAlt)
import Http
import Json.Decode as JD
import Json.Encode as JE
import Localizations.Doctor as Ln exposing (Ln)
import Route
import Session exposing (Session)
import Task
import Time exposing (Posix)
import UI.Button as Button
import UI.Color as Color
import UI.Dialog as Dialog
import UI.Page as Page
import UI.Scale as Scale



-- MODEL


type alias Model =
    { session : Session
    , orderedReport : Bool
    , orderedProgram : Bool
    , now : Maybe Time.Posix
    }


init : Session -> ( Model, Cmd Msg )
init session =
    ( { session = session
      , orderedReport = False
      , orderedProgram = False
      , now = Nothing
      }
    , Task.perform GotTimeNow Time.now
    )



-- UPDATE


type Msg
    = OrderedReport
    | OrderedProgram
    | ClosedDialog
    | GotResponse (Result Http.Error Bool)
    | GotTimeNow Time.Posix


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GotTimeNow psx ->
            ( { model | now = Just psx }, Cmd.none )

        OrderedReport ->
            ( { model | orderedReport = True }
            , orderReport model.session.profile.id
            )

        OrderedProgram ->
            ( { model | orderedProgram = True }
            , orderProgram model.session.profile.id
            )

        ClosedDialog ->
            ( { model
                | orderedReport = False
                , orderedProgram = False
              }
            , Cmd.none
            )

        GotResponse _ ->
            ( model, Route.replaceUrl model.session.navKey Route.Subscription )


orderReport : String -> Cmd Msg
orderReport uuid =
    Http.post
        { url = endpoint ++ "report"
        , body =
            Http.jsonBody <|
                JE.object [ ( "uuid", JE.string uuid ) ]
        , expect =
            Http.expectJson
                GotResponse
                (JD.field "saved" JD.bool)
        }


orderProgram : String -> Cmd Msg
orderProgram uuid =
    Http.post
        { url = endpoint ++ "program"
        , body =
            Http.jsonBody <|
                JE.object [ ( "uuid", JE.string uuid ) ]
        , expect =
            Http.expectJson
                GotResponse
                (JD.field "saved" JD.bool)
        }



-- VIEW


view : Model -> { title : String, content : Element Msg }
view model =
    { title = Ln.translate model.session.profile.language Ln.Title
    , content = content model
    }


content : Model -> Element Msg
content model =
    let
        ln =
            Ln.translate model.session.profile.language
    in
    -- if model.orderedProgram || model.orderedReport then
    --     dialogRegionNotSupported ln
    -- else
    El.column
        [ El.width El.fill
        , El.height El.fill
        , paddingEach { bottom = 70, left = 16, right = 16, top = 16 }
        , inFront (Page.bottomBar Page.Doctor)
        ]
        (viewBody ln model)


dialogRegionNotSupported : Ln -> Element Msg
dialogRegionNotSupported ln =
    Dialog.view
        { title = ln Ln.DialogTitle
        , content = ln Ln.DialogContent
        , label = ln Ln.DialogLabel
        , msg = ClosedDialog
        }


viewBody : Ln -> Model -> List (Element Msg)
viewBody ln model =
    case ( model.session.profile.subscriptionEnd, model.now ) of
        ( Just ed, Just n ) ->
            if Time.posixToMillis ed > Time.posixToMillis n then
                viewBodySubscription ln model

            else
                viewBodyNoSubscription ln model

        _ ->
            viewBodyNoSubscription ln model


viewBodySubscription : Ln -> Model -> List (Element Msg)
viewBodySubscription ln model =
    [ Page.header (ln Ln.Header)
    , row
        [ width fill ]
        [ newTabLink
            [ centerX, paddingXY 16 16 ]
            { url = "https://t.me/+8HDqLU4vhOQ3Yzg0"
            , label =
                image
                    [ height (px 200)
                    , centerX
                    , centerY
                    , Border.rounded 100
                    , clip
                    ]
                    { src = "/app/make-your-mental-health-priority.webp"
                    , description = ""
                    }
            }
        ]
    , column [ Font.size 14, spacing 16 ]
        [ paragraph [ Font.center ] [ text "ДОБРО ПОЖАЛОВАТЬ В НАШ TELEGRAM-КАНАЛ!" ]
        , paragraph [] [ text "Мы запустили наш официальный Telegram-канал. Теперь вы можете быть в курсе самых последних новостей в области когнитивной терапии в самом удобном и приватном мессенджере в мире. На данном канале мы будем проводить воркшопы и онлайн-семинара на самые интересные темы, информацию о акциях и др. Также вы сможете написать нам напрямую воспользовавшись чатом нашего канала." ]
        , newTabLink
            [ paddingXY 16 16 ]
            { url = "https://t.me/+8HDqLU4vhOQ3Yzg0"
            , label =
                column [ spacing 8 ]
                    [ text "✅ ССЫЛКА НА НАШ КАНАЛ В TELEGRAM:"
                    , text "https://t.me/+8HDqLU4vhOQ3Yzg0"
                    ]
            }
        , newTabLink
            [ paddingXY 16 16 ]
            { url = "https://t.me/mentalscores"
            , label =
                column [ spacing 8 ]
                    [ text "💬 ССЫЛКА НА НАШ ЧАТ В TELEGRAM:"
                    , text "https://t.me/mentalscores"
                    ]
            }
        ]
    ]


viewBodyNoSubscription : Ln -> Model -> List (Element Msg)
viewBodyNoSubscription ln model =
    [ Page.header (ln Ln.Header)

    -- , column [ Font.size 14, padding 16, spacing 8 ]
    --     [ paragraph []
    --         [ text (ln Ln.ReportDescription1) ]
    --     , paragraph [ Font.bold ]
    --         [ text (ln Ln.ReportDescription2) ]
    --     ]
    , column [ Font.size 14, padding 16, spacing 8 ]
        [ paragraph []
            [ text (ln Ln.ProgramDescription1) ]
        , paragraph [ Font.bold ]
            [ text (ln Ln.ProgramDescription2) ]
        ]
    , column [ width fill, alignBottom, spacing 16 ]
        [ --  Button.action (ln Ln.ReportLabel) diagnoses OrderedReport
          -- ,
          Button.action (ln Ln.ProgramLabel) idCardAlt OrderedProgram
        ]
    ]



-- EXPORT


toSession : Model -> Session
toSession model =
    model.session
