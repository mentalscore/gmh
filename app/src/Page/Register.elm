module Page.Register exposing (..)

import Browser.Navigation as Nav
import Element as El exposing (..)
import Element.Border as Border
import Element.Font as Font
import Element.Input as EInput
import Email exposing (EmailAddress)
import Env exposing (endpoint)
import FontAwesome.Icon as Icon exposing (Icon)
import FontAwesome.Solid exposing (arrowLeft)
import Http
import Json.Encode as JE
import Localizations.Languages as Languages exposing (Language)
import Localizations.Register as Ln
import Profile exposing (Profile)
import RemoteData exposing (RemoteData(..))
import Route
import Session exposing (Session)
import UI.Color as Color
import UI.Input as Input
import UI.Page as Page
import Viewer



-- MODEL


type alias Model =
    { email : String
    , password : String
    , navKey : Nav.Key
    , language : Language
    , user : RemoteData Error Profile
    }


type Error
    = HttpError Http.Error
    | GeneralError String


initModel : Nav.Key -> Language -> Model
initModel navKey language =
    { email = ""
    , password = ""
    , navKey = navKey
    , language = language
    , user = NotAsked
    }


init : Nav.Key -> Language -> ( Model, Cmd Msg )
init navKey language =
    ( initModel navKey language
    , Cmd.none
    )



-- UPDATE


type Msg
    = ChangedEmail String
    | ChangedPassword String
    | Register
    | GotAccount (Result Http.Error (Result String Profile))
    | ChangeToLogin


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        ChangedPassword string ->
            ( { model | password = string }, Cmd.none )

        ChangedEmail string ->
            ( { model | email = string }, Cmd.none )

        Register ->
            case Email.parse model.email of
                Ok email ->
                    case model.password of
                        "" ->
                            ( model, Cmd.none )

                        password ->
                            ( { model | user = Loading }
                            , register email password model.language
                            )

                Err _ ->
                    ( model, Cmd.none )

        GotAccount (Ok (Ok profile)) ->
            let
                modelNew =
                    { model | user = Success profile }
            in
            ( modelNew
            , Cmd.batch
                [ Route.replaceUrl model.navKey Route.Home
                , toSession modelNew
                    |> Maybe.map Viewer.store
                    |> Maybe.withDefault Cmd.none
                ]
            )

        GotAccount (Ok (Err message)) ->
            ( { model
                | user = Failure (GeneralError message)
              }
            , Cmd.none
            )

        GotAccount (Err data) ->
            ( { model | user = Failure (HttpError data) }
            , Cmd.none
            )

        ChangeToLogin ->
            ( model
            , Route.replaceUrl model.navKey Route.Login
            )


register : EmailAddress -> String -> Language -> Cmd Msg
register email password language =
    Http.post
        { url = endpoint ++ "accounts"
        , body =
            Http.jsonBody
                (JE.object
                    [ ( "email", JE.string (Email.toString email) )
                    , ( "password", JE.string password )
                    , ( "language", Languages.encode language )
                    ]
                )
        , expect = Http.expectJson GotAccount Profile.accountDecoder
        }



-- VIEW


view : Model -> { title : String, content : Element Msg }
view model =
    { title = Ln.translate model.language Ln.Title
    , content = content model
    }


content : Model -> Element Msg
content model =
    let
        ln =
            Ln.translate model.language
    in
    El.column
        [ El.width El.fill
        , El.height El.fill
        , Font.color Color.black
        , Font.size 14
        , inFront (viewNotification model)
        , padding 16
        ]
        [ Page.viewHeaderLayout
            (EInput.button
                [ width (px 16)
                , height (px 16)
                , Font.color Color.gray
                , alignLeft
                ]
                { onPress = Just ChangeToLogin
                , label =
                    arrowLeft
                        |> Icon.viewIcon
                        |> html
                }
            )
            (ln Ln.Header)
            none
        , El.column
            [ El.width El.fill
            , El.padding 16
            , spacing 16
            , centerY
            ]
            [ el [ Font.bold, centerX ]
                (text (ln Ln.Description))
            , El.column
                [ El.width El.fill
                , spacing 16
                ]
                [ Input.email (ln Ln.Email) model.email ChangedEmail
                , Input.currentPassword (ln Ln.Password) model.password ChangedPassword
                ]
            ]
        , El.row
            [ El.width El.fill
            , padding 16
            , alignBottom
            ]
            [ Input.buttonPrimary (ln Ln.Register) Nothing Register ]
        ]


viewNotification : Model -> Element Msg
viewNotification model =
    case model.user of
        Failure error ->
            case error of
                HttpError _ ->
                    Page.notification "Oops something went wrong"

                GeneralError message ->
                    Page.notification message

        Loading ->
            Page.loading

        NotAsked ->
            none

        Success _ ->
            none



-- EXPORT


toSession : Model -> Maybe Session
toSession model =
    model.user
        |> RemoteData.toMaybe
        |> Maybe.map (Session.login model.navKey)
