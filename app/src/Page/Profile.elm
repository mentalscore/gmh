module Page.Profile exposing (..)

import Browser.Navigation as Nav
import Data.Indication exposing (..)
import Element as El exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Events exposing (onClick)
import Element.Font as Font
import Element.Input as Input
import FontAwesome.Attributes as IconAttr
import FontAwesome.Icon as Icon exposing (Icon)
import FontAwesome.Solid
    exposing
        ( angry
        , calendarAlt
        , cog
        , comment
        , fileAlt
        , signOutAlt
        , signature
        , spinner
        , stickyNote
        , theaterMasks
        )
import List.Extra as List
import Localizations.Profile as Ln exposing (Ln)
import Route
import Session exposing (Session)
import UI.Color as Color
import UI.Input as Input
import UI.Page as Page
import Viewer



-- MODEL


type alias Model =
    { session : Session }


init : Session -> ( Model, Cmd msg )
init session =
    ( { session = session }
    , Cmd.none
    )



-- UPDATE


type Msg
    = Logout


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Logout ->
            ( model
            , Route.replaceUrl (.navKey model.session) Route.Logout
            )



-- VIEW


view : Model -> { title : String, content : Element Msg }
view model =
    { title = Ln.translate model.session.profile.language Ln.Title
    , content = content model
    }


content : Model -> Element Msg
content model =
    El.column
        [ El.width El.fill
        , El.height El.fill
        , Font.color Color.black
        , Font.size 14
        , paddingEach { bottom = 70, left = 16, right = 16, top = 16 }
        , inFront (Page.bottomBar Page.Profile)
        ]
        (viewBody model)


viewBody : Model -> List (Element Msg)
viewBody model =
    let
        ln =
            Ln.translate model.session.profile.language
    in
    [ Page.viewHeaderLayout none
        (ln Ln.Header)
        (Input.button
            [ Font.color Color.primary ]
            { onPress = Just Logout
            , label =
                signOutAlt
                    |> Icon.viewIcon
                    |> html
                    |> el
                        [ width (px 16)
                        , height (px 16)
                        , Font.color Color.gray
                        ]
            }
        )
    , row
        [ paddingEach { bottom = 16, left = 16, right = 16, top = 0 }
        , spacing 32
        ]
        [ image
            [ width (px 60)
            , height (px 60)
            , Border.rounded 30
            , clip
            ]
            { description = "Avatar", src = "https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d52?d=mp&f=y" }
        , column [ centerY, spacing 8 ]
            [ El.el [ Font.bold ]
                (model.session.profile.name
                    |> El.text
                )
            , link
                [ width (px 24)
                , height (px 24)
                ]
                { url = Route.toString Route.Settings
                , label =
                    row
                        [ Font.color Color.primary, spacing 8 ]
                        [ cog
                            |> Icon.viewIcon
                            |> html
                            |> el
                                [ width (px 14)
                                , height (px 14)
                                , centerX
                                ]
                        , El.el [] (El.text (ln Ln.Settings))
                        ]
                }
            ]
        ]

    -- , El.el [] (El.text (toId model))
    , viewPanels ln panels
    ]


panels =
    [ { name = Ln.Score
      , icon = spinner
      , color = rgb255 250 92 130
      , url = Route.toString (Route.Indication Score)
      }
    , { name = Ln.Depression
      , icon = theaterMasks
      , color = rgb255 101 132 244
      , url = Route.toString (Route.Indication Depression)
      }
    , { name = Ln.Anxiety
      , icon = signature
      , color = rgb255 255 135 44
      , url = Route.toString (Route.Indication Anxiety)
      }
    , { name = Ln.Psychosamatic
      , icon = angry
      , color = rgb255 98 102 176
      , url = Route.toString (Route.Indication Psychosomatic)
      }

    -- , { name = "Notes"
    --   , icon = stickyNote
    --   , color = rgb255 133 218 255
    --   , link = ""
    --   }
    -- , { name = "Diary"
    --   , icon = comment
    --   , color = rgb255 254 125 125
    --   , link = ""
    --   }
    -- , { name = "Calendar"
    --   , icon = calendarAlt
    --   , color = rgb255 101 132 244
    --   , link = ""
    --   }
    -- , { name = "Test Results"
    --   , icon = fileAlt
    --   , color = rgb255 250 92 130
    --   , link = ""
    --   }
    ]


type alias Panel =
    { name : Ln.Id
    , icon : Icon
    , color : Color
    , url : String
    }


viewPanels : Ln -> List Panel -> Element Msg
viewPanels ln ps =
    ps
        |> List.greedyGroupsOf 2
        |> List.map
            (\group ->
                row
                    [ width fill
                    , spacing 10
                    , height
                        (fill
                            |> maximum 115
                        )
                    ]
                    (List.map (viewPanel ln) group)
            )
        |> column
            [ paddingXY 0 16
            , spacing 16
            , width fill
            , height fill
            ]


viewPanel : Ln -> Panel -> Element Msg
viewPanel ln { name, icon, color, url } =
    column
        [ width fill
        , height fill
        , Border.rounded 10
        , padding 16
        , Background.color color
        , Font.color Color.white
        ]
        [ link
            [ width fill
            , height fill
            ]
            { url = url
            , label =
                column
                    [ width fill
                    , height fill
                    ]
                    [ el
                        [ Font.size 16
                        , Font.bold
                        ]
                        (text (ln name))
                    , icon
                        |> Icon.viewIcon
                        |> html
                        |> el
                            [ width (px 30)
                            , height (px 30)
                            , Font.color Color.white
                            , alignRight
                            , alignBottom
                            ]
                    ]
            }
        ]



-- EXPORT


toSession : Model -> Session
toSession model =
    model.session
