module Page.Tools exposing (..)

import Element as El exposing (..)
import Element.Font as Font
import Env exposing (endpoint)
import FontAwesome.Solid exposing (comment, diagnoses, idCardAlt)
import Http
import Json.Decode as JD
import Json.Encode as JE
import Localizations.Tools as Ln exposing (Ln)
import Screen.Activities as Activities
import Session exposing (Session)
import UI.Button as Button
import UI.Color as Color
import UI.Dialog as Dialog
import UI.Page as Page
import UI.Scale as Scale



-- MODEL


type alias Model =
    { session : Session
    , screen : Maybe Screen
    }


init : Session -> ( Model, Cmd msg )
init session =
    ( { session = session
      , screen = Nothing
      }
    , Cmd.none
    )


type Screen
    = ScreenActivities Activities.State



-- UPDATE


type Msg
    = Opened Screen
    | ActivitiesMsg Activities.Msg
    | Closed


getActivities : Screen -> Activities.State
getActivities screen =
    case screen of
        ScreenActivities state ->
            state


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    let
        uuid =
            model.session.profile.id
    in
    case msg of
        ActivitiesMsg activitiesMsg ->
            let
                mc : Maybe ( Activities.State, Cmd Activities.Msg )
                mc =
                    model.screen
                        |> Maybe.map getActivities
                        |> Maybe.map (Activities.update uuid activitiesMsg)
            in
            case mc of
                Just ( aState, cmds ) ->
                    ( { model
                        | screen = Just (ScreenActivities aState)
                      }
                    , Cmd.map ActivitiesMsg cmds
                    )

                Nothing ->
                    ( model, Cmd.none )

        Opened screen ->
            ( { model | screen = Just screen }
            , case screen of
                ScreenActivities _ ->
                    Activities.getActivities uuid
                        |> Cmd.map ActivitiesMsg
            )

        Closed ->
            ( { model | screen = Nothing }
            , Cmd.none
            )



-- VIEW


view : Model -> { title : String, content : Element Msg }
view model =
    { title = Ln.translate model.session.profile.language Ln.Title
    , content =
        model.screen
            |> Maybe.map (viewScreen model)
            |> Maybe.withDefault (content model)
    }


content : Model -> Element Msg
content model =
    let
        ln =
            Ln.translate model.session.profile.language
    in
    El.column
        [ El.width El.fill
        , El.height El.fill
        , paddingEach { bottom = 70, left = 16, right = 16, top = 16 }
        , inFront (Page.bottomBar Page.Tools)
        ]
        (viewBody ln model)


viewBody : Ln -> Model -> List (Element Msg)
viewBody ln model =
    [ Page.header (ln Ln.Header)
    , column
        [ width fill
        , height fill
        , alignBottom
        , spacing 16
        , paddingXY 0 96
        ]
        [ el [ width fill, alignBottom ]
            (Button.select (rgb255 255 135 44) (ln Ln.ActivitiesTracker) (Opened (ScreenActivities Activities.init)))
        ]
    ]


viewScreen : Model -> Screen -> Element Msg
viewScreen model screen =
    let
        ln =
            Ln.translate model.session.profile.language
    in
    case screen of
        ScreenActivities activities ->
            Activities.view
                { toMsg = ActivitiesMsg
                , closed = Closed
                , language = model.session.profile.language
                }
                activities



-- EXPORT


toSession : Model -> Session
toSession model =
    model.session
