module Page.Blank exposing (view)

import Element exposing (Element, el, text)



-- VIEW


view : { title : String, content : Element msg }
view =
    { title = ""
    , content = el [] (text "")
    }
