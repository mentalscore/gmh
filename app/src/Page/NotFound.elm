module Page.NotFound exposing (view)

import Element exposing (Element, alignRight, centerY, el, fill, padding, rgb255, row, spacing, text, width)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font



-- VIEW


view : { title : String, content : Element msg }
view =
    { title = "Page Not Found"
    , content =
        el
            [ Background.color (rgb255 240 0 245)
            , Font.color (rgb255 255 255 255)
            , Border.rounded 3
            , padding 30
            ]
            (text "Not found!")
    }
