module Page.Settings exposing (..)

import Browser.Dom as Dom
import Browser.Navigation as Nav
import Data.NotificationsStatus exposing (NotificationsStatus(..))
import Element as El exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Events exposing (onClick, onLoseFocus)
import Element.Font as Font
import Element.Input as Input
import Env exposing (endpoint)
import Html.Attributes as Attr
import Http
import Json.Decode as JD
import Json.Encode as JE
import Localizations.Languages as Languages exposing (Language(..))
import Localizations.Settings as Ln exposing (Ln)
import Ports
import Profile
import Route
import Session exposing (Session)
import Task
import UI.Attributes
import UI.Button as Button
import UI.Color as Color
import UI.Dialog.Input as DialogInput
import UI.Dialog.Radio as DialogRadio
import UI.Page as Page
import Viewer



-- MODEL


type alias Model =
    { session : Session
    , settingsId : Maybe Int
    , name : Maybe String
    , email : String
    , phone : String
    , notifications : NotificationsStatus
    , dialog : Maybe Dialog
    , dialogInput : DialogInput.State
    }


type Dialog
    = Name String
    | Phone String
    | Language Language


type alias Settings =
    { id : Int
    , name : String
    , phone : String
    }


init : Session -> ( Model, Cmd Msg )
init session =
    ( { session = session
      , settingsId = Nothing
      , name = Nothing
      , email = " "
      , phone = " "
      , notifications = Other ""
      , dialog = Nothing
      , dialogInput = DialogInput.init
      }
    , getSettings session.profile.id
    )



-- UPDATE


type Msg
    = GotSettings (Result Http.Error (List Settings))
    | ChangedName String
    | ChangedEmail String
    | ChangedPhone String
    | DialogOpened Dialog
    | SetNotificationsClicked NotificationsStatus
    | DialogClosed
    | SubmittedName String
    | SubmittedPhone String
    | SubmittedLanguage Language
    | GotDialogInputMsg DialogInput.Msg
    | SavedSettings (Result Http.Error Int)
    | UpdatedSettings (Result Http.Error Bool)
    | UpdatedLanguage Language (Result Http.Error Bool)
    | Closed
    | GotEventIn (Result JD.Error (Maybe Ports.EventIn))
    | NoOp


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GotSettings (Ok xs) ->
            List.head xs
                |> Maybe.map
                    (\settings ->
                        ( { model
                            | settingsId = Just settings.id
                            , name = Just settings.name
                            , phone = settings.phone
                          }
                        , Cmd.none
                        )
                    )
                |> Maybe.withDefault ( model, Cmd.none )

        GotSettings (Err err) ->
            ( model, Cmd.none )

        ChangedName str ->
            ( { model | name = Just str }, Cmd.none )

        ChangedEmail str ->
            ( model, Cmd.none )

        ChangedPhone str ->
            ( { model | phone = str }, Cmd.none )

        DialogOpened dialog ->
            ( { model
                | dialog = Just dialog
                , dialogInput = DialogInput.init
              }
            , focus DialogInput.attrId
            )

        DialogClosed ->
            ( { model
                | dialog = Nothing
              }
            , Cmd.none
            )

        SetNotificationsClicked bool ->
            ( model, Ports.toEventOut Ports.SetNotificationsClicked )

        SubmittedName name ->
            let
                model_ =
                    { model | dialog = Nothing, name = Just name }
            in
            ( model_, setSettings model_ )

        SubmittedPhone phone ->
            let
                model_ =
                    { model | dialog = Nothing, phone = phone }
            in
            ( model_, setSettings model_ )

        SubmittedLanguage lang ->
            ( { model | dialog = Nothing }
            , setLanguage model.session.profile.id lang
            )

        GotDialogInputMsg msg_ ->
            ( { model
                | dialogInput = DialogInput.update msg_ model.dialogInput
              }
            , Cmd.none
            )

        SavedSettings (Ok int) ->
            ( { model | settingsId = Just int }
            , Viewer.store (toSession model)
            )

        SavedSettings (Err err) ->
            ( model, Cmd.none )

        UpdatedSettings (Ok bool) ->
            ( model, Viewer.store (toSession model) )

        UpdatedSettings (Err err) ->
            ( model, Cmd.none )

        UpdatedLanguage lang _ ->
            -- lang (Ok bool) ->
            let
                model_ =
                    { model
                        | session =
                            model.session
                                |> Session.updateProfile (Profile.updateLanguage (\_ -> lang))
                    }
            in
            ( model_, Viewer.store (toSession model_) )

        -- UpdatedLanguage _ (Err _) ->
        --     ( model, Cmd.none )
        Closed ->
            ( model, Route.replaceUrl (.navKey model.session) Route.Profile )

        GotEventIn (Ok (Just result)) ->
            case result of
                Ports.NotificationsStatusReturned status ->
                    ( { model | notifications = status }, Cmd.none )

                Ports.ShowInstallPromotion ->
                    ( model, Cmd.none )

        GotEventIn (Ok Nothing) ->
            ( model, Cmd.none )

        GotEventIn (Err err) ->
            ( model, Cmd.none )

        NoOp ->
            ( model, Cmd.none )


getSettings : String -> Cmd Msg
getSettings uuid =
    Http.get
        { url = endpoint ++ "accounts/" ++ uuid ++ "/settings"
        , expect = Http.expectJson GotSettings <| JD.list decodeSettings
        }


setLanguage : String -> Language -> Cmd Msg
setLanguage uuid language =
    Http.request
        { method = "PUT"
        , headers = []
        , url = endpoint ++ "accounts" ++ "/" ++ uuid
        , body =
            [ ( "language", JE.string (Languages.toString language) )
            ]
                |> JE.object
                |> Http.jsonBody
        , expect =
            Http.expectJson
                (UpdatedLanguage language)
                (JD.field "saved" JD.bool)
        , timeout = Nothing
        , tracker = Nothing
        }


setSettings : Model -> Cmd Msg
setSettings { session, settingsId, name, phone } =
    let
        name_ =
            name
                |> Maybe.withDefault session.profile.name
    in
    case settingsId of
        Just id ->
            Http.request
                { method = "PUT"
                , headers = []
                , url = endpoint ++ "settings" ++ "/" ++ String.fromInt id
                , body =
                    Http.jsonBody
                        (encodeSettings session.profile.id name_ phone)
                , expect =
                    Http.expectJson
                        UpdatedSettings
                        (JD.field "saved" JD.bool)
                , timeout = Nothing
                , tracker = Nothing
                }

        Nothing ->
            Http.request
                { method = "POST"
                , headers = []
                , url = endpoint ++ "settings"
                , body =
                    Http.jsonBody
                        (encodeSettings session.profile.id name_ phone)
                , expect =
                    Http.expectJson
                        SavedSettings
                        (JD.field "id" JD.int)
                , timeout = Nothing
                , tracker = Nothing
                }


decodeSettings : JD.Decoder Settings
decodeSettings =
    JD.map3 Settings
        (JD.field "setId" JD.int)
        (JD.field "setName" JD.string)
        (JD.field "setPhone" JD.string)


encodeSettings : String -> String -> String -> JE.Value
encodeSettings uuid name phone =
    [ ( "uuid", JE.string uuid )
    , ( "name", JE.string name )
    , ( "phone", JE.string phone )
    ]
        |> JE.object



-- VIEW


view : Model -> { title : String, content : Element Msg }
view model =
    { title = Ln.translate model.session.profile.language Ln.Title
    , content = content model
    }


content : Model -> Element Msg
content model =
    let
        ln =
            Ln.translate model.session.profile.language
    in
    El.column
        [ El.width El.fill
        , El.height El.fill
        , Font.color Color.black
        , Background.color Color.primary
        , Font.size 14
        , paddingEach { bottom = 64, left = 2, right = 2, top = 16 }
        , inFront (Page.bottomEl (el [ Background.color Color.primary, width fill, paddingXY 2 8 ] (Button.contrast (ln Ln.Close) Closed)))
        , inFront
            (model.dialog
                |> Maybe.map (viewDialog ln model)
                |> Maybe.withDefault none
            )
        ]
        (viewBody model)


viewBody : Model -> List (Element Msg)
viewBody model =
    let
        ln =
            Ln.translate model.session.profile.language
    in
    [ El.column [ El.width El.fill, El.height El.fill ]
        [ el [ Font.color Color.white, centerX, centerY ]
            (Page.header (ln Ln.Header))
        ]
    , el
        [ width fill
        , height fill
        , paddingEach
            { bottom = 32
            , left = 2
            , right = 2
            , top = 0
            }
        ]
        (El.column [ El.width El.fill, Background.color Color.white, padding 16, Border.rounded 5 ]
            [ model.name
                |> Maybe.withDefault model.session.profile.name
                |> (\x -> viewElementDialog (ln Ln.Name) (Name x) x)
            , border
            , model.session.profile.email
                |> Maybe.withDefault (ln Ln.Guest)
                |> viewElement (ln Ln.Email) NoOp
            , border
            , model.phone
                |> (\x -> viewElementDialog (ln Ln.PhoneLabel) (Phone x) x)
            , border
            , (\x ->
                viewElementDialog
                    (ln Ln.Language)
                    (Language x)
                    (Languages.toLocalizeString x)
              )
                model.session.profile.language
            , border
            , (\x ->
                viewElementNotifications ln model.notifications
              )
                model.session.profile.language
            ]
        )
    ]


viewElementDialog : String -> Dialog -> String -> Element Msg
viewElementDialog label dialog name =
    row [ width fill, Background.color Color.white, padding 16, spacing 16, onClick (DialogOpened dialog) ]
        [ column [ spacing 8, height fill ]
            [ el [ Font.size 18, Font.bold, centerY ] (text label)
            , el [ centerY ] (text name)
            ]
        ]


viewElement label msg name =
    row [ width fill, Background.color Color.white, padding 16, spacing 16, onClick msg ]
        [ column [ spacing 8, height fill ]
            [ el [ Font.size 18, Font.bold, centerY ] (text label)
            , el [ centerY ] (text name)
            ]
        ]



-- viewElementNotifications : NotificationsStatus -> Element Msg


viewElementNotifications ln status =
    row
        [ width fill
        , Background.color Color.white
        , padding 16
        , spacing 16
        , onClick (SetNotificationsClicked status)
        ]
        [ column [ spacing 8, height fill ]
            [ el [ Font.size 18, Font.bold, centerY ] (text (ln Ln.NotificationsTitle))
            , el [ centerY ] (text (ln Ln.NotificationsDescription))
            ]
        , el [ Font.bold, alignRight ] (text (ln (Ln.NotificationsStatus status)))
        ]


border =
    el
        [ width fill
        , Border.color Color.background
        , Border.widthEach
            { bottom = 1
            , left = 0
            , right = 0
            , top = 0
            }
        , Border.solid
        ]
        none


viewDialog : (Ln.Id -> String) -> Model -> Dialog -> Element Msg
viewDialog ln model dialog =
    case dialog of
        Name value ->
            DialogInput.view
                { toMsg = GotDialogInputMsg
                , cancelled = DialogClosed
                , submitted = SubmittedName
                , cancelLabel = ln Ln.Cancel
                , okLabel = ln Ln.OK
                }
                { title = ln Ln.NameQuestion, value = value }
                model.dialogInput

        Phone value ->
            DialogInput.view
                { toMsg = GotDialogInputMsg
                , cancelled = DialogClosed
                , submitted = SubmittedPhone
                , cancelLabel = ln Ln.Cancel
                , okLabel = ln Ln.OK
                }
                { title = ln Ln.PhoneQuestion, value = value }
                model.dialogInput

        Language lang ->
            DialogRadio.view
                { cancelled = DialogClosed
                , submitted = SubmittedLanguage
                , active = lang
                , title = ln Ln.LanguageQuestion
                , toString = Languages.toLocalizeString
                , cancelLabel = ln Ln.Cancel
                }
                Languages.all


focus : String -> Cmd Msg
focus id =
    Task.attempt (\_ -> NoOp) (Dom.focus id)



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch [ Ports.sub GotEventIn ]



-- EXPORT


toSession : Model -> Session
toSession model =
    let
        name _ =
            model.name
                |> Maybe.withDefault model.session.profile.name
    in
    model.session
        |> Session.updateProfile (Profile.updateName name)



-- , column [ width fill, height fill, padding 16, spacing 16 ]
--     [ Input.text
--         [ onLoseFocus SetSettings
--         , UI.Attributes.onEnter SetSettings
--         , htmlAttribute <| Attr.id nameId
--         ]
--         { onChange = ChangedName
--         , text =
--             model.name
--                 |> Maybe.withDefault model.session.profile.name
--         , placeholder = Nothing
--         , label = Input.labelAbove [] (el [] (text (ln Ln.Name)))
--         }
--     , Input.email
--         [ Background.color Color.secondary ]
--         { onChange = ChangedEmail
--         , text =
--             model.session.profile.email
--                 |> Maybe.withDefault (ln Ln.Guest)
--         , placeholder = Nothing
--         , label = Input.labelAbove [] (el [] (text (ln Ln.Email)))
--         }
--     , Input.text
--         [ onLoseFocus SetSettings
--         , UI.Attributes.onEnter SetSettings
--         , htmlAttribute <| Attr.id phoneId
--         ]
--         { onChange = ChangedPhone
--         , text = model.phone
--         , placeholder = Just (Input.placeholder [] (el [] (text (ln Ln.PhonePlaceholder))))
--         , label = Input.labelAbove [] (el [] (text (ln Ln.PhoneLabel)))
--         }
--     ]
