module Profile exposing (..)

import Element as El exposing (Element)
import Element.Background as Background
import Element.Border as Border
import Json.Decode as Decode exposing (Decoder)
import Localizations.Languages as Languages exposing (Language(..))
import Time exposing (Posix)


type alias Profile =
    { id : String
    , language : Language
    , email : Maybe String
    , disclaimer : Bool
    , name : String
    , avatar : String
    , subscriptionEnd : Maybe Posix
    }


guest : String -> Language -> Profile
guest uuid lang =
    { id = uuid
    , language = lang
    , email = Nothing
    , disclaimer = False
    , name = ""
    , avatar = ""
    , subscriptionEnd = Nothing
    }


account : String -> Language -> String -> Bool -> Profile
account uuid lang email bool =
    { id = uuid
    , language = lang
    , email = Just email
    , disclaimer = bool
    , name = ""
    , avatar = ""
    , subscriptionEnd = Nothing
    }


updateName : (String -> String) -> Profile -> Profile
updateName setName profile =
    { profile | name = setName profile.name }


updateLanguage : (Language -> Language) -> Profile -> Profile
updateLanguage setLanguage profile =
    { profile | language = setLanguage profile.language }


setSubscriptionEnd : Posix -> Profile -> Profile
setSubscriptionEnd subscriptionEnd profile =
    { profile | subscriptionEnd = Just subscriptionEnd }



-- SERIALIZATION


guestDecoder : Decoder Profile
guestDecoder =
    Decode.map2 guest
        (Decode.field "id" Decode.string)
        (Decode.field "language" Languages.decoder)


accountDecoder : Decoder (Result String Profile)
accountDecoder =
    Decode.oneOf
        [ Decode.map4 account
            (Decode.field "id" Decode.string)
            (Decode.field "language" Languages.decoder)
            (Decode.field "email" Decode.string)
            (Decode.field "disclaimer" Decode.bool)
            -- (Decode.field "name" Decode.string)
            |> Decode.map Ok
        , Decode.field "error" Decode.string
            |> Decode.map Err
        ]


decoder : Decoder Profile
decoder =
    Decode.map7 Profile
        (Decode.field "id" Decode.string)
        (Decode.field "language" Languages.decoder)
        (Decode.field "email" (Decode.nullable Decode.string))
        (Decode.field "disclaimer" Decode.bool)
        (Decode.field "name" Decode.string)
        (Decode.field "picture" Decode.string)
        (Decode.field "subscriptionEnd" (Decode.nullable decodePosix))


decodePosix : Decoder Posix
decodePosix =
    Decode.int
        |> Decode.map Time.millisToPosix


view : Profile -> Element msg
view profile =
    El.el
        [ El.width (El.px 40)
        , El.height (El.px 40)
        , Border.rounded 40
        , Background.image profile.avatar
        ]
        El.none
