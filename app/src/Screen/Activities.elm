module Screen.Activities exposing (..)

import Data.Activities as Activities exposing (..)
import Data.Notifications.Activities as NotificationsActivities
import Element as El exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Events exposing (onClick)
import Element.Font as Font
import Element.Input as Input
import Env exposing (endpoint)
import FontAwesome.Icon as Icon exposing (Icon)
import Http
import Json.Decode as JD
import List.Extra as List
import Localizations.Activities as Ln
import Localizations.Languages exposing (..)
import Ports
import UI.Button as Button
import UI.Color as Color
import UI.Page as Page



-- STATE


type alias State =
    { activities : List UserActivity }


init : State
init =
    { activities =
        Activities.activities
            |> List.map initUserActivity
    }


type alias UserActivity =
    { id : Maybe Int
    , activity : Activity
    , isSelected : Bool
    }


initUserActivity : Activity -> UserActivity
initUserActivity activity =
    { id = Nothing
    , activity = activity
    , isSelected = False
    }


updateActivities : (List UserActivity -> List UserActivity) -> State -> State
updateActivities f state =
    { state | activities = f state.activities }



-- UPDATE


type Msg
    = ActivitySelected UserActivity
    | GotActivityResponse Activity (Result Http.Error Int)
    | ActivityUnselected UserActivity
    | GotActivitiesResponse (Result Http.Error (List UserActivity))
    | GotActivityDelete (Result Http.Error ())


update : String -> Msg -> State -> ( State, Cmd Msg )
update uuid msg state =
    case msg of
        ActivitySelected activity ->
            let
                add =
                    List.updateIf ((==) activity) (\a -> { a | isSelected = True })
            in
            ( updateActivities add state
            , addActivity activity.activity uuid
            )

        ActivityUnselected activity ->
            let
                remove =
                    List.updateIf ((==) activity) (\a -> { a | isSelected = False })
            in
            ( updateActivities remove state
            , activity.id
                |> Maybe.map (deleteActivity uuid)
                |> Maybe.withDefault Cmd.none
            )

        GotActivityResponse a (Ok id) ->
            ( updateActivities (addIndex a id) state
              -- TODO:
              -- , Ports.toEventOut
              --     (Ports.SetActivityNotifications NotificationsActivities.default)
            , Cmd.none
            )

        GotActivityResponse activity (Err _) ->
            ( state, Cmd.none )

        GotActivitiesResponse (Ok activities) ->
            ( updateActivities (combine activities) state
            , Cmd.none
            )

        GotActivitiesResponse (Err _) ->
            -- TODO: Think about an error
            ( state, Cmd.none )

        GotActivityDelete _ ->
            -- TODO: Think about an error
            ( state, Cmd.none )


getActivities : String -> Cmd Msg
getActivities uuid =
    Http.get
        { url = endpoint ++ "accounts/" ++ uuid ++ "/activities"
        , expect = Http.expectJson GotActivitiesResponse (JD.list decodeUserActivities)
        }


decodeUserActivities : JD.Decoder UserActivity
decodeUserActivities =
    JD.map3 UserActivity
        (JD.field "activityId" (JD.maybe JD.int))
        (JD.field "activityType" Activities.decode)
        (JD.succeed True)


addActivity : Activities.Activity -> String -> Cmd Msg
addActivity activity uuid =
    Http.post
        { url = endpoint ++ "activities"
        , body =
            Http.jsonBody (Activities.encode uuid activity)
        , expect =
            Http.expectJson (GotActivityResponse activity)
                (JD.field "id" JD.int)
        }


deleteActivity : String -> Int -> Cmd Msg
deleteActivity uuid id =
    Http.request
        { method = "DELETE"
        , headers = []
        , url = endpoint ++ "activities/" ++ String.fromInt id
        , body = Http.emptyBody
        , expect = Http.expectWhatever GotActivityDelete
        , timeout = Nothing
        , tracker = Nothing
        }


combine : List UserActivity -> (List UserActivity -> List UserActivity)
combine new current =
    List.uniqueBy (\a -> Activities.toString a.activity)
        (new ++ current)


addIndex : Activity -> Int -> (List UserActivity -> List UserActivity)
addIndex activity id =
    List.updateIf (\a -> a.activity == activity)
        (\a ->
            { a
                | id = Just id
                , isSelected = True
            }
        )



-- VIEW


type alias Config msg =
    { toMsg : Msg -> msg
    , closed : msg
    , language : Language
    }


view : Config msg -> State -> Element msg
view config state =
    let
        translate =
            Ln.translate config.language
    in
    column
        [ width fill
        , height fill
        , padding 16
        , Background.color Color.white
        ]
        [ Page.header (translate Ln.Header)
        , viewActivities config state.activities
        , el [ width fill, alignBottom ]
            (Button.secondary (translate Ln.OK) config.closed)
        ]


viewActivities : Config msg -> List UserActivity -> Element msg
viewActivities config activities =
    let
        translate =
            Ln.translateActivity config.language
    in
    activities
        |> List.uniqueBy (Activities.toString << .activity)
        |> List.sortBy (Activities.toString << .activity)
        |> List.greedyGroupsOf 4
        |> List.map
            (\group ->
                row [ width fill, spacing 10, height fill ]
                    (List.map (viewActivity config.toMsg translate) group)
            )
        |> column
            [ paddingXY 0 16
            , spacing 16
            , width fill
            ]


viewActivity : (Msg -> msg) -> (Activity -> String) -> UserActivity -> Element msg
viewActivity toMsg translate activity =
    let
        color =
            if activity.isSelected then
                Color.primary

            else
                Color.gray

        msg =
            if activity.isSelected then
                toMsg (ActivityUnselected activity)

            else
                toMsg (ActivitySelected activity)
    in
    column
        [ width fill
        , height fill
        , onClick msg
        , Font.color color
        ]
        [ column
            [ padding 16, width fill ]
            [ toIcon activity.activity
                |> Icon.viewIcon
                |> html
                |> el
                    [ width (px 32)
                    , height (px 32)
                    , centerX
                    ]
            ]
        , el [ padding 4, width fill, centerX, centerY ]
            (paragraph
                [ Font.size 12
                , width fill
                , Font.center
                , Font.color Color.black
                ]
                [ text (translate activity.activity) ]
            )
        ]
