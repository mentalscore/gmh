module Localizations.Languages exposing (..)

import Json.Decode as JD exposing (Decoder)
import Json.Encode as JE exposing (Value)


type alias Translation =
    { english : String
    , russian : String

    -- , dutch : String
    }


all : List Language
all =
    [ English, Russian ]


type Language
    = English
    | Russian



-- | Dutch


decoderFromFlag : Decoder Language
decoderFromFlag =
    JD.map fromFlagString JD.string


fromFlag : Value -> Language
fromFlag value =
    value
        |> JD.decodeValue (JD.field "language" decoderFromFlag)
        |> Result.withDefault English


decoder : Decoder Language
decoder =
    JD.map fromString JD.string


encode : Language -> Value
encode =
    toString >> JE.string


fromFlagString : String -> Language
fromFlagString str =
    case String.left 2 str of
        "en" ->
            English

        "ru" ->
            Russian

        -- "nl" ->
        --     Dutch
        _ ->
            English


fromString : String -> Language
fromString str =
    case str of
        "english" ->
            English

        "russian" ->
            Russian

        -- "nl" ->
        --     Dutch
        _ ->
            English


toString : Language -> String
toString lang =
    case lang of
        English ->
            "english"

        Russian ->
            "russian"


toLocalizeString : Language -> String
toLocalizeString lang =
    case lang of
        English ->
            "English"

        Russian ->
            "Русский"



-- Dutch ->
--     "Dutch"
