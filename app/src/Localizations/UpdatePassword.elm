module Localizations.UpdatePassword exposing (..)

import Localizations.Languages exposing (..)


type alias Ln =
    Id -> String


translate : Language -> Id -> String
translate lang =
    case lang of
        English ->
            translationEn

        Russian ->
            translationRu


type Id
    = Title
    | Label
    | Submit
    | PasswordHasBeenChanged
    | PasswordHasNotUpdated
    | GoBackToTheLoginPage
    | GeneralError


translationEn : Id -> String
translationEn id =
    case id of
        Title ->
            "MentalScore"

        Label ->
            "Please enter your new password"

        Submit ->
            "Update"

        PasswordHasBeenChanged ->
            "Success! Your Password has been changed!"

        PasswordHasNotUpdated ->
            "Password has not updated please try to reset password again"

        GoBackToTheLoginPage ->
            "Go back to the login page"

        GeneralError ->
            "Oops something went wrong"


translationRu : Id -> String
translationRu id =
    case id of
        Title ->
            "MentalScore"

        Label ->
            "Пожалуйста, введите ваш новый пароль"

        Submit ->
            "Обновить"

        PasswordHasBeenChanged ->
            "Ваш пароль был изменен!"

        PasswordHasNotUpdated ->
            "Ваш пароль не обновлен, попробуйте сбросить пароль еще раз"

        GoBackToTheLoginPage ->
            "Вернуться на страницу входа"

        GeneralError ->
            "Упс! Что-то пошло не так"
