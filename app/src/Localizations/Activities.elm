module Localizations.Activities exposing (..)

import Data.Activities exposing (..)
import Localizations.Languages exposing (..)


translate : Language -> Id -> String
translate lang =
    case lang of
        English ->
            translationEn

        Russian ->
            translationRu


type Id
    = Header
    | OK


translationEn : Id -> String
translationEn id =
    case id of
        Header ->
            "Activities"

        OK ->
            "OK"


translationRu : Id -> String
translationRu id =
    case id of
        Header ->
            "Активность"

        OK ->
            "OK"


translateActivity : Language -> Activity -> String
translateActivity lang =
    case lang of
        English ->
            translationActivityEn

        Russian ->
            translationActivityRu


translationActivityEn : Activity -> String
translationActivityEn activity =
    case activity of
        Alcohol ->
            "Alcohol"

        Beer ->
            "Beer"

        ChattingFriends ->
            "Chatting Friends"

        Gym ->
            "Gym"

        Joint ->
            "Joint"

        JunkFood ->
            "Junk Food"

        MeetFriends ->
            "Meet Friends"

        Reading ->
            "Reading"

        Running ->
            "Running"

        Shopping ->
            "Shopping"

        Smoking ->
            "Smoking"

        StayingHome ->
            "Staying Home"

        Vacation ->
            "Vacation"

        Walking ->
            "Walking"

        WatchTV ->
            "WatchTV"

        PlayVideoGame ->
            "Play Video Game"


translationActivityRu : Activity -> String
translationActivityRu activity =
    case activity of
        Alcohol ->
            "Алкоголь"

        Beer ->
            "Пиво"

        ChattingFriends ->
            "Общение с друзьями"

        Gym ->
            "Гимнастический зал"

        Joint ->
            "Трава"

        JunkFood ->
            "Фастфуд"

        MeetFriends ->
            "Встреча с друзьями"

        Reading ->
            "Чтение"

        Running ->
            "Бег"

        Shopping ->
            "Шоппинг"

        Smoking ->
            "Сигареты"

        StayingHome ->
            "Дом"

        Vacation ->
            "Отпуск"

        Walking ->
            "Прогулка"

        WatchTV ->
            "Телевизор"

        PlayVideoGame ->
            "Компьютерные игры"
