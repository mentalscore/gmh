module Localizations.Screeners exposing (..)

import Data.Level exposing (..)
import Localizations.Languages exposing (..)


type alias Ln =
    Id -> String


translate : Language -> Id -> String
translate lang =
    case lang of
        English ->
            translationEn

        Russian ->
            translationRu


type Id
    = Title
    | Header
    | PHQTitle
    | Depression
    | Anxiety
    | Subjective
    | Psychosomatic
    | YourResult
    | OutOf
    | Next
    | QuestionnaireName String
    | QuestionnaireExplanation String
    | QuestionTitle String
    | QuestionResponse String
    | SubjectiveResultTitle
    | SubjectiveResultContent
    | MeasureHeader String
    | MeasureLevel String
    | MeasureDescription Level String


translationEn : Id -> String
translationEn id =
    case id of
        Title ->
            "Screening"

        Header ->
            "Screening"

        PHQTitle ->
            "PATIENT HEALTH QUESTIONNAIRE (PHQ)"

        Depression ->
            "Depression"

        Anxiety ->
            "Anxiety"

        Subjective ->
            "Subjective"

        Psychosomatic ->
            "Psychosomatic"

        QuestionnaireName "PHQ-9" ->
            "Patient Health Questionnaire-9"

        QuestionnaireName "GAD-7" ->
            "Generalized Anxiety Disorder subscale"

        QuestionnaireName "PHQ-15" ->
            "PHYSICAL SYMPTOMS"

        QuestionnaireName "SUBJECTIVE-5" ->
            "SUBJECTIVE"

        YourResult ->
            "Your Result"

        OutOf ->
            "Out of "

        Next ->
            "Next"

        QuestionnaireName str ->
            str

        QuestionnaireExplanation str ->
            questionnaireExplanationEn str

        QuestionTitle str ->
            questionTitleEn str

        QuestionResponse str ->
            questionResponseEn str

        SubjectiveResultTitle ->
            "Great job!"

        SubjectiveResultContent ->
            "You successfully complete the first step of diagnosis. Let's see your primary result and some recommendations."

        MeasureHeader string ->
            string

        MeasureLevel string ->
            string

        MeasureDescription level str ->
            measureDescriptionEn level str


measureDescriptionEn : Level -> String -> String
measureDescriptionEn level s =
    let
        str =
            questionnaireToTypeEn s
    in
    case level of
        Minimal ->
            "Your results indicate that you may not be experiencing symptoms of " ++ str ++ "."

        Mild ->
            "Your results indicate that you may be experiencing symptoms of mild " ++ str ++ "."

        Moderate ->
            "Your results indicate that you may be experiencing symptoms of moderate " ++ str ++ "."

        ModeratelySevere ->
            "Your results indicate that you may be experiencing symptoms of moderate " ++ str ++ "."

        Severe ->
            "Your results indicate that you may be experiencing symptoms of severe " ++ str ++ "."


questionnaireToTypeEn : String -> String
questionnaireToTypeEn questionnaire =
    case questionnaire of
        "PHQ-9" ->
            "depression"

        "GAD-7" ->
            "anxiety"

        "PHQ-15" ->
            "psychosomatic disorder"

        _ ->
            ""


questionResponseEn : String -> String
questionResponseEn str =
    str


questionnaireExplanationEn : String -> String
questionnaireExplanationEn str =
    case str of
        "PHQ-9" ->
            "Over the last 2 weeks, how often have you been bothered by any of the following problems?"

        _ ->
            str


questionTitleEn : String -> String
questionTitleEn str =
    case str of
        _ ->
            str


translationRu : Id -> String
translationRu id =
    case id of
        Title ->
            "Диагностика"

        Header ->
            "Диагностика"

        PHQTitle ->
            "ОПРОСНИК ПО СОСТОЯНИЮ ЗДОРОВЬЯ (PHQ)"

        Depression ->
            "Депрессия"

        Anxiety ->
            "Тревожность"

        Subjective ->
            "Субъектвиная"

        Psychosomatic ->
            "Психосоматика"

        QuestionnaireName "PHQ-9" ->
            "Опросник по состоянию здоровья"

        QuestionnaireName "GAD-7" ->
            "Шкала тревожного расстройства"

        QuestionnaireName "PHQ-15" ->
            "Физические симптомы"

        QuestionnaireName "SUBJECTIVE-5" ->
            "Субъектвиная"

        YourResult ->
            "Ваш Результат"

        OutOf ->
            "из "

        Next ->
            "Далее"

        QuestionnaireName str ->
            str

        QuestionnaireExplanation str ->
            questionnaireExplanationRu str

        QuestionTitle str ->
            questionTitleRu str

        QuestionResponse str ->
            questionResponseRu str

        SubjectiveResultTitle ->
            "Отличная работа!"

        SubjectiveResultContent ->
            "Вы успешно прошли первый этап диагностики. Посмотрим на ваш основной результат и несколько рекомендаций."

        MeasureHeader string ->
            measureHeaderRu string

        MeasureLevel string ->
            measureLevelRu string

        MeasureDescription level str ->
            measureDescriptionRu level str


measureDescriptionRu : Level -> String -> String
measureDescriptionRu level type_ =
    case type_ of
        "PHQ-9" ->
            measureDescriptionDepression level

        "GAD-7" ->
            measureDescriptionAnxiety level

        "PHQ-15" ->
            measureDescriptionPsychosomatic level

        _ ->
            ""


measureDescriptionDepression : Level -> String
measureDescriptionDepression level =
    case level of
        Minimal ->
            "Поздравляю! Желаю и дальше чувствовать себя также хорошо."

        Mild ->
            "Обратите внимание на своё настроение, питание и сон , наблюдайте за ними с помощью приложения. Позаботьтесь о себе. Найдите свои проблемные зоны и исправьте их."

        Moderate ->
            "Будьте внимательны, ваше состояние требует особенного наблюдения. Не закрывайте глаза на проблемы. Наблюдайте за своим настроением, питанием и сном в течение некоторого времени с помощью приложения. Рекомендуем обращение к специалисту."

        ModeratelySevere ->
            "Если вы честно ответили на вопросы, следует активно заняться своим состоянием. Помимо наблюдения за сном, аппетитом и настроением с помощью приложения, рекомендуем обратиться за специальной помощью."

        Severe ->
            "Обязательно обращение за помощью специалиста. Берегите себя и необходимо принимать активные меры по улучшению самочувствия."


measureDescriptionAnxiety : Level -> String
measureDescriptionAnxiety level =
    case level of
        Minimal ->
            "Поздравляем вас, тревога побуждает к активным действиям и развитию. Будьте в форм е."

        Mild ->
            "Следует наблюдать за своим состоянием в течение некоторого времени с помощью приложения. Постарайтесь понять в чем причина тревоги и решить проблему, если это невозможно, то рекомендуем отложить решение проблемы и заняться собой."

        Moderate ->
            "Будьте внимательны к себе, успокойтесь, попробуйте решать проблемы постепенно. Ситуация требует постоянного наблюдения с помощью приложения. Позаботьтесь о себе. Обратитесь к специалисту."

        ModeratelySevere ->
            "Обязательно обращение к специалисту. Позвольте себе получить помощь и поддержку. Необходимо активно заняться своим эмоциональным состоянием."

        Severe ->
            "Обязательно обращение к специалисту. Позвольте себе получить помощь и поддержку. Необходимо активно заняться своим эмоциональным состоянием."


measureDescriptionPsychosomatic : Level -> String
measureDescriptionPsychosomatic level =
    case level of
        Minimal ->
            "Всё идет хорошо. Позвольте себе и дальше получать удовольствие от жизни."

        Mild ->
            "Обратите внимание на своё самочувствие, займитесь своим телом, оно требует внимания.\nНе закрывайте глаза на проблему. Понаблюдайте за собой в течение некоторого времени\nс помощью приложения."

        Moderate ->
            "Ваше состояние требует постоянного наблюдения с помощью приложения, рекомендуем\nобратиться к специалисту."

        ModeratelySevere ->
            "Вам следует активно заняться своим здоровьем, ваше состояние не должно остаться без внимания. В ближайшее время рекомендуем обратиться к специалисту."

        Severe ->
            "Вам следует активно заняться своим здоровьем, ваше состояние не должно остаться без внимания. В ближайшее время рекомендуем обратиться к специалисту."


measureLevelRu : String -> String
measureLevelRu str =
    case str of
        "Minimal" ->
            "Минимальный"

        "Mild" ->
            "Начальный"

        "Moderate" ->
            "Умеренный"

        "Moderately Severe" ->
            "Умеренно тяжёый"

        "Severe" ->
            "Тяжёлый"

        _ ->
            str


measureHeaderRu : String -> String
measureHeaderRu str =
    case str of
        "Your Score" ->
            "Ваши баллы"

        "Your Depression Measure" ->
            "Показатель депрессии"

        "Your Anxiety Measure" ->
            "Показатель тревожности"

        "Your Somatic Symptom Severity" ->
            "Психосоматика"

        _ ->
            str


questionResponseRu : String -> String
questionResponseRu str =
    case str of
        "Not at all" ->
            "Ни разу"

        "Several days" ->
            "Несколько дней"

        "More than half the days" ->
            "Более недели"

        "Nearly every day " ->
            "Почти каждый день "

        "Not bothered at all" ->
            "Вообще не беспокоили"

        "Bothered a little" ->
            "Немного беспокоили"

        "Bothered" ->
            "Сильно беспокоили"

        _ ->
            str


questionnaireExplanationRu : String -> String
questionnaireExplanationRu str =
    case str of
        "PHQ-9" ->
            "Как часто за последние 2 недели Вас беспокоили следующие проблемы?"

        "GAD-7" ->
            "Как часто Вас за последние 14 дней беспокоили следующие проблемы?"

        "PHQ-15" ->
            "Насколько сильно вас беспокоили какие-либо из перечисленных ниже проблем на протяжении последних 4 недель? "

        _ ->
            str


questionTitleRu : String -> String
questionTitleRu str =
    case str of
        "Little interest or pleasure in doing things" ->
            "Вам не хотелось ничего делать"

        "Feeling down, depressed, or hopeless" ->
            "У Вас было плохое настроение, Вы были подавлены\nили испытывали чувство безысходности"

        "Trouble falling or staying asleep, or sleeping too much" ->
            "Вам было трудно заснуть, у Вас был прерывистый сон,\nили Вы слишком много спали"

        "Feeling tired or having little energy" ->
            "Вы были утомлены, или у Вас было мало сил "

        "Poor appetite or overeating" ->
            "У Вас был плохой аппетит, или Вы переедали"

        "Feeling bad about yourself — or that you are a failure or have let yourself or your family down" ->
            "Вы плохо о себе думали: считали себя неудачником (неудачницей), или были в себе разочарованы, или считали, что подвели свою семью"

        "Trouble concentrating on things, such as reading the newspaper or watching television" ->
            "Вам было трудно сосредоточиться (например, на чтении газеты или на просмотре телепередач"

        "Moving or speaking so slowly that other people could have noticed? Or the opposite — being so fidgety or restless that you have been moving around a lot more than usual" ->
            "Вы двигались или говорили настолько медленно, что окружающие это замечали? Или, наоборот, Вы были настолько суетливы или взбудоражены, что двигались  гораздо больше обычного"

        "Thoughts that you would be better off dead or of hurting yourself in some way" ->
            "Вас посещали мысли о том, что Вам лучше было бы умереть, или о том, чтобы причинить себе какой-нибудь вред"

        "Feeling nervous, anxious or on edge" ->
            "Вы нервничали, тревожились или испытывали сильный стресс"

        "Not being able to stop or control worrying" ->
            "Вы были неспособны успокоиться или контролировать свое волнение"

        "Worrying too much about different things" ->
            "Вы слишком сильно волновались по различным поводам"

        "Trouble relaxing" ->
            "Вам было трудно расслабиться "

        "Being so restless that it is hard to sit still" ->
            "Вы были настолько суетливы, что Вам было тяжело усидеть на месте"

        "Becoming easily annoyed or irritable" ->
            "Вы легко злились или раздражались"

        "Feeling afraid as if something awful might happen" ->
            "Вы испытывали страх, словно должно произойти нечто ужасное"

        "Stomach pain" ->
            "Боль в желудке"

        "Back pain" ->
            "Боль в спине"

        "Pain in your arms, legs, or joints (knees, hips, etc.)" ->
            "Боль в руках, ногах или суставах (в коленях, бедрах и т.д.)"

        "Menstrual cramps or other problems with your periods" ->
            "Менструальная боль или другие проблемы с Вашим менструальным циклом"

        "Headaches" ->
            "Головные боли"

        "Chest pain" ->
            "Боль в груди"

        "Dizziness" ->
            "Головокружение"

        "Fainting spells" ->
            "Потеря сознания"

        "Feeling your heart pound or race" ->
            "Усиленное или учащенное сердцебиение"

        "Shortness of breath" ->
            "Одышка"

        "Pain or problems during sexual intercourse" ->
            "Боль или проблемы во время полового акта"

        "Constipation, loose bowels, or diarrhea" ->
            "Запор, склонность к поносу или диарея"

        "Nausea, gas, or indigestion" ->
            "Тошнота, газы или расстройство желудка"

        "Feeling tired or having low energy" ->
            "Ощущение усталости или слабости"

        "Trouble sleeping" ->
            "Нарушения сна"

        "How do you feel about your home environment?" ->
            "Как вы оцениваете свою домашнюю обстановке?"

        "How do you feel about your social life?" ->
            "Как вы оцениваете свою общественную жизни?"

        "How do you feel about your work environment?" ->
            "Как вы оцениваете свою рабочую среду?"

        "How do you feel about your eating behaviour?" ->
            "Как вы оцениваете свою диету?"

        "How physically active are you?" ->
            "Насколько вы физически активны?"

        _ ->
            str
