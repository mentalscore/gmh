module Localizations.Indication exposing (..)

import Localizations.Languages exposing (..)


type alias Ln =
    Id -> String


translate : Language -> Id -> String
translate lang =
    case lang of
        English ->
            translationEn

        Russian ->
            translationRu


type Id
    = Title
    | IndicationGeneral Int


translationEn : Id -> String
translationEn id =
    case id of
        Title ->
            "Settings"

        IndicationGeneral _ ->
            "We are proud of you, great job! It looks like you are doing very good, keep on working on your negative thoughts and try to be mindful of your diet."


translationRu : Id -> String
translationRu id =
    case id of
        Title ->
            "Настройки"

        IndicationGeneral int ->
            indicationGeneralRu int


indicationGeneralRu : Int -> String
indicationGeneralRu p =
    if p >= 0 && p < 25 then
        "Поздравляем вас, так держать. Поддерживайте себя в этом состоянии при помощи приложения."

    else if p >= 25 && p < 50 then
        "Будьте внимательны к себе, успокойтесь, попробуйте решать проблемы постепенно. Ситуация требует постоянного наблюдения с помощью приложения. Позаботьтес ь о себе. Обратитесь к специалисту. Не закрывайте глаза на проблему"

    else if p >= 50 && p < 75 then
        "Следует наблюдать за своим состоянием в течение некоторого времени с помощью приложения. Постарайтесь понять в чем причина вашего состояния и решить проблему. Возможно потребуется помощь специалиста. Обратите внимание на своё самочувствие, позаботьтесь о себе."

    else
        "Поздравляем вас, так держать. Поддерживайте себя в этом состоянии при помощи приложения"
