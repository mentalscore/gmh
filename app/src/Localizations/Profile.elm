module Localizations.Profile exposing (..)

import Localizations.Languages exposing (..)


type alias Ln =
    Id -> String


translate : Language -> Id -> String
translate lang =
    case lang of
        English ->
            translationEn

        Russian ->
            translationRu


type Id
    = Title
    | Header
    | Settings
    | Score
    | Depression
    | Anxiety
    | Psychosamatic


translationEn : Id -> String
translationEn id =
    case id of
        Title ->
            "Profile"

        Header ->
            "Profile"

        Settings ->
            "Settings"

        Score ->
            "Score"

        Depression ->
            "Depression"

        Anxiety ->
            "Anxiety"

        Psychosamatic ->
            "Psychosamatic"


translationRu : Id -> String
translationRu id =
    case id of
        Title ->
            "Профайл"

        Header ->
            "Профайл"

        Settings ->
            "Настройки"

        Score ->
            "Mental Score"

        Depression ->
            "Депрессия"

        Anxiety ->
            "Тревожность"

        Psychosamatic ->
            "Психосоматика"
