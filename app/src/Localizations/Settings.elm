module Localizations.Settings exposing (..)

import Data.NotificationsStatus exposing (NotificationsStatus(..))
import Localizations.Languages exposing (..)


type alias Ln =
    Id -> String


translate : Language -> Id -> String
translate lang =
    case lang of
        English ->
            translationEn

        Russian ->
            translationRu


type Id
    = Title
    | Header
    | Name
    | NameQuestion
    | Guest
    | Email
    | PhonePlaceholder
    | PhoneQuestion
    | PhoneLabel
    | Close
    | Language
    | LanguageQuestion
    | NotificationsTitle
    | NotificationsDescription
    | NotificationsStatus NotificationsStatus
    | Cancel
    | OK


translationEn : Id -> String
translationEn id =
    case id of
        Title ->
            "Settings"

        Header ->
            "Settings"

        Name ->
            "Name"

        NameQuestion ->
            "What should the Psychotherapist call you?"

        Guest ->
            "not specified"

        Email ->
            "Email"

        PhonePlaceholder ->
            "+1 (650) 251-53-21"

        PhoneQuestion ->
            "Add phone number"

        PhoneLabel ->
            "Phone Number"

        Close ->
            "Close"

        Language ->
            "Language"

        LanguageQuestion ->
            "Choose your preferred language"

        Cancel ->
            "Cancel"

        OK ->
            "OK"

        NotificationsTitle ->
            "Notifications"

        NotificationsDescription ->
            "Turn notifications on or off"

        NotificationsStatus status ->
            case status of
                Granted ->
                    "Granted"

                Denied ->
                    "Denied"

                Other str ->
                    str


translationRu : Id -> String
translationRu id =
    case id of
        Title ->
            "Настройки"

        Header ->
            "Настройки"

        Name ->
            "Ваше Имя"

        NameQuestion ->
            "Как к вам должен обращаться психотерапевт?"

        Guest ->
            "не указан"

        Email ->
            "Почтовый ящик"

        PhonePlaceholder ->
            "8 999 123-45-67"

        PhoneQuestion ->
            "Добавьте ваш номер телефона"

        PhoneLabel ->
            "Номер телефон"

        Close ->
            "Закрыть"

        Language ->
            "Language"

        LanguageQuestion ->
            "Выберите предпочтительный для вас язык"

        Cancel ->
            "Отмена"

        OK ->
            "Сохранить"

        NotificationsTitle ->
            "Push уведомления"

        NotificationsDescription ->
            "Включить или отключить оповещения"

        NotificationsStatus status ->
            case status of
                Granted ->
                    "Granted"

                Denied ->
                    "Denied"

                Other str ->
                    str
