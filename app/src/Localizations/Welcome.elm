module Localizations.Welcome exposing (..)

import Localizations.Languages exposing (..)


type alias Ln =
    Id -> String


translate : Language -> Id -> String
translate lang =
    case lang of
        English ->
            translationEn

        Russian ->
            translationRu


type Id
    = Title
    | Header
    | ColorTest
    | LusherDescription
    | SurveyHeader
    | Question
    | Submit
    | Name
    | Age
    | Gender
    | Anxiety
    | Treatment
    | NeedHelp
    | PossibleAnswer String
    | Placeholder String


translationEn : Id -> String
translationEn id =
    case id of
        Title ->
            "Welcome"

        Header ->
            "Lüscher"

        ColorTest ->
            "Color Test: "

        LusherDescription ->
            "Choose the color you like the most at the moment. Do not associate it with objects, your surrounding or your favorite color – just the color you like the most."

        SurveyHeader ->
            "Survey"

        Question ->
            "Question: "

        Submit ->
            "Submit"

        Name ->
            "What's your name?"

        Age ->
            "What's your age?"

        Gender ->
            "To which gender identity do you most identify?"

        Anxiety ->
            "What's worrying you?"

        Treatment ->
            "What treatment did you have already?"

        NeedHelp ->
            "Do you feel lack of support?"

        PossibleAnswer str ->
            str

        Placeholder str ->
            str


translationRu : Id -> String
translationRu id =
    case id of
        Title ->
            "Опрос"

        Header ->
            "Люшера"

        ColorTest ->
            "Цветовой тест: "

        LusherDescription ->
            "Выберите цвет, который вам нравится больше всего в данный момент. Не связывайте его с предметами, вашим окружением или вашим любимым цветом - просто с цветом, который вам нравится больше всего."

        SurveyHeader ->
            "Опросник"

        Question ->
            "Вопрос: "

        Submit ->
            "Отправить"

        Name ->
            "Ваше имя"

        Age ->
            "Ваш возраст"

        Gender ->
            "Какой гендерной идентичности вы больше всего придерживаетесь?"

        Anxiety ->
            "Что вас беспокоит?"

        Treatment ->
            "Обращались ли вы за помощью?"

        NeedHelp ->
            "Есть ли потребность консультации специалиста и контроля за решением?"

        PossibleAnswer str ->
            toPossibleAnswerRu str

        Placeholder str ->
            toPossiblePlaceholderRu str


toPossibleAnswerRu : String -> String
toPossibleAnswerRu str =
    case str of
        "Male" ->
            "Мужской"

        "Female" ->
            "Женский"

        "Prefer Not to Answer" ->
            "Предпочитаю не отвечать"

        "Personal life" ->
            "Личные проблемы"

        "Psychosomatics" ->
            "Психосоматика"

        "Family" ->
            "Конфликты в семье"

        "Mood" ->
            "Плохое настроение"

        "Friends" ->
            "Проблемы с друзьями"

        "Work" ->
            "Проблемы на работе"

        "Communication" ->
            "Проблемы с общением"

        "Other Anxiety" ->
            "Другое"

        "Self Diagnosis" ->
            "Самодиагностика"

        "Selft Tests" ->
            "Тесты"

        "Mobile App" ->
            "Мобильное приложение"

        "Psychotherapist" ->
            "Психотерапевт"

        "Yes" ->
            "Да"

        "No" ->
            "Нет"

        "Not Sure" ->
            "Не уверен"

        _ ->
            str


toPossiblePlaceholderRu : String -> String
toPossiblePlaceholderRu str =
    case str of
        "Will Barrow" ->
            ""

        _ ->
            str
