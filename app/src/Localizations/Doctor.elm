module Localizations.Doctor exposing (..)

import Localizations.Languages exposing (..)


type alias Ln =
    Id -> String


translate : Language -> Id -> String
translate lang =
    case lang of
        English ->
            translationEn

        Russian ->
            translationRu


type Id
    = Title
    | Header
    | ReportDescription1
    | ReportDescription2
    | ReportLabel
    | ProgramDescription1
    | ProgramDescription2
    | ProgramLabel
    | DialogTitle
    | DialogContent
    | DialogLabel


translationEn : Id -> String
translationEn id =
    case id of
        Title ->
            "Consultant"

        Header ->
            "Consultant"

        ReportDescription1 ->
            "Please order a full mental health report to determine whether you are experiencing symptoms of a mental health condition."

        ReportDescription2 ->
            "Mental health conditions, such as depression or anxiety, are real, common and treatable. And recovery is possible."

        ProgramDescription1 ->
            "Behavioral health treatments are ways of helping people with mental illnesses or substance use disorders. For many people, the most effective behavioral health approach involves a combination of counseling and medication. Early treatment is best."

        ProgramDescription2 ->
            "No single treatment works best. Treatments must address each person’s needs and symptoms."

        ReportLabel ->
            "Full Mental Health Report"

        ProgramLabel ->
            "Behavioral Health Program"

        DialogTitle ->
            "Region not supported"

        DialogContent ->
            "This service isn't available in your location. We will keep you informed on further update."

        DialogLabel ->
            "OK"


translationRu : Id -> String
translationRu id =
    case id of
        Title ->
            "Консультант"

        Header ->
            "Консультант"

        ReportDescription1 ->
            "Закажите полный отчет о психическом здоровье, чтобы определить, испытываете ли вы симптомы психического состояния."

        ReportDescription2 ->
            "Нарушения психического здоровья, такие как депрессия или тревога, реальны, распространены и поддаются лечению. И выздоровление возможно."

        ProgramDescription1 ->
            "Лечение поведенческого здоровья - это способ помочь людям с психическими заболеваниями или расстройствами, связанными с употреблением психоактивных веществ. Для многих наиболее эффективный подход к поведенческому здоровью предполагает сочетание консультирования и приема лекарств. Лучше всего начать лечение на раннем этапе."

        ProgramDescription2 ->
            "Ни одно лечение не работает лучше всего. Лечение должно учитывать потребности и симптомы каждого человека."

        ReportLabel ->
            "Полный отчет"

        ProgramLabel ->
            "Программа"

        DialogTitle ->
            "Регион не поддерживается"

        DialogContent ->
            "Эта услуга недоступна в вашем регионе. Мы будем держать вас в курсе дальнейших обновлений."

        DialogLabel ->
            "OK"
