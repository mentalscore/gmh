module Localizations.Subscription exposing (..)

import Data.Plan as Plan
import Localizations.Languages exposing (..)


type alias Ln =
    Id -> String


translate : Language -> Id -> String
translate lang =
    case lang of
        English ->
            translationEn

        Russian ->
            translationRu


type Id
    = Title
    | Header
    | Step
    | Of
    | ChoosePlanTitle
    | ChoosePlanDescription
    | MonthlyPrice
    | IndividualSelfHelpProgram
    | FullMentalHealthReport
    | VideoConsultation
    | PlanName Plan.Type
    | PlanPrice Plan.Type
    | PlanVideCons Plan.Type
    | Continue
    | SetUpYourPayment
    | PaymentDescription


translationEn : Id -> String
translationEn id =
    case id of
        Title ->
            "Subscription"

        Header ->
            "Subscription"

        PlanName Plan.MonthlyRecurring ->
            "MonthlyRecurring"

        PlanName Plan.Annual ->
            "Annual"

        PlanPrice Plan.MonthlyRecurring ->
            "$3.5"

        PlanPrice Plan.Annual ->
            "$250"

        PlanVideCons Plan.MonthlyRecurring ->
            ""

        PlanVideCons Plan.Annual ->
            "1 hour"

        Step ->
            "STEP"

        Of ->
            "OF"

        ChoosePlanTitle ->
            "Choose the plan that's right for you"

        ChoosePlanDescription ->
            "Change or cancel your plan anytime."

        MonthlyPrice ->
            "Monthly price"

        IndividualSelfHelpProgram ->
            "Individual Self-Help Program"

        FullMentalHealthReport ->
            "Full Mental Health Report"

        VideoConsultation ->
            "Video Consultation"

        Continue ->
            "Continue"

        SetUpYourPayment ->
            "Set up your payment."

        PaymentDescription ->
            "Your membership starts as soon as you set up payment."


translationRu : Id -> String
translationRu id =
    case id of
        Title ->
            "Подписка"

        Header ->
            "Подписка"

        Step ->
            "ШАГ"

        Of ->
            "ИЗ"

        ChoosePlanTitle ->
            "Воспользуйтесь расширенными возможностями"

        ChoosePlanDescription ->
            "Сделайте душевное благополучие неотъемлемой частью своей жизни."

        MonthlyPrice ->
            "Ежемесячная плата"

        IndividualSelfHelpProgram ->
            "Гибкие программы психологического оздоровления, созданные экспертами MentalScore"

        FullMentalHealthReport ->
            "Неограниченный доступ к мероприятиям, которые проводят наши специалисты"

        VideoConsultation ->
            "Постоянный трекинг психического здоровья"

        PlanName Plan.MonthlyRecurring ->
            "Ежемесячная"

        PlanName Plan.Annual ->
            "Годовая"

        PlanPrice Plan.MonthlyRecurring ->
            "500"

        PlanPrice Plan.Annual ->
            "3000"

        PlanVideCons Plan.MonthlyRecurring ->
            ""

        PlanVideCons Plan.Annual ->
            "1 час"

        Continue ->
            "Продолжить"

        SetUpYourPayment ->
            "Оплата подписки"

        PaymentDescription ->
            ""
