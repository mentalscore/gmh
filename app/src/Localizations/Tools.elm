module Localizations.Tools exposing (..)

import Localizations.Languages exposing (..)


type alias Ln =
    Id -> String


translate : Language -> Id -> String
translate lang =
    case lang of
        English ->
            translationEn

        Russian ->
            translationRu


type Id
    = Title
    | Header
    | ActivitiesTracker


translationEn : Id -> String
translationEn id =
    case id of
        Title ->
            "Self-Help Therapy"

        Header ->
            "Self-Help Therapy"

        ActivitiesTracker ->
            "Activities Tracker"


translationRu : Id -> String
translationRu id =
    case id of
        Title ->
            "Селф-Терапия"

        Header ->
            "Селф-Терапия"

        ActivitiesTracker ->
            "Трекер активности"
