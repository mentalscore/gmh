module Localizations.Home exposing (..)

import Localizations.Languages exposing (..)


type alias Ln =
    Id -> String


translate : Language -> Id -> String
translate lang =
    case lang of
        English ->
            translationEn

        Russian ->
            translationRu


type Id
    = HomePageTitle
    | Disclaimer
    | DisclaimerParagraph1
    | DisclaimerParagraph2
    | DisclaimerAccept
    | GeneralError
    | Points
    | Depression
    | Anxiety
    | Psychosomatic
    | Subjective
    | DefaultIndication
    | HowWasYourDay
    | Activities
    | ActivitiesOK
    | InstallTitle
    | InstallDescription
    | InstallDismiss
    | InstallOK


translationEn : Id -> String
translationEn id =
    case id of
        HomePageTitle ->
            "Home"

        Disclaimer ->
            "Disclaimer"

        DisclaimerParagraph1 ->
            "This mental health application is not intended to be a replacement for treatment nor any sort of medical intervention. Treatment is, by far, the best way to overcome clinical depression or other mental illnesses. Therapy and antidepressants have been shown to effectively treat clinical depression and anxiety. This application will only aid you on your path to recovery from depression or anxiety. In addition, if you are not in treatment, talk to your doctor or therapist or use this app to find resources that can connect you to treatment."

        DisclaimerParagraph2 ->
            "While App is designed as a self-help mental health application to help people suffering from clinical depression, it can also assist people without mental illness or people suffering from other mental disorders / mental illnesses such as anxiety, post-traumatic stress disorder (PTSD), bipolar disorder, seasonal affective disorder, dysthymia, obsessive-compulsive disorder (OCD), panic disorder, generalized anxiety disorder, or schizophrenia"

        DisclaimerAccept ->
            "ACCEPT"

        GeneralError ->
            "oops something went wrong"

        Points ->
            "points"

        Depression ->
            "Depression"

        Anxiety ->
            "Anxiety"

        Psychosomatic ->
            "Psychosomatic"

        Subjective ->
            "Subjective"

        DefaultIndication ->
            "We are proud of you, great job! It looks like you are doing very good, keep on working on your negative thoughts and try to be mindful of your diet."

        HowWasYourDay ->
            "How was your day?"

        Activities ->
            "Activities"

        ActivitiesOK ->
            "OK"

        InstallTitle ->
            "Get out free app"

        InstallDescription ->
            "Installing uses almost no storage and provides a quick way to return to this app."

        InstallDismiss ->
            "Not now"

        InstallOK ->
            "Install"


translationRu : Id -> String
translationRu id =
    case id of
        HomePageTitle ->
            "MentalScore"

        Disclaimer ->
            "Дисклеймер"

        DisclaimerParagraph1 ->
            "Это приложение для охраны психического здоровья не предназначено для замены лечения или какого-либо медицинского вмешательства. Безусловно, лечение - лучший способ преодолеть клиническую депрессию или другие психические заболевания. Доказано, что терапия и антидепрессанты эффективно лечат клиническую депрессию и тревогу. Это приложение только поможет вам на пути к выздоровлению от депрессии или тревоги. Кроме того, если вы не проходите лечение, поговорите со своим врачом или терапевтом или воспользуйтесь этим приложением, чтобы найти ресурсы, которые помогут вам пройти лечение."

        DisclaimerParagraph2 ->
            "Хотя приложение разработано как приложение для самопомощи психического здоровья, чтобы помочь людям, страдающим от клинической депрессии, оно также может помочь людям без психических заболеваний или людям, страдающим другими психическими расстройствами / психическими заболеваниями, такими как тревога, посттравматическое стрессовое расстройство (ПТСР). , биполярное расстройство, сезонное аффективное расстройство, дистимия, обсессивно-компульсивное расстройство (ОКР), паническое расстройство, генерализованное тревожное расстройство или шизофрения"

        DisclaimerAccept ->
            "СОГЛАСЕН"

        GeneralError ->
            "Упс что-то пошло не так"

        Points ->
            "баллов"

        Depression ->
            "Депрессия"

        Anxiety ->
            "Тревожность"

        Psychosomatic ->
            "Психосоматика"

        Subjective ->
            "Субъективное"

        DefaultIndication ->
            "Мы тобой гордимся, отличная работа! Похоже, у вас все хорошо, продолжайте работать над своими негативными мыслями и старайтесь соблюдать диету."

        HowWasYourDay ->
            "Как прошёл день?"

        Activities ->
            "Активность"

        ActivitiesOK ->
            "Сохранить"

        InstallTitle ->
            "Установите бесплатное приложение"

        InstallDescription ->
            "Оно не займет места на вашем телефоне."

        InstallDismiss ->
            "Позже"

        InstallOK ->
            "Усновить"
