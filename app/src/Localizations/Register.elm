module Localizations.Register exposing (..)

import Localizations.Languages exposing (..)


translate : Language -> Id -> String
translate lang =
    case lang of
        English ->
            .english << translation

        Russian ->
            .russian << translation



-- Dutch ->
--     .dutch << translation


type Id
    = Title
    | Header
    | Description
    | Register
    | Email
    | Password


translation : Id -> Translation
translation id =
    case id of
        Title ->
            Translation "Register" "Регистрация"

        Header ->
            Translation "MentalScore" "MentalScore"

        Description ->
            Translation
                "Create your MentalScore account."
                "Создать учетную запись"

        Register ->
            Translation "Register" "Зарегестрироваться"

        Email ->
            Translation "Email address" "Почтовый ящик (Email)"

        Password ->
            Translation "Password" "Пароль"
