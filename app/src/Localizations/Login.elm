module Localizations.Login exposing (..)

import Localizations.Languages exposing (..)


translate : Language -> Id -> String
translate lang =
    case lang of
        English ->
            .english << translation

        Russian ->
            .russian << translation



-- Dutch ->
--     .dutch << translation


type Id
    = Title
    | Header
    | Description
    | QuickDiagnosis
    | OR
    | Register
    | Email
    | Password
    | Login
    | ForgotPassword
    | DontHave
    | Join
    | ResetPasswordLabel
    | ResetPasswordButton
    | HttpError
    | EmailIsNotValid
    | CheckYourEmail
    | GoBackToTheLoginPage


translation : Id -> Translation
translation id =
    case id of
        Title ->
            Translation "Sign in" "Вход"

        Header ->
            Translation "MentalScore" "MentalScore"

        Description ->
            Translation
                "To continue, log in to MentalScore."
                "Для продолжения, введите свою учетную запись"

        QuickDiagnosis ->
            Translation "QUICK DIAGNOSIS" "БЫСТРАЯ ДИАГНОСТИКА"

        Register ->
            Translation "Register" "Зарегестрироваться"

        OR ->
            Translation "OR" "ИЛИ"

        Email ->
            Translation "Email address" "Почтовый ящик (Email)"

        Password ->
            Translation "Password" "Пароль"

        Login ->
            Translation "Login" "Вход"

        ForgotPassword ->
            Translation "Forgot your password?" "Забыли пароль?"

        DontHave ->
            Translation "Don't have an account?" "Ещё нет аккаунта?"

        Join ->
            Translation "JOIN MENTAL SCORE" "ПРИСОЕДИНИТЬСЯ К СООБЩЕСТВУ"

        ResetPasswordLabel ->
            Translation "Please enter your email address to reset your password." "Пожалуйста, введите свой адрес электронной почты, чтобы сбросить пароль."

        ResetPasswordButton ->
            Translation "Reset Password" "Сбросить пароль"

        HttpError ->
            Translation "The application has encountered an http error." "Возникла ошибка http."

        EmailIsNotValid ->
            Translation "Email is not valid" "Значение «E-mail» не верно"

        CheckYourEmail ->
            Translation "Please check your email to reset your password" "Пожалуйста, проверьте свою электронную почту, чтобы сбросить пароль"

        GoBackToTheLoginPage ->
            Translation "Go back to the login page" "Вернуться на страницу входа"
