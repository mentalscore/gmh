port module Ports exposing (..)

{-|

@docs EventIn, eventIn
@docs EventOut, eventOut

-}

import Data.Notifications.Activities as Activities
import Data.NotificationsStatus exposing (NotificationsStatus(..))
import Json.Decode as JD
import Json.Encode as JE



-- EVENT IN --


type EventIn
    = ShowInstallPromotion
    | NotificationsStatusReturned NotificationsStatus


toEventIn : String -> Maybe JD.Value -> Maybe EventIn
toEventIn str mValue =
    case str of
        "ShowInstallPromotion" ->
            Just ShowInstallPromotion

        "NotificationsStatusReturned" ->
            mValue
                |> Maybe.andThen
                    (\value ->
                        value
                            |> JD.decodeValue Data.NotificationsStatus.decoder
                            |> Result.toMaybe
                            |> Maybe.map NotificationsStatusReturned
                    )

        _ ->
            Nothing


eventInDecoder : JD.Decoder (Maybe EventIn)
eventInDecoder =
    JD.map2 toEventIn
        (JD.field "type" JD.string)
        (JD.field "data" (JD.maybe JD.value))


sub : (Result JD.Error (Maybe EventIn) -> msg) -> Sub msg
sub toMsg =
    eventIn (JD.decodeValue eventInDecoder >> toMsg)


port eventIn : (JD.Value -> msg) -> Sub msg


port testEventIn : (JD.Value -> msg) -> Sub msg



-- EVENT OUT --


type EventOut
    = AppInstallClicked
    | SetNotificationsClicked
    | SetActivityNotifications Activities.Data


encodeEventOut : EventOut -> JE.Value
encodeEventOut event =
    case event of
        AppInstallClicked ->
            JE.object
                [ ( "type", JE.string "AppInstallClicked" ) ]

        SetNotificationsClicked ->
            JE.object
                [ ( "type", JE.string "SetNotificationsClicked" ) ]

        SetActivityNotifications data ->
            JE.object
                [ ( "type", JE.string "SetActivityNotifications" )
                , ( "data", Activities.encode data )
                ]


toEventOut : EventOut -> Cmd msg
toEventOut event =
    eventOut (encodeEventOut event)


port eventOut : JE.Value -> Cmd msg
