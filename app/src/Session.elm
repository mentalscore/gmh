module Session exposing (..)

import Browser.Navigation as Nav
import Data.Screeners exposing (..)
import Dict exposing (Dict)
import Json.Decode as Decode
import Localizations.Languages as Languages exposing (Language)
import Profile exposing (Profile)
import Viewer exposing (Viewer)



-- TYPES


type alias Session =
    { navKey : Nav.Key
    , profile : Profile
    , screener : Maybe Screener
    }



--


login : Nav.Key -> Profile -> Session
login navKey p =
    { navKey = navKey
    , profile = p
    , screener = Nothing
    }



-- signOut : Session -> Session
-- signOut session =
--     { session
--         | disclaimer = False
--         , profile = Nothing
--         , screener = Nothing
--     }


setScreener : Screener -> Session -> Session
setScreener screener session =
    { session | screener = Just screener }


updateProfile : (Profile -> Profile) -> Session -> Session
updateProfile f session =
    { session | profile = f session.profile }



-- INFO


getNavKey : Session -> Nav.Key
getNavKey { navKey } =
    navKey


uuid : Session -> String
uuid { profile } =
    profile.id



-- CHANGES
-- changes : (Session -> msg) -> Nav.Key -> Sub msg
-- changes toMsg key =
--     Api.viewerChanges (\maybeViewer -> toMsg (fromViewer key maybeViewer)) Viewer.decoder


fromViewer : Nav.Key -> Result Language Viewer -> Maybe Session
fromViewer key viewer =
    -- It's stored in localStorage as a JSON String;
    -- first decode the Value as a String, then
    -- decode that String as JSON.
    case viewer of
        Ok v ->
            { navKey = key
            , profile = v.profile
            , screener = Nothing
            }
                |> Just

        Err language ->
            Nothing



-- empty key language


acceptedDisclaimer : Session -> Session
acceptedDisclaimer s =
    let
        f p =
            { p | disclaimer = True }
    in
    { s | profile = f s.profile }
