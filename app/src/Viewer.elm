port module Viewer exposing (..)

import Dict exposing (Dict)
import Json.Decode as Decode exposing (Decoder, Value, at, bool, nullable)
import Json.Decode.Pipeline exposing (custom, optional, required)
import Json.Encode as Encode
import Localizations.Languages as Languages
import Profile exposing (Profile)
import Time exposing (Posix)



-- TYPES


type alias Viewer =
    { disclaimer : Bool
    , profile : Profile
    }



-- INFO
-- cred : Viewer -> Cred
-- cred (Viewer _ val) =
--     val
-- username : Viewer -> Username
-- username (Viewer _ val) =
--     Api.username val
-- avatar : Viewer -> Avatar
-- avatar (Viewer val _) =
--     val
-- {-| Passwords must be at least this many characters long!
-- -}
-- minPasswordChars : Int
-- minPasswordChars =
--     6
-- SERIALIZATION


decoder : Decoder Viewer
decoder =
    Decode.succeed Viewer
        |> optional "disclaimer" bool False
        |> required "profile" Profile.decoder


decode : Value -> Result Decode.Error Viewer
decode =
    Decode.decodeValue decoder



-- PORTS


port storeCache : Maybe Value -> Cmd msg


type alias Cache r =
    { r | profile : Profile }


store : Cache r -> Cmd msg
store { profile } =
    -- let
    --     json =
    --         case profile of
    --             Just p ->
    --                 encodeProfile p disclaimer
    --                     |> Just
    --             Nothing ->
    --                 Nothing
    -- in
    storeCache (Just <| encodeProfile profile)



-- login : Cache r -> Cmd msg
-- login { profile, disclaimer } =
-- let
--     json =
--         case profile of
--             Just p ->
--                 encodeProfile p disclaimer
--                     |> Just
--             Nothing ->
--                 Nothing
-- in
-- storeCache (encodeProfile profile disclaimer)


encodeProfile : Profile -> Value
encodeProfile p =
    Encode.object
        [ ( "profile"
          , Encode.object
                [ ( "id", Encode.string p.id )
                , ( "language", Languages.encode p.language )
                , ( "email", encodeEmail p.email )
                , ( "name", Encode.string p.name )
                , ( "disclaimer", Encode.bool p.disclaimer )
                , ( "picture", Encode.string p.avatar )
                , ( "subscriptionEnd"
                  , p.subscriptionEnd
                        |> Maybe.map (Encode.int << Time.posixToMillis)
                        |> Maybe.withDefault Encode.null
                  )
                ]
          )
        ]


encodeEmail : Maybe String -> Encode.Value
encodeEmail email =
    case email of
        Just e ->
            Encode.string e

        Nothing ->
            Encode.null


logout : Cmd msg
logout =
    storeCache Nothing
