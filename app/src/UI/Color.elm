module UI.Color exposing (..)

import Element exposing (..)


primary =
    -- #5db075
    rgb255 93 176 117


primaryBright =
    rgba255 93 176 117 0.57


secondary =
    rgb255 240 240 240


background =
    rgb255 44 56 64


success =
    rgb255 58 219 118


warning =
    rgb255 255 174 0


alert =
    rgb255 204 75 55


white =
    rgb255 254 254 254


black =
    rgb255 30 30 30


lightGray =
    rgb255 222 222 222


gray =
    rgb255 202 202 202


primaryGray =
    rgb255 196 196 196


darkGray =
    rgb255 124 124 124
