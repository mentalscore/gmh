module UI.Button exposing (..)

import Element as El exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Events exposing (onClick)
import Element.Font as Font
import Element.Input as Input
import Element.Region as Region
import FontAwesome.Attributes as IconAttr
import FontAwesome.Icon as Icon exposing (Icon)
import UI.Color as Color



-- EXPORT


primary : String -> Icon -> msg -> Element msg
primary label icon onPress =
    view
        { label = label
        , icon = Just icon
        , onPress = onPress
        , style = Primary
        }


secondary : String -> msg -> Element msg
secondary label onPress =
    view
        { label = label
        , icon = Nothing
        , onPress = onPress
        , style = Secondary
        }


contrast : String -> msg -> Element msg
contrast label onPress =
    view
        { label = label
        , icon = Nothing
        , onPress = onPress
        , style = Contrast
        }


select : Color -> String -> msg -> Element msg
select color label onPress =
    viewSelect
        { label = label
        , icon = Nothing
        , onPress = onPress
        , style = Secondary
        }
        color


simple : String -> msg -> Element msg
simple label onPress =
    Input.button
        [ Font.color Color.primary
        , Font.bold
        ]
        { onPress = Just onPress
        , label =
            row
                [ centerX
                , centerY
                , spacing 10
                ]
                [ text label ]
        }



-- DATA


type alias Button msg =
    { label : String
    , icon : Maybe Icon
    , onPress : msg
    , style : Style
    }


type Style
    = Primary
    | Secondary
    | Contrast



-- VIEW


view : Button msg -> Element msg
view { label, icon, onPress, style } =
    let
        colors =
            case style of
                Primary ->
                    [ Background.color Color.primary
                    , Font.color Color.white
                    , Border.rounded 5
                    ]

                Secondary ->
                    [ Background.color Color.secondary
                    , Font.color Color.black
                    , Border.color Color.gray
                    , Border.solid
                    , Border.rounded 5
                    , Border.width 1
                    ]

                Contrast ->
                    [ Background.color Color.white
                    , Font.color Color.black
                    , Border.color Color.gray
                    , Border.solid
                    , Border.rounded 5
                    , Border.width 1
                    ]
    in
    Input.button
        ([ height (px 51)
         , width fill
         ]
            ++ colors
        )
        { onPress = Just onPress
        , label =
            row
                [ centerX
                , centerY
                , spacing 10
                ]
                [ case icon of
                    Just i ->
                        Icon.viewIcon i
                            |> html
                            |> el
                                [ width (px 20)
                                , height (px 20)
                                ]

                    Nothing ->
                        none
                , text label
                ]
        }


viewSelect : Button msg -> Color -> Element msg
viewSelect { label, icon, onPress, style } color =
    let
        colors =
            case style of
                Primary ->
                    [ Background.color Color.primary
                    , Font.color Color.white
                    , Border.rounded 5
                    ]

                Secondary ->
                    [ Background.color color
                    , Font.color Color.white
                    , Border.solid
                    , Border.rounded 5
                    , Border.width 1
                    ]

                Contrast ->
                    []
    in
    Input.button
        ([ width fill ]
            ++ colors
        )
        { onPress = Just onPress
        , label =
            row
                [ centerY
                , paddingXY 42 20
                , spacing 10
                ]
                [ case icon of
                    Just i ->
                        Icon.viewIcon i
                            |> html
                            |> el
                                [ width (px 20)
                                , height (px 20)
                                ]

                    Nothing ->
                        none
                , el [ Font.color Color.white, Font.size 20 ] (text label)
                ]
        }


viewStar : Icon.Icon -> String -> Element msg
viewStar icon response =
    icon
        |> Icon.present
        |> Icon.view
        |> html


link : String -> String -> Element msg
link url label =
    El.link
        [ height (px 51)
        , width fill
        , Background.color Color.secondary
        , Font.color Color.black
        , Border.color Color.gray
        , Border.solid
        , Border.rounded 5
        , Border.width 1
        ]
        { url = url
        , label =
            row
                [ centerX
                , centerY
                , spacing 10
                ]
                [ text label ]
        }



-- El.row Button.attrs
--     [ Icon.viewIcon stethoscope
--         |> El.html
--         |> El.el
--             [ El.width (El.fill |> El.maximum 48)
--             , El.height (El.fill |> El.maximum 48)
--             , El.padding 10
--             , El.alignLeft
--             ]
--     , El.text "DIAGNOSIS"
--     ]
-- /* Auto Layout */
-- display: flex;
-- flex-direction: column;
-- padding: 16px 32px;
-- position: absolute;
-- width: 131px;
-- height: 51px;
-- left: 40px;
-- top: 191px;
-- /* Green/Primary */
-- background: #5DB075;
-- border-radius: 100px;
-- Legacy


attrs =
    [ El.width (El.px 250)
    , El.height (El.px 80)
    , El.centerX
    , El.centerY
    , Background.color Color.white
    , Font.color Color.black
    , Border.shadow
        { offset = ( 2, 4 )
        , size = 1
        , blur = 4
        , color = El.rgba 0 0 0 0.25
        }
    , El.centerX
    , El.paddingXY 20 10
    , El.spacing 20
    ]


action : String -> Icon -> msg -> Element msg
action title icon msg =
    row
        [ width fill
        , Border.rounded 5
        , Font.size 16
        , Font.bold
        , Font.color Color.primary
        , Background.color Color.secondary
        , onClick msg
        ]
        [ el [ paddingXY 24 12 ] (text title)
        , el [ padding 12, alignRight ]
            (Icon.viewIcon icon
                |> html
                |> el
                    [ width (px 32)
                    , height (px 32)
                    ]
            )
        ]
