module UI.Page exposing (..)

import Element as El exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Element.Region as Region
import FontAwesome.Attributes as IconAttr
import FontAwesome.Icon as Icon exposing (Icon)
import FontAwesome.Solid exposing (arrowLeft, arrowRight, bars, home, lifeRing, notesMedical, spinner, stethoscope, userCircle, userMd)
import Html
import Html.Attributes
import Route exposing (..)
import Svg.Attributes as SvgA
import UI.Color as Color
import UI.Input as Input


container : List (Element msg) -> Element msg
container =
    El.column
        [ El.width El.fill
        , El.height El.fill
        , El.padding 16
        , Font.color Color.black
        , Font.size 14
        ]



-- data


type alias Header =
    { back : Maybe String
    , title : String
    , forward : Maybe String
    }



--


header : String -> Element msg
header str =
    viewHeaderLayout none str none


headerWithBack : msg -> String -> Element msg
headerWithBack msg str =
    viewHeaderLayout (viewArrow arrowLeft msg) str none



-- VIEW


viewHeaderLayout : Element msg -> String -> Element msg -> Element msg
viewHeaderLayout back title forward =
    El.row
        [ El.width El.fill
        , El.paddingEach
            { bottom = 16
            , top = 0
            , left = 0
            , right = 0
            }
        , Font.size 30
        , inFront back
        , inFront forward
        ]
        [ El.el [ Font.bold, El.centerX ] (El.text title) ]


viewArrow : Icon -> msg -> Element msg
viewArrow icon msg =
    el
        [ El.paddingEach
            { bottom = 16
            , top = 0
            , left = 0
            , right = 0
            }
        , height fill
        ]
        (Input.button
            [ width (px 16)
            , height (px 16)
            , Font.color Color.gray
            , alignLeft
            , centerY
            ]
            { onPress = Just msg
            , label =
                icon
                    |> Icon.viewIcon
                    |> html
            }
        )



-- HEADERS


h1 : String -> Element msg
h1 str =
    el
        [ Font.bold
        , Font.size 30
        ]
        (text str)


h2 : String -> Element msg
h2 str =
    el
        [ Font.bold
        , Font.size 24
        ]
        (text str)


h3 : String -> Element msg
h3 str =
    el
        [ Font.bold
        , Font.size 20
        ]
        (text str)


h4 : String -> Element msg
h4 str =
    el
        [ Font.size 16 ]
        (text str)



-- BORDER


border : Element msg
border =
    el
        [ width fill
        , Border.color Color.gray
        , Border.widthEach
            { bottom = 2
            , left = 0
            , right = 0
            , top = 0
            }
        ]
        none



-- NOTIFICATION


generalError : msg -> msg -> Element msg
generalError closeDialog tryAgain =
    "The application has encountered an http error."
        |> error closeDialog tryAgain


notification : String -> Element msg
notification message =
    column
        [ width fill
        , padding 16
        , alignBottom
        , Border.roundEach
            { topLeft = 16
            , topRight = 16
            , bottomLeft = 0
            , bottomRight = 0
            }
        , Border.shadow
            { offset = ( 0, 0 )
            , size = 0
            , blur = 20
            , color = El.rgba 0 0 0 0.25
            }
        , Background.color Color.gray
        , spacing 16
        ]
        [ el [ centerX ] (paragraph [ Font.center ] [ text message ])
        , row
            [ width fill
            ]
            [ el
                [ Background.color Color.black
                , height (px 4)
                , width (px 134)
                , centerX
                , centerY
                , Border.rounded 2
                ]
                none
            ]
        ]


error : msg -> msg -> String -> Element msg
error reset tryAgain explation =
    column
        [ width fill
        , height fill
        , Background.color (rgba 0 0 0 0.6)
        ]
        [ column
            [ width fill
            , padding 16
            , alignBottom
            , Border.roundEach
                { topLeft = 16
                , topRight = 16
                , bottomLeft = 0
                , bottomRight = 0
                }
            , Border.shadow
                { offset = ( 0, 0 )
                , size = 0
                , blur = 20
                , color = El.rgba 0 0 0 0.25
                }
            , Background.color Color.white
            , spacing 16
            ]
            [ row
                [ width fill
                ]
                [ el
                    [ Background.color Color.gray
                    , Border.rounded 2
                    , height (px 4)
                    , width (px 16)
                    , centerX
                    , centerY
                    ]
                    none
                ]
            , el [ centerX ] (h1 "Error")
            , el [ centerX ] (paragraph [ Font.center ] [ text explation ])
            , Input.buttonPrimary "Reset State" Nothing reset
            , el [ centerX ] (Input.buttonLink tryAgain "Try it again")
            , row
                [ width fill
                ]
                [ el
                    [ Background.color Color.black
                    , height (px 4)
                    , width (px 134)
                    , centerX
                    , centerY
                    , Border.rounded 2
                    ]
                    none
                ]
            ]
        ]



-- LOADING


loading : Element msg
loading =
    el
        [ width fill
        , height fill
        , Background.color (rgba 0 0 0 0.6)
        ]
        (el [ centerX, centerY ]
            (Icon.viewIcon spinner
                |> html
                |> el
                    [ width (px 100)
                    , height (px 100)
                    , Font.color (rgba 0 0 0 0.3)
                    ]
            )
        )



-- BOTTOM BAR


type Menu
    = Home
    | Screeners
    | Doctor
    | Tools
    | Profile


toRoute : Menu -> Route
toRoute menu =
    case menu of
        Home ->
            Route.Home

        Screeners ->
            Route.Screeners

        Doctor ->
            Route.Doctor

        Tools ->
            Route.Tools

        Profile ->
            Route.Profile


toIcon : Menu -> Icon
toIcon element =
    case element of
        Home ->
            home

        Screeners ->
            notesMedical

        Doctor ->
            userMd

        Tools ->
            lifeRing

        Profile ->
            userCircle


listMenu : List Menu
listMenu =
    [ Home, Screeners, Doctor, Tools, Profile ]


bottomBar : Menu -> Element msg
bottomBar active =
    let
        viewIcon_ color x =
            el [ width fill, height fill ]
                (link
                    [ Font.color color
                    , centerX
                    , centerY
                    ]
                    { url = Route.toString (toRoute x)
                    , label =
                        toIcon x
                            |> Icon.viewStyled
                                [ SvgA.width "24"
                                , SvgA.height "24"
                                ]
                            |> html
                    }
                )

        viewIcon element =
            if active == element then
                element
                    |> viewIcon_ Color.primary

            else
                element
                    |> viewIcon_ Color.gray

        icons =
            listMenu
                |> List.map viewIcon
    in
    column
        [ width
            (fill
                |> maximum 600
            )
        , Background.color Color.white
        , htmlAttribute (Html.Attributes.style "position" "fixed")
        , htmlAttribute (Html.Attributes.style "bottom" "0")
        , htmlAttribute (Html.Attributes.style "bottom" "0")
        ]
        [ border
        , El.row
            [ padding 16
            , El.spaceEvenly
            , El.width El.fill
            , El.alignBottom
            ]
            icons
        ]


bottomEl el =
    column
        [ width
            (fill
                |> maximum 600
            )
        , htmlAttribute (Html.Attributes.style "position" "fixed")
        , htmlAttribute (Html.Attributes.style "bottom" "0")
        , htmlAttribute (Html.Attributes.style "bottom" "0")
        ]
        [ el ]
