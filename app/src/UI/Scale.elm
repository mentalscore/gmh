module UI.Scale exposing (..)

import Html exposing (Html)
import Html.Attributes
import Svg exposing (..)
import Svg.Attributes exposing (..)


view : String -> Maybe Int -> Html msg
view label value =
    svg
        [ viewBox "0 0 250 250" ]
        [ circle
            [ fill "none"
            , stroke "#eee"
            , strokeWidth "3"
            , cx "125"
            , cy "125"
            , r (String.fromFloat radius)
            , Html.Attributes.style "transform" "rotate(-90deg)"
            , Html.Attributes.style "transform-origin" "50% 50%"
            ]
            []
        , circle
            [ fill "none"
            , stroke (Maybe.withDefault "0" <| Maybe.map color value)
            , strokeDasharray dasharray
            , strokeDashoffset
                (Maybe.map setProgress value
                    |> Maybe.map String.fromFloat
                    |> Maybe.withDefault "0"
                )
            , strokeWidth "9"
            , cx "125"
            , cy "125"
            , r (String.fromFloat radius)
            , Html.Attributes.style "transition" "0.35s stroke-dashoffset"
            , Html.Attributes.style "transform" "rotate(-90deg)"
            , Html.Attributes.style "transform-origin" "50% 50%"
            ]
            []
        , text_
            [ x "50%"
            , y "50%"
            , textAnchor "middle"
            , fontFamily "monospace"
            , fontSize "40px"
            , fontWeight "bold"
            ]
            [ Maybe.map String.fromInt value
                |> Maybe.withDefault ""
                |> text
            ]
        , text_
            [ x "50%"
            , fontFamily "monospace"
            , textAnchor "middle"
            , fontSize "18px"
            , y "150"
            ]
            [ text label ]
        ]


viewIndication : Int -> Int -> Html msg
viewIndication value max =
    -- let
    --     value : Int
    --     value =
    --         round (100 * toFloat points / toFloat max)
    -- in
    svg
        [ viewBox "0 0 250 250" ]
        [ circle
            [ fill "none"
            , stroke "#eee"
            , strokeWidth "3"
            , cx "125"
            , cy "125"
            , r (String.fromFloat radius)
            , Html.Attributes.style "transform" "rotate(-90deg)"
            , Html.Attributes.style "transform-origin" "50% 50%"
            ]
            []
        , circle
            [ fill "none"
            , stroke (color value)
            , strokeDasharray dasharray
            , strokeDashoffset
                (String.fromFloat (setProgress value))
            , strokeWidth "9"
            , cx "125"
            , cy "125"
            , r (String.fromFloat radius)
            , Html.Attributes.style "transition" "0.35s stroke-dashoffset"
            , Html.Attributes.style "transform" "rotate(-90deg)"
            , Html.Attributes.style "transform-origin" "50% 50%"
            ]
            []
        , text_
            [ x "50%"
            , y "50%"
            , textAnchor "middle"
            , fontFamily "monospace"
            , fontSize "40px"
            , fontWeight "bold"
            ]
            [ String.fromInt value
                |> text
            ]
        , text_
            [ x "50%"
            , fontFamily "monospace"
            , textAnchor "middle"
            , fontSize "18px"
            , y "150"
            ]
            [ text "points" ]
        ]


radius : Float
radius =
    120


circumference =
    radius * 2 * pi


dasharray =
    String.fromFloat circumference
        ++ " "
        ++ String.fromFloat circumference


setProgress : Int -> Float
setProgress percent =
    circumference - toFloat percent / 100 * circumference


color : Int -> String
color p =
    if p >= 0 && p < 20 then
        "#eb4841"

    else if p >= 20 && p < 40 then
        "#f48847"

    else if p >= 40 && p < 60 then
        "#fcc952"

    else if p >= 60 && p < 80 then
        "#a4c34c"

    else
        "#4ec04e"



-- colorA =
--     rgb255 78 192 78
-- colorB =
--     rgb255 164 195 76
-- colorC =
--     rgb255 255 200 74
-- colorD =
--     rgb255 244 136 71
-- colorE =
--     rgb255 235 72 65


bar : String -> Maybe Int -> Html msg
bar label oppositePerc =
    let
        perc =
            Maybe.map (\x -> 100 - x) oppositePerc

        getColor : String
        getColor =
            perc
                |> Maybe.map color
                |> Maybe.withDefault "0"

        length : String
        length =
            perc
                |> Maybe.map
                    (String.fromInt
                        << round
                        << (\p -> 135 * toFloat p / 100 + 10)
                    )
                |> Maybe.withDefault "0"
    in
    svg
        [ viewBox "0 0 150 35" ]
        [ text_
            [ x "14"
            , y "12"
            , fontFamily "monospace"
            , fontSize "12px"
            ]
            [ text label ]
        , line
            [ stroke "#eee"
            , fill "purple"
            , strokeWidth "10"
            , x1 "10"
            , y1 "25"
            , x2 "145"
            , y2 "25"
            , strokeLinecap "round"
            ]
            []
        , line
            [ stroke getColor
            , strokeWidth "10"
            , x1 "10"
            , y1 "25"
            , x2 length
            , y2 "25"
            , strokeLinecap "round"
            , Html.Attributes.style "transition" "0.35s all"
            ]
            []
        ]


verticalBar : Int -> Html msg
verticalBar perc =
    let
        getColor : String
        getColor =
            color perc

        length : String
        length =
            perc
                |> (String.fromInt
                        << round
                        << (\p -> 280 * toFloat p / 100 + 10)
                   )
    in
    svg
        [ viewBox "0 0 300 30" ]
        [ line
            [ stroke "#eee"
            , fill "purple"
            , strokeWidth "10"
            , x1 "10"
            , y1 "20"
            , x2 "290"
            , y2 "20"
            , strokeLinecap "round"
            ]
            []
        , line
            [ stroke getColor
            , strokeWidth "10"
            , x1 "10"
            , y1 "20"
            , x2 length
            , y2 "20"
            , strokeLinecap "round"
            , Html.Attributes.style "transition" "0.35s all"
            ]
            []
        ]
