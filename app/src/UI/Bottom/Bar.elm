module UI.Bottom.Bar exposing (..)

import Browser exposing (Document)
import Element as El exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import FontAwesome.Attributes as IconAttr
import FontAwesome.Icon as Icon exposing (Icon)
import FontAwesome.Solid exposing (bars)
import Html exposing (Html)
import Html.Attributes
import Route exposing (Route)
import UI.Button as Button
import UI.Color as Color
import UI.Page


footer : Element msg
footer =
    el
        [ htmlAttribute (Html.Attributes.style "position" "fixed")
        , htmlAttribute (Html.Attributes.style "bottom" "0")
        , htmlAttribute (Html.Attributes.style "left" "0")
        , htmlAttribute (Html.Attributes.style "width" "100vw")
        ]
        (row [ width fill, Background.color Color.gray ]
            [ text "Footer asdfa sfdsadf "

            -- , Button.simple label icon onPress
            ]
        )
