module UI.Dialog exposing (..)

import Element as El exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Element.Region as Region
import FontAwesome.Attributes as IconAttr
import FontAwesome.Icon as Icon exposing (Icon)
import FontAwesome.Solid exposing (check)
import UI.Button as Button
import UI.Color as Color
import UI.Page as Page


type alias Dialog msg =
    { title : String
    , content : String
    , label : String
    , msg : msg
    }


view : Dialog msg -> Element msg
view { title, content, label, msg } =
    el
        [ width fill
        , height fill
        , padding 32
        , Background.color Color.primary
        ]
        (column
            [ centerX
            , centerY
            , Border.shadow
                { offset = ( 2, 4 )
                , size = 0
                , blur = 4
                , color = rgba255 0 0 0 0.25
                }
            , Border.rounded 8
            , Background.color Color.white
            ]
            [ column [ centerX, centerY, El.paddingXY 16 32, spacing 32 ]
                [ el [ centerX ] (Page.h2 title)
                , paragraph [ Font.size 14, padding 16 ]
                    [ el
                        [ centerX ]
                        (text content)
                    ]
                , el [ width fill ]
                    (Button.view
                        { label = label
                        , icon = Nothing
                        , onPress = msg
                        , style = Button.Primary
                        }
                    )
                ]
            ]
        )
