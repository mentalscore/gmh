module UI.Input exposing (..)

import Element as El exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Element.Region as Region
import FontAwesome.Attributes as IconAttr
import FontAwesome.Icon as Icon exposing (Icon)
import FontAwesome.Solid exposing (bars, stethoscope)
import UI.Color as Color



-- EXPORT


checkbox : String -> Bool -> (Bool -> msg) -> Element msg
checkbox label isChecked onChange =
    Input.checkbox []
        { onChange = onChange
        , icon = Input.defaultCheckbox
        , checked = isChecked
        , label = Input.labelRight [] (text label)
        }


email : String -> String -> (String -> msg) -> Element msg
email placeholder value onChange =
    Input.email []
        { onChange = onChange
        , text = value
        , placeholder = Just (Input.placeholder [] (text placeholder))
        , label = Input.labelHidden placeholder
        }


currentPassword : String -> String -> (String -> msg) -> Element msg
currentPassword placeholder value onChange =
    Input.currentPassword []
        { onChange = onChange
        , text = value
        , placeholder = Just (Input.placeholder [] (text placeholder))
        , label = Input.labelHidden placeholder
        , show = False
        }


buttonPrimary : String -> Maybe Icon -> msg -> Element msg
buttonPrimary label icon onPress =
    view
        { label = label
        , icon = icon
        , onPress = onPress
        , style = Primary
        }


buttonSecondary : String -> Maybe Icon -> msg -> Element msg
buttonSecondary label icon onPress =
    view
        { label = label
        , icon = icon
        , onPress = onPress
        , style = Secondary
        }


buttonLink : msg -> String -> Element msg
buttonLink onPress label =
    Input.button
        [ Background.color Color.secondary
        , Font.color Color.primary
        ]
        { onPress = Just onPress
        , label =
            row
                [ centerX
                , centerY
                , spacing 10
                ]
                [ text label ]
        }


buttonSecondaryLink : msg -> String -> Element msg
buttonSecondaryLink onPress label =
    Input.button
        [ Font.color Color.darkGray ]
        { onPress = Just onPress
        , label =
            row
                [ centerX
                , centerY
                , spacing 10
                ]
                [ text label ]
        }



-- DATA


type alias Button msg =
    { label : String
    , icon : Maybe Icon
    , onPress : msg
    , style : Style
    }


type Style
    = Primary
    | Secondary



-- VIEW


view : Button msg -> Element msg
view { label, icon, onPress, style } =
    let
        colors =
            case style of
                Primary ->
                    [ Background.color Color.primary
                    , Font.color Color.white
                    ]

                Secondary ->
                    [ Background.color Color.secondary
                    , Font.color Color.primary
                    ]
    in
    Input.button
        ([ height (px 51)
         , width fill
         , Border.rounded 25
         ]
            ++ colors
        )
        { onPress = Just onPress
        , label =
            row
                [ centerX
                , centerY
                , spacing 10
                ]
                [ case icon of
                    Just i ->
                        Icon.viewIcon i
                            |> html
                            |> el
                                [ width (px 20)
                                , height (px 20)
                                ]

                    Nothing ->
                        none
                , text label
                ]
        }



-- El.row Button.attrs
--     [ Icon.viewIcon stethoscope
--         |> El.html
--         |> El.el
--             [ El.width (El.fill |> El.maximum 48)
--             , El.height (El.fill |> El.maximum 48)
--             , El.padding 10
--             , El.alignLeft
--             ]
--     , El.text "DIAGNOSIS"
--     ]
-- /* Auto Layout */
-- display: flex;
-- flex-direction: column;
-- padding: 16px 32px;
-- position: absolute;
-- width: 131px;
-- height: 51px;
-- left: 40px;
-- top: 191px;
-- /* Green/Primary */
-- background: #5DB075;
-- border-radius: 100px;
-- Legacy


attrs =
    [ El.width (El.px 250)
    , El.height (El.px 80)
    , El.centerX
    , El.centerY
    , Background.color Color.white
    , Font.color Color.black
    , Border.shadow
        { offset = ( 2, 4 )
        , size = 1
        , blur = 4
        , color = El.rgba 0 0 0 0.25
        }
    , El.centerX
    , El.paddingXY 20 10
    , El.spacing 20
    ]
