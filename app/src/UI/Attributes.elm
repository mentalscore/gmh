module UI.Attributes exposing (..)

import Element exposing (..)
import Html exposing (Html)
import Html.Events
import Json.Decode as Json


{-| -}
onKey : String -> msg -> Attribute msg
onKey desiredCode msg =
    let
        decode code =
            if code == desiredCode then
                Json.succeed msg

            else
                Json.fail "Not the enter key"

        isKey =
            Json.field "key" Json.string
                |> Json.andThen decode
    in
    Element.htmlAttribute <|
        Html.Events.preventDefaultOn "keyup"
            (Json.map (\fired -> ( fired, True )) isKey)


{-| -}
onEnter : msg -> Attribute msg
onEnter msg =
    onKey enter msg


enter : String
enter =
    "Enter"
