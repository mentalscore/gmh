module UI.Dialog.Radio exposing (..)

import Element as El exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Element.Region as Region
import FontAwesome.Attributes as IconAttr
import FontAwesome.Icon as Icon exposing (Icon)
import FontAwesome.Solid exposing (check)
import Html.Attributes
import UI.Button as Button
import UI.Color as Color
import UI.Page as Page



-- CONST


type alias Config data msg =
    { cancelled : msg
    , submitted : data -> msg
    , active : data
    , title : String
    , toString : data -> String
    , cancelLabel : String
    }


view : Config data msg -> List data -> Element msg
view { cancelled, submitted, title, active, toString, cancelLabel } list =
    column
        [ width fill
        , htmlAttribute (Html.Attributes.style "height" "100vh")
        , inFront
            (el [ width fill, centerX, centerY, padding 32 ]
                (column
                    [ width fill
                    , height fill
                    , Border.shadow
                        { offset = ( 2, 4 )
                        , size = 0
                        , blur = 4
                        , color = rgba255 0 0 0 0.25
                        }
                    , Border.rounded 8
                    , Background.color Color.white
                    ]
                    [ column
                        [ centerX
                        , centerY
                        , El.padding 32
                        , spacing 32
                        , width fill
                        ]
                        [ paragraph [] [ Page.h3 title ]
                        , Input.radio
                            [ width fill
                            ]
                            { onChange = submitted
                            , options =
                                List.map (viewOption toString) list
                            , selected = Just active
                            , label = Input.labelHidden ""
                            }
                        , row [ centerX, spacing 32 ]
                            [ Button.simple (String.toUpper cancelLabel) cancelled ]
                        ]
                    ]
                )
            )
        ]
        [ el
            [ width fill
            , height fill
            , Background.color Color.black
            , alpha 0.8
            ]
            (text "")
        ]


viewOption : (data -> String) -> data -> Input.Option data msg
viewOption toString data =
    let
        option =
            el [ padding 16 ] (text (toString data))
    in
    Input.option data option
