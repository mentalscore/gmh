module UI.Dialog.Input exposing (..)

import Element as El exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Element.Region as Region
import FontAwesome.Attributes as IconAttr
import FontAwesome.Icon as Icon exposing (Icon)
import FontAwesome.Solid exposing (check)
import Html.Attributes
import UI.Button as Button
import UI.Color as Color
import UI.Page as Page



-- CONST


attrId : String
attrId =
    "dialog-input"



-- STATE


type alias State =
    { val : Maybe String }


init : State
init =
    { val = Nothing }


toValue : State -> Maybe String
toValue { val } =
    val


type Msg
    = Changed String


update : Msg -> State -> State
update msg state =
    case msg of
        Changed str ->
            { state | val = Just str }


type alias DialogInput =
    { title : String
    , value : String
    }


type alias Config msg =
    { toMsg : Msg -> msg
    , cancelled : msg
    , submitted : String -> msg
    , cancelLabel : String
    , okLabel : String
    }


view : Config msg -> DialogInput -> State -> Element msg
view { toMsg, cancelled, submitted, cancelLabel, okLabel } { title, value } state =
    column
        [ width fill
        , htmlAttribute (Html.Attributes.style "height" "100vh")
        , inFront
            (el [ width fill, centerX, centerY, padding 32 ]
                (column
                    [ width fill
                    , height fill
                    , Border.shadow
                        { offset = ( 2, 4 )
                        , size = 0
                        , blur = 4
                        , color = rgba255 0 0 0 0.25
                        }
                    , Border.rounded 8
                    , Background.color Color.white
                    ]
                    [ column
                        [ centerX
                        , centerY
                        , El.padding 32
                        , spacing 32
                        , width fill
                        ]
                        [ paragraph [] [ Page.h3 title ]
                        , Input.text
                            [ htmlAttribute <| Html.Attributes.id attrId ]
                            { onChange = toMsg << Changed
                            , text = Maybe.withDefault "" state.val
                            , placeholder = Just (Input.placeholder [] (text value))
                            , label = Input.labelHidden ""
                            }
                        , row [ alignRight, spacing 32 ]
                            [ Button.simple (String.toUpper cancelLabel) cancelled
                            , state.val
                                |> Maybe.map submitted
                                |> Maybe.withDefault cancelled
                                |> Button.simple (String.toUpper okLabel)
                            ]
                        ]
                    ]
                )
            )
        ]
        [ el
            [ width fill
            , height fill
            , Background.color Color.black
            , alpha 0.8
            ]
            (text "")
        ]
