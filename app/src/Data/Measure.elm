module Data.Measure exposing (..)

import Data.Level exposing (..)


scoreMeasure : Int -> Measure
scoreMeasure p =
    { header = "Your Score"
    , points = p
    , max = 100
    , level = scoreCutPoint p
    , levels =
        [-- ( Minimal, "0-20" )
         -- , ( Mild, "20-40" )
         -- , ( Moderate, "40-60" )
         -- , ( ModeratelySevere, "60-80" )
         -- , ( Severe, "80-100" )
        ]
    }


phq9Measure : Int -> Measure
phq9Measure p =
    { header = "Your Depression Measure"
    , points = p
    , max = 27
    , level = phq9CutPoint p
    , levels =
        [ ( Minimal, "0-5" )
        , ( Mild, "5-10" )
        , ( Moderate, "10-15" )
        , ( ModeratelySevere, "15-20" )
        , ( Severe, "20-27" )
        ]
    }


gad7Measure : Int -> Measure
gad7Measure p =
    { header = "Your Anxiety Measure"
    , points = p
    , max = 21
    , level = gad7CutPoint p
    , levels =
        [ ( Minimal, "0-5" )
        , ( Mild, "5-10" )
        , ( Moderate, "10-15" )
        , ( Severe, "15-21" )
        ]
    }


phq15Measure : Int -> Measure
phq15Measure p =
    { header = "Your Somatic Symptom Severity"
    , points = p
    , max = 30
    , level = phq15CutPoint p
    , levels =
        [ ( Minimal, "0-5" )
        , ( Mild, "5-10" )
        , ( Moderate, "10-15" )
        , ( Severe, "15-30" )
        ]
    }


type alias Measure =
    { header : String
    , points : Int
    , max : Int
    , level : Level
    , levels : List ( Level, String )
    }


scoreCutPoint : Int -> Level
scoreCutPoint p =
    if p >= 0 && p < 10 then
        Minimal

    else if p >= 20 && p < 40 then
        Mild

    else if p >= 40 && p < 60 then
        Moderate

    else if p >= 60 && p < 80 then
        ModeratelySevere

    else
        Severe


phq9CutPoint : Int -> Level
phq9CutPoint p =
    if p >= 0 && p < 5 then
        Minimal

    else if p >= 5 && p < 10 then
        Mild

    else if p >= 10 && p < 15 then
        Moderate

    else if p >= 15 && p < 20 then
        ModeratelySevere

    else
        Severe


gad7CutPoint : Int -> Level
gad7CutPoint p =
    if p >= 0 && p < 5 then
        Minimal

    else if p >= 5 && p < 10 then
        Mild

    else if p >= 10 && p < 15 then
        Moderate

    else
        Severe


phq15CutPoint : Int -> Level
phq15CutPoint p =
    if p >= 0 && p < 5 then
        Minimal

    else if p >= 5 && p < 10 then
        Mild

    else if p >= 10 && p < 15 then
        Moderate

    else
        Severe
