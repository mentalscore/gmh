module Data.Notifications.Activities exposing (..)

import Json.Decode as JD
import Json.Encode as JE


default : Data
default =
    { timeout = 2000
    , title = "Title"
    , body = "Body"
    }


type alias Data =
    { timeout : Int
    , title : String
    , body : String
    }


encode : Data -> JE.Value
encode { timeout, title, body } =
    JE.object
        [ ( "timeout", JE.int timeout )
        , ( "title", JE.string title )
        , ( "body", JE.string body )
        ]
