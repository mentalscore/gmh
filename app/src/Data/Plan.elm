module Data.Plan exposing (..)

import Json.Decode as JD
import Json.Encode as Encode exposing (Value)


type alias Plan =
    { uuid : String
    , price : Int
    , type_ : Type
    , createdAt : Int
    , endAt : Int
    }


types =
    [ MonthlyRecurring, Annual ]


type Type
    = MonthlyRecurring
    | Annual


toSeconds : Type -> Int
toSeconds type_ =
    case type_ of
        MonthlyRecurring ->
            2629746 + 172800

        Annual ->
            31556952 + 604800



-- DECODE


decode : JD.Decoder Plan
decode =
    JD.map5 Plan
        (JD.field "planUUID" JD.string)
        (JD.field "planPrice" JD.int)
        (JD.field "planType" decodeType)
        (JD.field "planCreatedAt" JD.int)
        (JD.field "planEndAt" JD.int)


decodeType : JD.Decoder Type
decodeType =
    JD.map fromString JD.string


fromString : String -> Type
fromString str =
    case str of
        "monthlyRecurring" ->
            MonthlyRecurring

        "annual" ->
            Annual

        _ ->
            MonthlyRecurring



-- ENCODE


encodeType : Type -> Value
encodeType plan =
    Encode.string (toString plan)


toString : Type -> String
toString plan =
    case plan of
        MonthlyRecurring ->
            "monthlyRecurring"

        Annual ->
            "annual"
