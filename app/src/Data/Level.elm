module Data.Level exposing (..)


type Level
    = Minimal
    | Mild
    | Moderate
    | ModeratelySevere
    | Severe


levelToString : Level -> String
levelToString level =
    case level of
        Minimal ->
            "Minimal"

        Mild ->
            "Mild"

        Moderate ->
            "Moderate"

        ModeratelySevere ->
            "Moderately Severe"

        Severe ->
            "Severe"
