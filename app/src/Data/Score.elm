module Data.Score exposing (..)

import Env exposing (endpoint)
import Http
import Json.Decode as JD


type alias Score =
    { score : Int
    , anxietyScale : Maybe Int
    , depressionScale : Maybe Int
    , somaticScale : Maybe Int
    , subjectiveScale : Maybe Int
    , createdAt : Int
    }


decode : JD.Decoder Score
decode =
    JD.map6 Score
        (JD.field "score" JD.int)
        (JD.field "scoreAnxietyScale" (JD.nullable JD.int))
        (JD.field "scoreDepressionScale" (JD.nullable JD.int))
        (JD.field "scoreSomaticScale" (JD.nullable JD.int))
        (JD.field "scoreSubjectiveScale" (JD.nullable JD.int))
        (JD.field "scoreCreatedAt" JD.int)


get : (Result Http.Error (List Score) -> msg) -> String -> Cmd msg
get gotScore uuid =
    Http.get
        { url = endpoint ++ "accounts/" ++ uuid ++ "/scores"
        , expect = Http.expectJson gotScore (JD.list decode)
        }
