module Data.NotificationsStatus exposing (..)

import Json.Decode as JD


type NotificationsStatus
    = Granted
    | Denied
    | Other String


isGranted : NotificationsStatus -> Bool
isGranted status =
    case status of
        Granted ->
            True

        _ ->
            False


fromString : String -> NotificationsStatus
fromString str =
    case str of
        "granted" ->
            Granted

        "denied" ->
            Denied

        other ->
            Other other


decoder : JD.Decoder NotificationsStatus
decoder =
    JD.map fromString JD.string
