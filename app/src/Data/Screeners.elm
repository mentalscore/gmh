module Data.Screeners exposing (..)


type Screener
    = PHQ15 Int -- Somatic symptom scale from PHQ.
    | PHQ9 Int -- Depression scale from PHQ
    | GAD7 Int -- Anxiety measure developed after PHQ
    | Subjective Int -- Subjective test


initSubjective : Screener
initSubjective =
    Subjective 0
