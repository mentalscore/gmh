module Data.Indication exposing (..)

import Html exposing (Html)
import Json.Decode as Decode exposing (Decoder)
import Json.Encode as Encode exposing (Value)
import Url.Parser



-- TYPES


type Indication
    = Score
    | Depression
    | Anxiety
    | Psychosomatic



-- CREATE


decoder : Decoder (Maybe Indication)
decoder =
    Decode.map fromString Decode.string



-- TRANSFORM


encode : Indication -> Value
encode indication =
    Encode.string (toString indication)


toString : Indication -> String
toString indication =
    case indication of
        Score ->
            "score"

        Depression ->
            "depression"

        Anxiety ->
            "anxiety"

        Psychosomatic ->
            "psychosomatic"


toMax : Indication -> Int
toMax indication =
    case indication of
        Score ->
            100

        Depression ->
            27

        Anxiety ->
            21

        Psychosomatic ->
            30


toShort : Indication -> Int -> String
toShort indication =
    case indication of
        Score ->
            scoreShort

        Depression ->
            derpressionShort

        Anxiety ->
            anxietyShort

        Psychosomatic ->
            psychosomaticShort


scoreShort : Int -> String
scoreShort p =
    if p >= 0 && p < 20 then
        "Severe Mental State"

    else if p >= 20 && p < 40 then
        "Moderately Mental State"

    else if p >= 40 && p < 60 then
        "Moderate Mental State"

    else if p >= 60 && p < 80 then
        "Mild Mental State"

    else
        "Good Mental State"


derpressionShort : Int -> String
derpressionShort p =
    if p >= 0 && p < 20 then
        "Severe Derpression"

    else if p >= 20 && p < 40 then
        "Moderately Derpression"

    else if p >= 40 && p < 60 then
        "Moderate Derpression"

    else if p >= 60 && p < 80 then
        "Mild Derpression"

    else
        "Minimal Derpression"


anxietyShort : Int -> String
anxietyShort p =
    if p >= 0 && p < 20 then
        "Severe Anxiety"

    else if p >= 20 && p < 40 then
        "Moderately Anxiety"

    else if p >= 40 && p < 60 then
        "Moderate Anxiety"

    else if p >= 60 && p < 80 then
        "Mild Anxiety"

    else
        "Minimal Anxiety"


psychosomaticShort : Int -> String
psychosomaticShort p =
    if p >= 0 && p < 20 then
        "Severe Psychosomatic Disorder"

    else if p >= 20 && p < 40 then
        "Moderately Psychosomatic Disorder"

    else if p >= 40 && p < 60 then
        "Moderate Psychosomatic Disorder"

    else if p >= 60 && p < 80 then
        "Mild Psychosomatic Disorder"

    else
        "Minimal Psychosomatic Disorder"


toDescription : Indication -> Int -> String
toDescription indication =
    case indication of
        Score ->
            scoreDescription

        Depression ->
            derpressionDescription

        Anxiety ->
            anxietyDescription

        Psychosomatic ->
            psychosomaticDescription


scoreDescription : Int -> String
scoreDescription p =
    if p >= 0 then
        "We are proud of you, great job! It looks like you are doing very good, keep on working on your negative thoughts and try to be mindful of your diet."

    else
        "Mental Score"


derpressionDescription : Int -> String
derpressionDescription p =
    "Your results indicate that you may be experiencing symptoms of "
        ++ (if p >= 0 && p < 20 then
                "severe"

            else if p >= 20 && p < 40 then
                "moderately"

            else if p >= 40 && p < 60 then
                "moderate"

            else if p >= 60 && p < 80 then
                "mild"

            else
                "minimal"
           )
        ++ " depression."


anxietyDescription : Int -> String
anxietyDescription p =
    "Your results indicate that you may be experiencing symptoms of "
        ++ (if p >= 0 && p < 20 then
                "severe"

            else if p >= 20 && p < 40 then
                "moderately"

            else if p >= 40 && p < 60 then
                "moderate"

            else if p >= 60 && p < 80 then
                "mild"

            else
                "minimal"
           )
        ++ " anxiety."


psychosomaticDescription : Int -> String
psychosomaticDescription p =
    "Your results indicate that you may be experiencing symptoms of "
        ++ (if p >= 0 && p < 20 then
                "severe"

            else if p >= 20 && p < 40 then
                "moderately"

            else if p >= 40 && p < 60 then
                "moderate"

            else if p >= 60 && p < 80 then
                "mild"

            else
                "minimal"
           )
        ++ " psychosomatic disorder."


fromString : String -> Maybe Indication
fromString indication =
    case indication of
        "score" ->
            Just Score

        "depression" ->
            Just Depression

        "anxiety" ->
            Just Anxiety

        "psychosomatic" ->
            Just Psychosomatic

        _ ->
            Nothing


urlParser : Url.Parser.Parser (Indication -> a) a
urlParser =
    Url.Parser.custom "INDICATION" (\str -> fromString str)


toHtml : Indication -> Html msg
toHtml indication =
    Html.text <| toString indication
