module Data.Activities exposing (..)

import FontAwesome.Attributes as IconAttr
import FontAwesome.Brands exposing (google)
import FontAwesome.Icon as Icon exposing (Icon)
import FontAwesome.Solid
    exposing
        ( bars
        , beer
        , bookReader
        , check
        , comment
        , comments
        , couch
        , dumbbell
        , gamepad
        , glassCheers
        , hamburger
        , joint
        , plane
        , running
        , shoppingCart
        , smoking
        , stickyNote
        , tv
        , userFriends
        , walking
        )
import Json.Decode as JD
import Json.Encode as JE


type Activity
    = Alcohol
    | Beer
    | ChattingFriends
    | Gym
    | Joint
    | JunkFood
    | MeetFriends
    | Reading
    | Running
    | Shopping
    | Smoking
    | StayingHome
    | Vacation
    | Walking
    | WatchTV
    | PlayVideoGame


toIcon : Activity -> Icon
toIcon activity =
    case activity of
        Alcohol ->
            glassCheers

        Beer ->
            beer

        ChattingFriends ->
            comments

        Gym ->
            dumbbell

        JunkFood ->
            hamburger

        MeetFriends ->
            userFriends

        Reading ->
            bookReader

        Running ->
            running

        Shopping ->
            shoppingCart

        Smoking ->
            smoking

        Joint ->
            joint

        StayingHome ->
            couch

        Vacation ->
            plane

        Walking ->
            walking

        WatchTV ->
            tv

        PlayVideoGame ->
            gamepad


activities : List Activity
activities =
    [ Alcohol
    , Beer
    , ChattingFriends
    , Gym
    , Joint
    , JunkFood
    , MeetFriends
    , Reading
    , Running
    , Shopping
    , Smoking
    , StayingHome
    , Vacation
    , Walking
    , WatchTV
    , PlayVideoGame
    ]


toString : Activity -> String
toString activity =
    case activity of
        Alcohol ->
            "alcohol"

        Beer ->
            "beer"

        ChattingFriends ->
            "chattingFriends"

        Gym ->
            "gym"

        Joint ->
            "joint"

        JunkFood ->
            "junkFood"

        MeetFriends ->
            "meetFriends"

        Reading ->
            "reading"

        Running ->
            "running"

        Shopping ->
            "shopping"

        Smoking ->
            "smoking"

        StayingHome ->
            "stayingHome"

        Vacation ->
            "vacation"

        Walking ->
            "walking"

        WatchTV ->
            "watchTV"

        PlayVideoGame ->
            "playVideoGame"


encode : String -> Activity -> JE.Value
encode uuid activity =
    [ ( "uuid", JE.string uuid )
    , ( "activity", JE.string (toString activity) )
    ]
        |> JE.object


decode : JD.Decoder Activity
decode =
    JD.map fromString JD.string


fromString : String -> Activity
fromString str =
    case str of
        "alcohol" ->
            Alcohol

        "beer" ->
            Beer

        "chattingFriends" ->
            ChattingFriends

        "gym" ->
            Gym

        "joint" ->
            Joint

        "junkFood" ->
            JunkFood

        "meetFriends" ->
            MeetFriends

        "reading" ->
            Reading

        "running" ->
            Running

        "shopping" ->
            Shopping

        "smoking" ->
            Smoking

        "stayingHome" ->
            StayingHome

        "vacation" ->
            Vacation

        "walking" ->
            Walking

        "watchTV" ->
            WatchTV

        "playVideoGame" ->
            PlayVideoGame

        _ ->
            Reading
