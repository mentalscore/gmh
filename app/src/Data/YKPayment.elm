module Data.YKPayment exposing (..)

import Json.Decode as JD
import Json.Encode as Encode exposing (Value)


type alias YKPayment =
    { uuid : String
    , status : Status
    , responseId : String
    , confirmationURL : String
    , createdAt : Int
    , planUUID : String
    }


type Status
    = Pending
    | WaitingForCapture
    | Succeeded
    | Canceled



-- DECODE


decode : JD.Decoder YKPayment
decode =
    JD.map6 YKPayment
        (JD.field "ykPaymentUUID" JD.string)
        (JD.field "ykPaymentStatus" decodeStatus)
        (JD.field "ykPaymentResponseId" JD.string)
        (JD.field "ykPaymentConfirmationURL" JD.string)
        (JD.field "ykPaymentCreatedAt" JD.int)
        (JD.field "ykPaymentPlanUUID" JD.string)


decodeStatus : JD.Decoder Status
decodeStatus =
    JD.map fromString JD.string


fromString : String -> Status
fromString str =
    case str of
        "pending" ->
            Pending

        "waiting_for_capture" ->
            WaitingForCapture

        "succeeded" ->
            Succeeded

        "canceled" ->
            Canceled

        _ ->
            Canceled



-- ENCODE


encodeStatus : Status -> Value
encodeStatus ykPayment =
    Encode.string (toString ykPayment)


toString : Status -> String
toString ykPayment =
    case ykPayment of
        Pending ->
            "pending"

        WaitingForCapture ->
            "waiting_for_capture"

        Succeeded ->
            "succeeded"

        Canceled ->
            "canceled"
