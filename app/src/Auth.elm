module Auth exposing (..)

import Json.Decode as Decode exposing (Decoder)
import Profile exposing (Profile)
import Url exposing (Protocol(..), Url)
import Url.Parser.Query as Query


configurationFor : OAuthProvider -> OAuthConfiguration
configurationFor provider =
    let
        defaultHttpsUrl =
            { protocol = Https
            , host = ""
            , path = ""
            , port_ = Nothing
            , query = Nothing
            , fragment = Nothing
            }
    in
    case provider of
        Google ->
            { provider = Google
            , clientId = "1020909633387-dpgom37sghat641176oukkg8n4ip2jij.apps.googleusercontent.com"
            , secret = "-PuWADi1DRKQqQcs6qY7zlRo"
            , authorizationEndpoint = { defaultHttpsUrl | host = "accounts.google.com", path = "/o/oauth2/v2/auth" }
            , tokenEndpoint = { defaultHttpsUrl | host = "www.googleapis.com", path = "/oauth2/v4/token" }
            , profileEndpoint = { defaultHttpsUrl | host = "www.googleapis.com", path = "/oauth2/v1/userinfo" }
            , scope = [ "profile" ]
            , profileDecoder = Profile.decoder
            }


type alias OAuthConfiguration =
    { provider : OAuthProvider
    , authorizationEndpoint : Url
    , tokenEndpoint : Url
    , profileEndpoint : Url
    , clientId : String
    , secret : String
    , scope : List String
    , profileDecoder : Decoder Profile
    }


type OAuthProvider
    = Google
