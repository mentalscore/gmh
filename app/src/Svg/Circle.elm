module Svg.Circle exposing (..)

import Html exposing (Html)
import Html.Attributes
import Svg exposing (..)
import Svg.Attributes exposing (..)


view : Float -> Html msg
view value =
    svg
        [ viewBox "0 0 250 250" ]
        [ circle
            [ fill "none"
            , stroke "#eee"
            , strokeWidth "3"
            , cx "125"
            , cy "125"
            , r (String.fromFloat radius)
            , Html.Attributes.style "transform" "rotate(-90deg)"
            , Html.Attributes.style "transform-origin" "50% 50%"
            ]
            []
        , circle
            [ fill "none"
            , stroke (color value)
            , strokeDasharray dasharray
            , strokeDashoffset (String.fromFloat <| setProgress value)
            , strokeWidth "9"
            , cx "125"
            , cy "125"
            , r (String.fromFloat radius)
            , Html.Attributes.style "transition" "0.35s stroke-dashoffset"
            , Html.Attributes.style "transform" "rotate(-90deg)"
            , Html.Attributes.style "transform-origin" "50% 50%"
            ]
            []
        , text_
            [ x "100"
            , fontFamily "monospace"
            , fontSize "40px"
            , y "135"
            ]
            [ text <| String.fromFloat value ]
        ]


radius : Float
radius =
    100


circumference =
    radius * 2 * pi


dasharray =
    String.fromFloat circumference
        ++ " "
        ++ String.fromFloat circumference


setProgress : Float -> Float
setProgress percent =
    circumference - percent / 100 * circumference


color : Float -> String
color p =
    if p >= 0 && p < 20 then
        "#eb4841"

    else if p >= 20 && p < 40 then
        "#f48847"

    else if p >= 40 && p < 60 then
        "#fcc952"

    else if p >= 60 && p < 80 then
        "#a4c34c"

    else
        "#4ec04e"



-- colorA =
--     rgb255 78 192 78
-- colorB =
--     rgb255 164 195 76
-- colorC =
--     rgb255 255 200 74
-- colorD =
--     rgb255 244 136 71
-- colorE =
--     rgb255 235 72 65
