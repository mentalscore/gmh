// INIT ELM APP

const storageKey = "store";
let flags = JSON.parse(localStorage.getItem(storageKey));

if (flags == null) {
  flags = { language: navigator.language };
}

// class StripeCheckout extends HTMLElement {
//   constructor() {
//     super();
//     // Create a shadow root
//     const shadow = this.attachShadow({ mode: "open" });

//     // Create spans
//     const wrapper = document.createElement("span");
//     wrapper.setAttribute("class", "wrapper");

//     const info = document.createElement("span");

//     info.setAttribute("class", "info");
//     info.textContent = "Stripe Checkout";

//     const button = document.createElement("button");
//     button.id = "checkout";
//     button.textContent = "Subscribe";

//     shadow.appendChild(wrapper);
//     wrapper.appendChild(info);
//     wrapper.appendChild(button);
//   }

//   connectedCallback() {
//     console.log("connectedCallback");
//   }
//   disconnectedCallback() {
//     console.log("disconnectedCallback");
//   }
// }

// customElements.define("stripe-checkout", StripeCheckout);

const app = Elm.Main.init({ flags: flags });

app.ports.storeCache.subscribe(function (val) {
  if (val === null) {
    localStorage.removeItem(storageKey);
  } else {
    localStorage.removeItem(storageKey);
    localStorage.setItem(storageKey, JSON.stringify(val));
  }
});

app.ports.eventOut.subscribe((event) => {
  switch (event.type) {
    case "AppInstallClicked":
      installApp();
      break;
    case "SetNotificationsClicked":
      requestPermission();
      break;
    case "SetActivityNotifications":
      setNotification(event.data);
      break;

    default:
      throw new Error("Unextected object: " + x);
  }
});

// https://web.dev/codelab-make-installable/
function installApp() {
  const promptEvent = window.deferredPrompt;

  if (!promptEvent) {
    return;
  }

  promptEvent.prompt();

  window.deferredPrompt = null;
}

function showAppInstall() {
  app.ports.eventIn.send({
    type: "ShowInstallPromotion",
    data: null,
  });
}

// Register service worker to control making site work offline

if ("serviceWorker" in navigator) {
  navigator.serviceWorker
    .register("sw.js")
    .then((serviceWorker) => {
      console.log("Service Worker registered: ", serviceWorker);
    })
    .catch((error) => {
      console.error("Error registering the Service Worker: ", error);
    });
}

// Initialize deferredPrompt for use later to show browser install prompt.
let deferredPrompt;

window.addEventListener("beforeinstallprompt", (e) => {
  // Prevent the mini-infobar from appearing on mobile
  e.preventDefault();
  // Stash the event so it can be triggered later.
  window.deferredPrompt = e;
  // Update UI notify the user they can install the PWA
  showAppInstall();
  // Optionally, send analytics event that PWA install promo was shown.
  // console.log(`'beforeinstallprompt' event was fired.`);
});

function requestPermission() {
  Notification.requestPermission()
    .then(function (result) {
      app.ports.eventIn.send({
        type: "NotificationsStatusReturned",
        data: result,
      });
    })
    .catch(function (e) {
      console.log("RequestPermission errors: ", e);
      return e;
    });
}

function setNotification(data) {
  Notification.requestPermission(function (result) {
    if (result === "granted") {
      navigator.serviceWorker.getRegistration("/").then((rg) => {
        rg.showNotification(data.title, {
          body: data.body,
          icon: "./android-chrome-192x192.png",
          vibrate: [200, 100, 200, 100, 200, 100, 200],
          tag: "activities",
        });
      });
    }
  });
}
