module Page exposing (Page(..), view)

import Browser exposing (Document)
import Element as El exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import FontAwesome.Attributes as IconAttr
import FontAwesome.Icon as Icon exposing (Icon)
import FontAwesome.Solid exposing (bars)
import Html exposing (Html)
import Route exposing (Route)
import UI.Bottom.Bar as Bar
import UI.Color as Color
import UI.Page


type Page
    = Other


view : Page -> { title : String, content : Element msg } -> Document msg
view page { title, content } =
    { title = title
    , body =
        [ El.layoutWith
            { options =
                [ El.focusStyle
                    { borderColor = Nothing
                    , backgroundColor = Nothing
                    , shadow = Nothing
                    }
                ]
            }
            [ El.width (El.fill |> El.maximum 600)
            , Font.family
                [ Font.typeface "Manrope"
                , Font.typeface "Helvetica Neue"
                , Font.typeface "Helvetica"
                , Font.sansSerif
                ]
            , Font.letterSpacing 1
            ]
            content
        ]
    }
