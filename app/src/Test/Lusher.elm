module Test.Lusher exposing (..)

import Dict exposing (Dict)
import Element as El exposing (rgb255)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline exposing (required)
import Json.Encode as Encode exposing (Value)
import Set exposing (Set)
import UI.Color as Color



-- MODEL


type alias Lusher =
    { first : List String
    , second : List String
    }


create : ( a, List Color ) -> ( a, List Color ) -> Lusher
create ( _, color1 ) ( _, color2 ) =
    { first = List.map toString color1
    , second = List.map toString color2
    }



-- SERIALIZATION


encode : Dict String Lusher -> Value
encode =
    Encode.dict identity encodeLusher


encodeLusher : Lusher -> Value
encodeLusher lusher =
    Encode.object
        [ ( "first", Encode.list Encode.string lusher.first )
        , ( "second", Encode.list Encode.string lusher.second )
        ]


decoder : Decoder (Dict String Lusher)
decoder =
    Decode.dict decoderLusher


decoderLusher : Decoder Lusher
decoderLusher =
    Decode.succeed Lusher
        |> required "first" (Decode.list Decode.string)
        |> required "second" (Decode.list Decode.string)



--COLORS


toString str =
    case str of
        Blue ->
            "Blue"

        Green ->
            "Green"

        Red ->
            "Red"

        Yellow ->
            "Yellow"

        Violet ->
            "Violet"

        Brown ->
            "Brown"

        Black ->
            "Black"

        Grey ->
            "Grey"


fromString color =
    case color of
        "Blue" ->
            Just Blue

        "Green" ->
            Just Green

        "Red" ->
            Just Red

        "Yellow" ->
            Just Yellow

        "Violet" ->
            Just Violet

        "Brown" ->
            Just Brown

        "Black" ->
            Just Black

        "Grey" ->
            Just Grey

        _ ->
            Nothing


colorsSet : Set String
colorsSet =
    Set.fromList
        [ "Blue", "Green", "Red", "Yellow", "Violet", "Brown", "Black", "Grey" ]


type Color
    = Blue
    | Green
    | Red
    | Yellow
    | Violet
    | Brown
    | Black
    | Grey


toRGB color =
    case color of
        Blue ->
            rgb255 27 27 91

        Green ->
            rgb255 37 72 66

        Red ->
            rgb255 156 35 78

        Yellow ->
            rgb255 244 218 59

        Violet ->
            rgb255 223 69 35

        Brown ->
            rgb255 174 93 40

        Black ->
            rgb255 21 8 18

        Grey ->
            rgb255 118 111 101


meaning color =
    case color of
        Blue ->
            "Depth of Feeling  \"passive\", concentric, tranquility, calm, tenderness"

        Green ->
            "Elasticity of Will passive, concentric, defensive, persistence, self-esteem/assertion, pride, control"

        Red ->
            "Force of Will ex-centric, active aggressive, competitive, action, desire, excitement, sexuality"

        Yellow ->
            "Spontaneity ex-centric, active, projective, aspiring, expectancy, exhilaration"

        Violet ->
            "Identification unrealistic/ wishful fulfillment, charm, enchantment"

        Brown ->
            "Bodily senses, indicates the body's condition"

        Black ->
            "Nothingness, renunciation, surrender or relinquishment"

        Grey ->
            "Non-involvement and concealment "
