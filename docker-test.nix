{ pkgs ? import <nixpkgs> {} }:

pkgs.dockerTools.buildImage {
  name = "mentalscore";
  tag = "latest";
  config = {
    # Cmd = [ "nginx" "-c" nginxConf ];
    Cmd = [ "${pkgs.hello}/bin/hello" ];
    services.nginx.enable = true;
    services.nginx.virtualHosts."mentalscore.xyz" = {
      enableACME = true;
      addSSL = true;
      forceSSL = true;
      root = "/var/www/mentalscore";
    };
  };
}
