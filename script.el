;;; script --- script.el
;;; Commentary:
;;; Code:

;;  application watch
(defun watch ()
  (shell "score app watch shell")
  (insert "cd app")
  (comint-send-input)
  (insert "make watch")
  (comint-send-input)
  )

;; live server application
(defun server ()
  (shell "score app server shell")
  (insert "cd app")
  (comint-send-input)
  (insert "make server")
  (comint-send-input)
  )

;; run backend serfver
(defun backend ()
  (shell "score backend server shell")
  (insert "cd ../backend")
  (comint-send-input)
  (insert "make ghcid")
  (comint-send-input)
  )


(watch)
(server)
(backend)

;;; script.el ends here
