import * as React from "react";
import { Link, useStaticQuery, graphql } from "gatsby";
import Layout from "../components/layout";
import { StaticImage } from "gatsby-plugin-image";

const members = [
  {name: "Алла Сухих", src: "../images/face_4.jpg"},
  {name: "Илья Варваренко", src: ""},
  {name: "Даша Канаева", src: ""},
]

export default function IndexPage() {
  let isOpen = false;
  return (
    <Layout >
      <div className="w-full h-full flex flex-col justify-start items-center">
       <div className="w-full flex justify-center mx-8 bg-darkGreen text-pureGray">
          <div className="h-full w-full flex flex-col-reverse justify-center max-w-screen-lg mx-4">
            <div className="-mb-40 ">
              <StaticImage
                src="../images/face_4.jpg"
                alt="Healhty Face"
                placeholder="darkGreen"
                className="w-full lg:w-56 "
                width={500}
              />
            </div>
            <div className="flex flex-col my-8 pb-8 gap-4">
              <h3 className="text-lg"> Мы переосмыслили систему</h3>
              <h1 className="text-4xl">Сделайте ментальное здоровье своим приоритетом</h1>
              <p className="text-sm">Важность поддержания психологического здоровья часто отодвигается на второй план. Кажется, что это может подождать. Ценник на сессии со специалистом кусается, симптомы вроде не так страшны, а признаваться в ментальных проблемах до сих пор очень многим стыдно. Но психологическая помощь необходима всем. Вот поэтому мы создали MentalScore - слияние опыта психологии и технологий, которое всегда будет у Вас под рукой.</p>
            </div>
          </div>
        </div>

       <div className="w-full flex justify-center mx-8 bg-pureGray text-darkGreen">
          <div className="h-full w-full flex flex-col-reverse justify-center max-w-screen-lg mx-4">
            <div className="w-full flex flex-col my-8 pt-40 pb-8 gap-4">
              <h1 className="text-xl font-medium text-darkGray ">Новый подход</h1>
              <p className="text-sm font-medium text-darkGray">Мы в MentalScore считаем, что психологическая помощь - постоянный процесс развития более прочного и гармоничного отношения с самим собой, который выстроит прочный личностный фундамент, позволяющий пресекать или уверено преодолевать кризисы. Забота о своём ментальном здоровье - залог качественно нового уровня жизни.</p>
          </div>
        </div>
       </div>




       <div className="w-full flex justify-center mx-8 bg-lightGreen text-darkGreen">
          <div className="h-full w-full flex flex-col justify-center max-w-screen-lg mx-4 py-16 gap-4">
            <h1 className="text-4xl font-medium text-pureGray text-center ">Познакомьтесь с командой</h1>
            <p className="text-sm font-medium text-darkGray">Наша миссия - нормализовать заботу о психологическом здоровье. Сделать процесс проще, удобнее и доступнее для каждого.</p>
            <div className=" m-auto md:m-0 flex flex-col lg:flex-row justify-center gap-4">
              <div className="mx-4">
                <StaticImage
                  src="../images/maria.jpg"
                  alt="Maria"
                  placeholder="darkGreen"
                  className="rounded-lg my-4 lg:w-64"
                />
                <p className="text-bold text-4xl text-pureGray">Мария</p>
                <p className="">Основатель & Врач-Психотерапевт</p>
              </div>

              <div className="mx-4">
                <StaticImage
                  src="../images/dima_s_2.jpg"
                  alt="Dima"
                  placeholder="darkGreen"
                  className="rounded-lg my-4 lg:w-64"
                />
                <p className="text-bold text-4xl text-pureGray">Дима</p>
                <p className="">Основатель & CTO</p>
              </div>
            </div>
          </div>
        </div>

       <div className="w-full flex justify-center mx-8 bg-pureGray text-darkGreen">
          <div className="h-full w-full flex flex-col justify-center max-w-screen-lg mx-4 py-16 gap-4">
            <div className="flex flex-col my-8 pb-8 gap-4 ">
              <div className="flex flex-col pb-8">
                <h1 className="text-xl font-medium text-darkGray text-center">Наша команда</h1>
                <p className="text-sm font-medium text-darkGray">Наша миссия - нормализовать заботу о психологическом здоровье. Сделать процесс проще, удобнее и доступнее для каждого.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}



function Members(props) {
  const members = props.members;
  const listItems = members.map((member) =>
    <div className="flex text-sm">
      <p>{member.name}</p>
    </div>
  );

  return (
    <div className="flex flex-col items-center gap-4 text-sm">{listItems}</div>
  );
}
