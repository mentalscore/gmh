import * as React from "react";
import { Link, useStaticQuery, graphql } from "gatsby";
import Layout from "../components/layout";
import { StaticImage } from "gatsby-plugin-image";
import { CheckIcon } from '@heroicons/react/solid';

export default function IndexPage() {
  let isOpen = false;
  return (
    <Layout>
      <Hero />
    </Layout>
  );
}

function Hero() {
  return(
    <div className="w-full h-full bg-darkGreen px-8  hero">
      <div className="h-full w-full max-w-screen-lg flex ">
        <div className="w-full h-full flex flex-col gap-8 items-center text-pureGray my-12">
          <div className="flex flex-col gap-4">
            <div>
              <div><p className="text-sm font-bold text-left">Полное юридическое наименование:</p></div>
              <div><h1 className="text-sm">ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ "ПОЗИТИВ АУТСОРСИНГ"</h1></div>
            </div>
            <div>
              <div><p className="text-sm font-bold text-left">Сокращенное наименование:</p></div>
              <div><h1 className="text-sm">ООО "ПОЗИТИВ"</h1></div>
            </div>
            <div>
              <div><p className="text-sm font-bold text-left">Юридический адрес:</p></div>
              <div><p className="text-sm">Юридический адрес664013, Иркутская область, город Иркутск, ул. Тельмана, 54-17</p></div>
            </div>
            <div>
              <div><p className="text-sm font-bold text-left">Телефон:</p></div>
              <div><p className="text-sm">+7-963-297-88-85</p></div>
            </div>
            <div>
              <div><p className="text-sm font-bold text-left">Сайт:</p></div>
              <div><p className="text-sm">https://pozitive.biz</p></div>
            </div>
            <div>
              <div><p className="text-sm font-bold text-left">ОГРН:</p></div>
              <div><p className="text-sm">1073810002056</p></div>
              <div><p className="text-sm">от 9 апреля 2007 г.</p></div>
            </div>
            <div>
              <div><p className="text-sm font-bold text-left">ИНН:</p></div>
              <div><p className="text-sm">3810046070</p></div>
            </div>
            <div>
              <div><p className="text-sm font-bold text-left">КПП:</p></div>
              <div><p className="text-sm">381001001</p></div>
            </div>
            <div>
              <div><p className="text-sm font-bold text-left">Банк:</p></div>
              <div><p className="text-sm">ИРКУТСКИЙ ФИЛИАЛ БАНКА СОЮЗ (АО)</p></div>
            </div>
            <div>
              <div><p className="text-sm font-bold text-left">Расчетный счет:</p></div>
              <div><p className="text-sm">40702810190040001736</p></div>
            </div>
            <div>
              <div><p className="text-sm font-bold text-left">БИК:</p></div>
              <div><p className="text-sm">042520728</p></div>
            </div>
            <div>
              <div><p className="text-sm font-bold text-left">к/сч:</p></div>
              <div><p className="text-sm">30101810300000000728</p></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}


