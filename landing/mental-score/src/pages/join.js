import * as React from "react";
import { Link, useStaticQuery, graphql } from "gatsby";
import Layout from "../components/layout";
import { StaticImage } from "gatsby-plugin-image";
import { CheckIcon } from '@heroicons/react/solid';

export default function IndexPage() {
  let isOpen = false;
  return (
    <Layout>
      <Hero />
    </Layout>
  );
}

const benefits = [
  "Гибкие программы психологического оздоровления, созданные экспертами MentalScore",
  "Неограниченный доступ к мероприятиям, которые проводят наши специалисты",
  "Постоянный трекинг психического здоровья",
  "Обширная библиотека инструментов для изучения вашего психического здоровья",
]

function Hero() {
  return(
    <div className="w-full h-full bg-darkGreen px-8 flex justify-center hero">
      <div className="h-full w-full max-w-screen-lg flex justify-center">
        <div className="w-full h-full flex flex-col gap-8 items-center text-pureGray mt-12">
          <div className="flex flex-col gap-4">
            <div><h1 className="text-2xl text-center">Воспользуйтесь расширенными возможностями</h1></div>
            <div><p className="text-sm text-center">Сделайте душевное благополучие неотъемлемой частью своей жизни</p></div>
          </div>
          <div className="mb-4 w-full flex flex-col gap-4 pb-8 border-b border-lightGreen">
            <div className="w-full ">
              <a href="/app/#/register">
                <button className="w-full border border-pureGray text-pureGray font-bold py-4 px-4 rounded-lg lg:py-8 lg:px-16 lg:rounded-lg">
                  500 ₽ в месяц
                </button>
              </a>
            </div>
            <div className="w-full flex flex-col gap-2">
              <a href="/app/#/register">
                <button className="w-full bg-green text-darkGray font-bold py-4 px-4 rounded-lg lg:py-8 lg:px-16 lg:rounded-lg">
                  3000 ₽ в год
                </button>
              </a>
              <p className="text-sm text-center">6 месяцев выгода</p>
            </div>
          </div>

          <Benefits benefitList={benefits} />

          <div className="my-8">
            <p classNmae="text-xs ">Отмена подписки в любое время</p>
          </div>
          <div></div>
        </div>
      </div>
    </div>
  )
}


function Benefits(props) {
  const benifits = props.benifits;
  const listItems = benefits.map((benefit) =>
    <div className="flex gap-2 text-sm">
      <div className="h-6 w-6"><CheckIcon className="h-6 w-6"/></div>
      <p>{benefit}</p>
    </div>
  );
  return (
    <div className="flex flex-col gap-4 text-sm">{listItems}</div>
  );
}
