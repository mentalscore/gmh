import * as React from "react";
import { Helmet } from 'react-helmet';
import { useState } from "react";
import { Link, useStaticQuery, graphql } from "gatsby";
import { Seo } from './seo.js';
import { MenuAlt2Icon, XIcon } from '@heroicons/react/solid';


export default function Layout({children}) {
  const data = useStaticQuery(graphql`
    query GetUserAgreementURL {
      allFile(filter: {name: {eq: "user_agreement"}, ext: {eq: ".pdf"}}) {
        edges {
          node {
            publicURL
          }
        }
      }
    }
  `);

  const [isOpen, setSelectValue] = useState(false);

  return (
    <>
      <Seo />
      <div className="w-full min-h-screen grid grid-rows-[50px_1fr_auto]">
        <Header isOpen={isOpen} fn={(() => setSelectValue(!isOpen))}/>
        <Menu isOpen={isOpen} fn={(() => setSelectValue(false))}/>
        <main className="w-full "> {children} </main>
        <Footer userAgreementUrl={data?.allFile?.edges[0]?.node?.publicURL} />
      </div>
    </>
  );
}

export function Header(props) {
  const bg = props.isOpen? "bg-black" : "bg-darkGreen"
  const icon = props.isOpen?  <XIcon className="h-6 w-6" onClick={props.fn}/>  : <MenuAlt2Icon className="h-6 w-6" onClick={props.fn}/>

  return(
    <header className={`w-full ${bg} text-pureGray flex justify-center`}>
        <div className="w-full max-w-screen-lg mx-8 flex justify-between items-center">
          <h1 className="text-2xl"><Link to="/">MentalScore</Link></h1>
          {icon}
        </div>
      </header>
  );
}
  
export function Footer(props) {
  return(
      <footer className="w-full bg-darkGray text-moreGray flex justify-center">
        <div className="w-full max-w-screen-lg flex mx-8">
          <div className="w-full my-16 flex flex-col gap-4">
            <div>
              <h1 className="text-xl text-pureGray">MentalScore</h1>
            </div>
            <div className="text-sm">
              <p>Наша служба поддержки готова ответить на Ваши вопросы:</p>
              <p className="text-pureGray"><a href="mailto:support@mentalscore.xyz">support@mentalscore.xyz</a></p>
            </div>
            <div className="text-base">
              <p className="text">Мы в соцсетях</p>
              <ul className="text-pureGray">
                <li><a href="https://t.me/+Ri918G4EqZkyNzlk">Telegram</a></li>
                <li><a href="vk.com">VK</a></li>
              </ul>
            </div>
            <div className="text-base">
              <p className="text ">Правовые документы</p>
              <ul className="text-pureGray">
                <li><Link to={props.userAgreementUrl}>Пользовательское соглашение</Link></li>
                <li className="w-full flex justify-between">
                  <div>
                    <Link to="https://pozitive.biz">©2022 ООО «ПОЗИТИВ»</Link> | <Link to="/rekviziti">Реквизиты</Link>
                  </div>
                  <span>18+</span></li>
              </ul>
            </div>
          </div>
        </div>
      </footer>
  );
}

function Menu(props) {
  if (!props.isOpen) { return (<></>)}
  return(
    <div className="bg-black w-screen h-screen text-pureGray">
      <ul className="text-xl flex flex-col justify-center items-center font-medium mt-16 gap-8">
        <li onClick={props.fn}><Link to="/">Главная</Link></li>
        <li onClick={props.fn}><Link to="/how-it-works/">Как это работает</Link></li>        
        <li onClick={props.fn}><Link to="/join">Просоединиться</Link></li>
        <li onClick={props.fn}><Link to="/about">О Нас</Link></li>
        <li onClick={props.fn}><Link to="/blog">Блог</Link></li>
      </ul>
    </div>
  )
}
