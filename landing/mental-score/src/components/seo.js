import * as React from 'react';
import { Helmet } from 'react-helmet';
import { useSiteMetadata } from "../hooks/use-site-metadata";


export function Seo(props) {
  const { title: defaultTitle, description: defaultDescription  } = useSiteMetadata();

  const seo = {
    title: props.title || defaultTitle,
    description: props.description || defaultDescription,
  };

  return (
    <>
    <Helmet>
      <title>{seo.title}</title>
      <meta name="description" content={seo.description} />
    </Helmet>
    {props.children}
    </>
  );
}
  
