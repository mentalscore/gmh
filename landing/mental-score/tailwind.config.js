/** @type {import('tailwindcss').Config} */ 
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx,mdx}",
    "./src/posts/**/*.{js,jsx,ts,tsx,mdx}",
    "./src/pages/**/*.{js,jsx,ts,tsx,mdx}",
    "./src/pages/**/**/*.{js,jsx,ts,tsx,mdx}",
    "./src/components/**/*.{js,jsx,ts,tsx,mdx}",
  ],
  theme: {
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      darkGray: "#181818", 
      darkGreen: "#142e29", 
      darkBlue: "#21354d", 
      cream: "#f7e8cb",
      lightGray: "#d4dbc0",
      lightBlue: "#e7eff7", 
      lightGreen: "#c5d3a8", 
      pureGray: "#eef2f8", 
      moreGray: "#707070",
      green: "#708840",
      lighterGreen: "#264131",
      black: "#000000",
    },
    extend: {
      typography: (theme) => ({
        DEFAULT: {
          css: {
            'h1':{
              color: theme('colors.emerald.700'),
              fontWeight: 'bold',
              fontFamily: theme('fontFamily.serif')
            },
            'h5, h6': {
              color: theme('colors.emerald.700'),
              fontWeight: 'bold',
              fontFamily: theme('fontFamily.serif')
            },
          },
        },
      }),
    },
  },
  plugins: [
    require('@tailwindcss/typography'),
  ],
}
