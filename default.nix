with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "gmh";
  buildInputs = [
    elmPackages.elm
    elmPackages.elm-format
    nodePackages.uglify-js
    git
    direnv
    coreutils
  ];
}
