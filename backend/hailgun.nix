{ mkDerivation, aeson, attoparsec, base, bytestring, email-validate
, exceptions, filepath, http-client, http-client-tls, http-types
, lib, tagsoup, text, time, transformers
}:
mkDerivation {
  pname = "hailgun";
  version = "0.5.1";
  src = ../hailgun;
  libraryHaskellDepends = [
    aeson attoparsec base bytestring email-validate exceptions filepath
    http-client http-client-tls http-types tagsoup text time
    transformers
  ];
  homepage = "https://bitbucket.org/echo_rm/hailgun";
  description = "Mailgun REST api interface for Haskell";
  license = lib.licenses.mit;
}
