{ mkDerivation, aeson, base, bytestring, conferer, hailgun
, http-client, http-client-tls, lib, mime-mail, mtl, password
, postgresql-simple, random, resource-pool, selda, selda-json
, selda-postgresql, servant-client, servant-server, stm, text, time
, uuid, wai-cors, warp
}:
mkDerivation {
  pname = "backend";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  libraryHaskellDepends = [
    aeson base password selda selda-json selda-postgresql text uuid
  ];
  executableHaskellDepends = [
    aeson base bytestring conferer hailgun http-client http-client-tls
    mime-mail mtl password postgresql-simple random resource-pool selda
    selda-postgresql servant-client servant-server stm text time uuid
    wai-cors warp
  ];
  doHaddock = false;
  description = "GMH backend";
  license = lib.licenses.gpl3Only;
}
