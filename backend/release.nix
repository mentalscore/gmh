{ compiler ? "ghc8107" }:

let
  config = {
    packageOverrides = pkgs: rec {
      haskellPackages = pkgs.haskell.packages."${compiler}".override {
        overrides = self: super: {
          hailgun =
              self.callPackage ./hailgun.nix { };

          project =
            self.callPackage ./project.nix { };

          project-minimal =
            pkgs.haskell.lib.overrideCabal
              ( pkgs.haskell.lib.justStaticExecutables
                  ( self.callPackage ./project.nix { } )
              ) ( oldDerivation: { } );
        };
      };
    };
  };

  pkgs = import <nixpkgs> { inherit config; };

in
  { project = pkgs.haskellPackages.project;
    project-minimal = pkgs.haskellPackages.project-minimal;
    hailgun = pkgs.haskellPackages.hailgun; 
  }
