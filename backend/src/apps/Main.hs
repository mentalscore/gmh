module Main where

import qualified Conferer as Conf
import Control.Monad
import Control.Monad.Reader
import Data.Aeson
import Data.Aeson.Types
import Data.ByteString (ByteString, singleton)
import Data.Char
import Data.Int
import qualified Data.List as List
import Data.Maybe (fromMaybe, listToMaybe, maybeToList)
import Data.Password.Bcrypt
import Data.Pool
import qualified Data.Text as T
import Data.Text.Encoding (encodeUtf8)
import qualified Data.Text.Lazy as Lazy
import Data.Time (nominalDiffTimeToSeconds)
import Data.Time.Clock.POSIX (getCurrentTime, utcTimeToPOSIXSeconds)
import qualified Data.UUID as UUID
import Data.UUID.V4 as V4
import Database.Selda
import Database.Selda.Backend
import Database.Selda.PostgreSQL
import qualified Debug.Trace as Debug
import GMH.Data as D
import qualified Mail.Hailgun as HG
import Network.HTTP.Client (managerModifyRequest, newManager, requestHeaders)
import qualified Network.HTTP.Client.TLS as NT --(tlsManagerSettings)
import Network.Wai.Handler.Warp (run)
import Network.Wai.Middleware.Cors
import Servant
import Servant.Client (BaseUrl (..), ClientError, ClientM, Scheme (..), client, mkClientEnv, parseBaseUrl, runClientM, showBaseUrl)
import System.Console.GetOpt
import System.Environment
import System.Exit
import System.IO
import System.IO.Unsafe
import System.Random
import Text.Printf
import qualified Text.Show

-- Data
type AppM =
  ReaderT Env Handler

-- type App a = ReaderT Env (Handler a)

data Env = Env
  { getPool :: Pool (SeldaConnection PG),
    getGen :: StdGen,
    getAppConfig :: AppConfig
  }

data SmtpCredentials = SmtpCredentials
  { smtpServer :: Text,
    smtpUsername :: Text,
    smtpPassword :: Text
  }
  deriving (Show)

type API =
  "static" :> Raw
    :<|> "api" :> "v1"
      :> ( "account" :> ("reset-password" :> ReqBody '[JSON] EmailInfo :> Post '[JSON] (Response Bool))
             :<|>
           "account" :> ("update-password" :> ReqBody '[JSON] UpdatePasswordInfo :> Post '[JSON] (Response Bool))
             :<|> "accounts" :> Get '[JSON] [Account]
             :<|> "accounts" :> ReqBody '[JSON] AccountInfo :> Post '[JSON] (Response Account)
             :<|> "guests" :> ReqBody '[JSON] LanguageInfo :> Post '[JSON] (Account)
             :<|> "signin" :> ReqBody '[JSON] SigninInfo :> Post '[JSON] (Response Account)
             :<|> "confirmations" :> ReqBody '[JSON] Confirmation :> Post '[JSON] (Response AccountConfirmation)
             :<|> "acceptance-disclaimer" :> ReqBody '[JSON] UUIDInfo :> Post '[JSON] (Response AcceptanceDisclaimerInfo)
             :<|> "tests" :> "lushers" :> ReqBody '[JSON] LusherInfo :> Post '[JSON] (Response Bool)
             :<|> "tests" :> "phq9" :> ReqBody '[JSON] PHQ9Info :> Post '[JSON] (Response Bool)
             :<|> "tests" :> "phq15" :> ReqBody '[JSON] PHQ15Info :> Post '[JSON] (Response Bool)
             :<|> "tests" :> "gad7" :> ReqBody '[JSON] GAD7Info :> Post '[JSON] (Response Bool)
             :<|> "tests" :> "subjective5" :> ReqBody '[JSON] Subjective5Info :> Post '[JSON] (Response Bool)
             :<|> "accounts" :> Capture "uuid" String :> "tests" :> "lushers" :> Get '[JSON] [Lusher]
             :<|> "surveys" :> "introductions" :> ReqBody '[JSON] IntroductionInfo :> Post '[JSON] (Response Bool)
             :<|> "accounts" :> Capture "uuid" String :> "surveys" :> "introductions" :> Get '[JSON] [Introduction]
             :<|> "accounts" :> Capture "uuid" String :> "scores" :> Get '[JSON] [Score]
             :<|> "accounts" :> Capture "uuid" String :> "activities" :> Get '[JSON] [Activity]
             :<|> "activities" :> ReqBody '[JSON] ActivityInfo :> Post '[JSON] (Response (ID Activity))
             :<|> "activities" :> Capture "activityId" Int :> DeleteNoContent
             :<|> "accounts" :> Capture "uuid" String :> "settings" :> Get '[JSON] [Settings]
             :<|> "settings" :> ReqBody '[JSON] SettingsInfo :> Post '[JSON] (Response (ID Settings))
             :<|> "settings" :> Capture "id" Int :> ReqBody '[JSON] SettingsInfo :> Put '[JSON] (Response Bool)
             :<|> "report" :> ReqBody '[JSON] MinimalInfo :> Post '[JSON] (Response (ID Report))
             :<|> "program" :> ReqBody '[JSON] MinimalInfo :> Post '[JSON] (Response (ID Program))
             :<|> "plans" :> ReqBody '[JSON] PlanInfo :> Post '[JSON] (Response (Plan))
             :<|> "accounts" :> Capture "uuid" String :> "plans" :> Get '[JSON] [Plan]
             :<|> "yk-payments" :> ReqBody '[JSON] PlanInfo :> Post '[JSON] (Response YKPayment)
             :<|> "yk-payments" :> Capture "uuid" String :> Get '[JSON] (Response YKPayment)
             :<|> "plans" :> Capture "uuid" String :> "yk-payments" :> Get '[JSON] [YKPayment]
         )

server :: ServerT API AppM
server =
  serveDirectoryWebApp "static"
    :<|> postResetPassword
    :<|> postUpdatePassword
    :<|> getAccounts
    :<|> postAccounts
    :<|> postGuests
    :<|> postSignin
    :<|> postConfirmations
    :<|> postAcceptanceDisclaimer
    :<|> postLusher
    :<|> postPHQ9
    :<|> postPHQ15
    :<|> postGAD7
    :<|> postSubject5
    :<|> getLushers
    :<|> postIntroduction
    :<|> getIntroductions
    :<|> getScores
    :<|> getActivities
    :<|> postActivity
    :<|> deleteActivity
    :<|> getSettings
    :<|> postSettings
    :<|> putSettings
    :<|> postReport
    :<|> postProgram
    :<|> postPlans
    :<|> getPlans
    :<|> postYKPayments
    :<|> getYKPayment
    :<|> getYKPayments

instance ToJSON (Response (ID Report)) where
  toJSON (RespSuccess int) =
    object ["id" .= int]
  toJSON (RespFailure error) =
    object ["error" .= error]

instance ToJSON (Response (ID Program)) where
  toJSON (RespSuccess int) =
    object ["id" .= int]
  toJSON (RespFailure error) =
    object ["error" .= error]

data MinimalInfo = MinimalInfo
  {_MinimalUUID :: UUID}
  deriving (Generic, Show)

instance FromJSON MinimalInfo where
  parseJSON (Object v) =
    MinimalInfo
      <$> v .: "uuid"

data PlanInfo = PlanInfo
  {_YKPaymentUUID :: UUID
  ,_YKPaymentType :: PlanType
  }
  deriving (Generic, Show)

instance FromJSON PlanInfo where
  parseJSON (Object v) =
    PlanInfo
      <$> v .: "uuid"
      <*> v .: "type"

data EmailInfo = EmailInfo
  {_emailInfo :: Text}
  deriving (Generic, Show)

instance FromJSON EmailInfo where
  parseJSON (Object v) =
    EmailInfo
      <$> v .: "email"

data UpdatePasswordInfo = UpdatePasswordInfo
  { _uuid :: UUID,
    _password :: Text
  }
  deriving (Generic, Show)

instance FromJSON UpdatePasswordInfo where
  parseJSON (Object v) =
    UpdatePasswordInfo
      <$> v .: "uuid"
      <*> v .: "password"

data SettingsInfo = SettingsInfo
  { _settingsUUID :: UUID,
    _settingsName :: Text,
    _settingsPhone :: Text
  }
  deriving (Generic, Show)

instance FromJSON SettingsInfo where
  parseJSON (Object v) =
    SettingsInfo
      <$> v .: "uuid"
      <*> v .: "name"
      <*> v .: "phone"

instance ToJSON (Response (ID Settings)) where
  toJSON (RespSuccess int) =
    object ["id" .= int]
  toJSON (RespFailure error) =
    object ["error" .= error]

data ActivityInfo = ActivityInfo
  { _activityUUID :: UUID,
    _activityType :: ActivityType
  }
  deriving (Generic, Show)

instance FromJSON ActivityInfo where
  parseJSON (Object v) =
    ActivityInfo
      <$> v .: "uuid"
      <*> v .: "activity"

data LanguageInfo = LanguageInfo
  { language :: Language
  }

instance FromJSON LanguageInfo where
  parseJSON (Object v) =
    LanguageInfo
      <$> v .: "language"

instance FromJSON Language where
  parseJSON (String t) = (fromString $ T.unpack t)
    where
      fromString :: String -> Parser Language
      fromString "english" = pure English
      fromString "russian" = pure Russian
      fromString "dutch" = pure Dutch
      fromString _ = pure English

data LusherInfo = LusherInfo
  { lusherUUID :: UUID,
    fisrt :: Text,
    second :: Text
  }
  deriving (Generic, Show)

instance FromJSON LusherInfo where
  parseJSON (Object v) =
    LusherInfo
      <$> v .: "uuid"
      <*> v .: "first"
      <*> v .: "second"

data PHQ9Info = PHQ9Info
  { phq9UUID :: UUID,
    phq9a :: Int,
    phq9b :: Int,
    phq9c :: Int,
    phq9d :: Int,
    phq9e :: Int,
    phq9f :: Int,
    phq9g :: Int,
    phq9h :: Int,
    phq9i :: Int
  }
  deriving (Generic, Show)

instance FromJSON PHQ9Info where
  parseJSON (Object v) =
    PHQ9Info
      <$> v .: "uuid"
      <*> v .: "0"
      <*> v .: "1"
      <*> v .: "2"
      <*> v .: "3"
      <*> v .: "4"
      <*> v .: "5"
      <*> v .: "6"
      <*> v .: "7"
      <*> v .: "8"

data PHQ15Info = PHQ15Info
  { phq15UUID :: UUID,
    phq15a :: Int,
    phq15b :: Int,
    phq15c :: Int,
    phq15d :: Int,
    phq15e :: Int,
    phq15f :: Int,
    phq15g :: Int,
    phq15h :: Int,
    phq15i :: Int,
    phq15j :: Int,
    phq15k :: Int,
    phq15l :: Int,
    phq15m :: Int,
    phq15n :: Int,
    phq15o :: Int
  }
  deriving (Generic, Show)

instance FromJSON PHQ15Info where
  parseJSON (Object v) =
    PHQ15Info
      <$> v .: "uuid"
      <*> v .: "0"
      <*> v .: "1"
      <*> v .: "2"
      <*> v .: "3"
      <*> v .: "4"
      <*> v .: "5"
      <*> v .: "6"
      <*> v .: "7"
      <*> v .: "8"
      <*> v .: "9"
      <*> v .: "10"
      <*> v .: "11"
      <*> v .: "12"
      <*> v .: "13"
      <*> v .: "14"

data GAD7Info = GAD7Info
  { gad7UUID :: UUID,
    gad7a :: Int,
    gad7b :: Int,
    gad7c :: Int,
    gad7d :: Int,
    gad7e :: Int,
    gad7f :: Int,
    gad7g :: Int
  }
  deriving (Generic, Show)

instance FromJSON GAD7Info where
  parseJSON (Object v) =
    GAD7Info
      <$> v .: "uuid"
      <*> v .: "0"
      <*> v .: "1"
      <*> v .: "2"
      <*> v .: "3"
      <*> v .: "4"
      <*> v .: "5"
      <*> v .: "6"

data Subjective5Info = Subjective5Info
  { subUUID :: UUID,
    subHome :: Int,
    subSocial :: Int,
    subWork :: Int,
    subEating :: Int,
    subPhysical :: Int
  }
  deriving (Generic, Show)

instance FromJSON Subjective5Info where
  parseJSON (Object v) =
    Subjective5Info
      <$> v .: "uuid"
      <*> v .: "0"
      <*> v .: "1"
      <*> v .: "2"
      <*> v .: "3"
      <*> v .: "4"

data IntroductionInfo = IntroductionInfo
  { intAccountUUID :: UUID,
    intNeedHelp :: Text,
    intTreatment :: Text,
    intAnxiety :: Text,
    intGender :: Text,
    intAge :: Text,
    intName :: Text
  }
  deriving (Generic, Show)

instance FromJSON IntroductionInfo where
  parseJSON (Object v) =
    IntroductionInfo
      <$> v .: "uuid"
      <*> v .: "needHelp"
      <*> v .: "treatment"
      <*> v .: "anxiety"
      <*> v .: "gender"
      <*> v .: "age"
      <*> v .: "name"

instance ToJSON (Response (ID Activity)) where
  toJSON (RespSuccess int) =
    object ["id" .= int]
  toJSON (RespFailure error) =
    object ["error" .= error]

instance ToJSON (Response Bool) where
  toJSON (RespSuccess bool) =
    object ["saved" .= bool]
  toJSON (RespFailure error) =
    object ["error" .= error]

data UUIDInfo = UUIDInfo
  {infoUUID :: UUID}
  deriving (Generic, Show)

instance FromJSON UUIDInfo where
  parseJSON (Object v) =
    UUIDInfo
      <$> v .: "uuid"

data AcceptanceDisclaimerInfo
  = AcceptanceDisclaimerInfo UUID Bool
  deriving (Generic, Show)

instance ToJSON (Response AcceptanceDisclaimerInfo) where
  toJSON (RespSuccess (AcceptanceDisclaimerInfo uuid bool)) =
    object ["uuid" .= uuid, "disclaimer" .= bool]
  toJSON (RespFailure error) =
    object ["error" .= error]

data AccountConfirmation
  = Confirm Text
  deriving (Generic, Show)

data Response a
  = RespSuccess a
  | RespFailure Text
  deriving (Generic, Show)

instance ToJSON (Response Text)

instance ToJSON (Response AccountConfirmation) where
  toJSON (RespSuccess (Confirm txt)) =
    object ["messags" .= show (T.unpack txt ++ " email is confirmed")]
  toJSON (RespFailure error) =
    object ["error" .= error]

instance ToJSON (Response Account) where
  toJSON (RespSuccess account) =
    toJSON account
  toJSON (RespFailure error) =
    object ["error" .= error]

data AccountInfo = AccountInfo
  { accountEmail :: Text,
    accountPassword :: Text,
    accountLanguage :: Language
  }
  deriving (Generic, Show)

instance ToJSON AccountInfo

instance FromJSON AccountInfo where
  parseJSON (Object v) =
    AccountInfo
      <$> v .: "email"
      <*> v .: "password"
      <*> v .: "language"

data SigninInfo = SigninInfo
  { signinEmail :: Text,
    signinPassword :: Text
  }
  deriving (Generic)

instance FromJSON SigninInfo where
  parseJSON (Object v) =
    SigninInfo
      <$> v .: "email"
      <*> v .: "password"

{- For now it's a user UUID -}
data Signin = Signin
  {sigininToken :: Text}
  deriving (Generic)

instance ToJSON SigninInfo

instance ToJSON (Response Signin) where
  toJSON (RespSuccess (Signin sigininToken)) =
    object ["id" .= sigininToken]
  toJSON (RespFailure error) =
    object ["error" .= error]

instance ToJSON (Response Plan) where
  toJSON (RespSuccess plan) =
    toJSON plan
  toJSON (RespFailure error) =
    object ["error" .= error]

instance ToJSON (Response YKPayment) where
  toJSON (RespSuccess ykPayment) =
    toJSON ykPayment
  toJSON (RespFailure error) =
    object ["error" .= error]

type Message = Text

api :: Proxy API
api = Proxy

runSeldaAppT :: Pool (SeldaConnection PG) -> SeldaT PG AppM a -> AppM a
runSeldaAppT conns seldaT = do
  withResource conns $ runSeldaT seldaT

runSeldaAppT' :: SeldaT PG AppM a -> AppM a
runSeldaAppT' seldaT = do
  env <- ask
  withResource (getPool env) $ runSeldaT seldaT

-- | GET
getAccounts :: AppM [Account]
getAccounts = do
  env <- ask
  runSeldaAppT (getPool env) getAccountsDB

getLushers :: String -> AppM [Lusher]
getLushers uuid = getLushers' (UUID.fromString uuid)

getLushers' :: Maybe UUID -> AppM [Lusher]
getLushers' Nothing = pure []
getLushers' (Just uuid) = do
  env <- ask
  runSeldaAppT (getPool env) (getLushersDB uuid)

getIntroductions :: String -> AppM [Introduction]
getIntroductions uuid = getIntroductions' (UUID.fromString uuid)

getIntroductions' :: Maybe UUID -> AppM [Introduction]
getIntroductions' Nothing = pure []
getIntroductions' (Just uuid) = do
  env <- ask
  runSeldaAppT (getPool env) (getIntroductionsDB uuid)

getScores :: String -> AppM [Score]
getScores uuid = getScores' (UUID.fromString uuid)

getScores' :: Maybe UUID -> AppM [Score]
getScores' Nothing = pure []
getScores' (Just uuid) = do
  env <- ask
  runSeldaAppT (getPool env) (getScoresDB uuid)

getActivities :: String -> AppM [Activity]
getActivities uuid = getActivities' (UUID.fromString uuid)

getActivities' :: Maybe UUID -> AppM [Activity]
getActivities' Nothing = pure []
getActivities' (Just uuid) = do
  env <- ask
  now <- liftIO $ getCurrentTime
  runSeldaAppT (getPool env) (getActivitiesDB uuid now)

getSettings :: String -> AppM [Settings]
getSettings uuid = getSettings' (UUID.fromString uuid)

getSettings' :: Maybe UUID -> AppM [Settings]
getSettings' Nothing = pure []
getSettings' (Just uuid) = do
  env <- ask
  runSeldaAppT (getPool env) (getSettingsDB uuid)

-- | POST
postAccounts :: AccountInfo -> AppM (Response Account)
postAccounts ai = do
  uuid <- liftIO $ createUuid
  env <- ask
  passwordHash <- liftIO $ hashPassword $ mkPassword (accountPassword ai)
  resp <- runSeldaAppT (getPool env) (insertAccount uuid passwordHash ai)
  _ <- maybeCreateConfirmation ai resp
  return resp

postConfirmations :: Confirmation -> AppM (Response AccountConfirmation)
postConfirmations conf = do
  env <- ask
  runSeldaAppT (getPool env) (confirmEmail conf)

maybeCreateConfirmation :: AccountInfo -> Response Account -> AppM ()
maybeCreateConfirmation _ (RespSuccess (Account accountUUID (Just email) _ _ _ _)) =
  generateConfirmation accountUUID email
maybeCreateConfirmation ai _ =
  liftIO $ print ("Can't create account with " <> (show (accountEmail ai)))

generateConfirmation :: UUID -> Text -> AppM ()
generateConfirmation accountUUID email = do
  uuid <- liftIO $ createUuid
  env <- ask
  resp <- runSeldaAppT (getPool env) (insertConfirmation uuid accountUUID)
  -- sendVerifyEmail resp email
  pure ()

postResetPassword :: EmailInfo -> AppM (Response Bool)
postResetPassword (EmailInfo email) = do
  env <- ask
  uuid <- liftIO $ createUuid
  isCreated <- runSeldaAppT (getPool env) (resetPassword uuid email)
  _ <- liftIO $ print isCreated
  when (isCreated) $ sendEmailResetPassword uuid email
  pure (RespSuccess True)

resetPassword :: UUID -> Text -> SeldaT PG AppM (Bool)
resetPassword uuid email = do
  as <- findAccountByEmail email
  u <- liftIO $ getCurrentTime
  tryInsert D.resetPasswords $
    map (D.ResetPassword uuid (nanosSinceEpoch u) . accountUUID) $ as

postUpdatePassword :: UpdatePasswordInfo -> AppM (Response Bool)
postUpdatePassword (UpdatePasswordInfo uuid password) = do
  env <- ask
  int <- runSeldaAppT (getPool env) (updatePassword uuid password)
  pure $ RespSuccess (int > 0)

updatePassword :: UUID -> Text -> SeldaT PG AppM (Int)
updatePassword uuid pwd = do
  rp <- findResetPassword uuid
  case map D.resetPasswordAccountUUID rp of
    [] ->
      pure 0
    (aUUID : _) -> do
      passwordHash <- liftIO $ hashPassword $ mkPassword pwd
      int <- updateAccountPassword aUUID passwordHash
      if int > 0
        then deleteResetPasswords aUUID
        else pure 0

deleteResetPasswords :: UUID -> SeldaT PG AppM (Int)
deleteResetPasswords uuid =
  deleteFrom
    resetPasswords
    (\rp -> rp ! #resetPasswordAccountUUID .== literal uuid)

findResetPassword :: UUID -> SeldaT PG AppM ([ResetPassword])
findResetPassword uuid = query $ do
  rp <- select (resetPasswords)
  restrict (rp ! #resetPasswordUUID .== literal (uuid))
  return rp

postAcceptanceDisclaimer :: UUIDInfo -> AppM (Response AcceptanceDisclaimerInfo)
postAcceptanceDisclaimer (UUIDInfo uuid) = do
  env <- ask
  runSeldaAppT (getPool env) (acceptDisclaimer uuid)

acceptDisclaimer :: UUID -> SeldaT PG AppM (Response AcceptanceDisclaimerInfo)
acceptDisclaimer uuid = do
  result <- acceptAccountDisclaimer uuid
  pure (toResponse uuid result)
  where
    toResponse :: UUID -> Int -> Response AcceptanceDisclaimerInfo
    toResponse uuid 1 = RespSuccess (AcceptanceDisclaimerInfo uuid True)
    toResponse _ _ = RespFailure "No account with this uuid"

postLusher :: LusherInfo -> AppM (Response Bool)
postLusher (LusherInfo uuid first second) = do
  env <- ask
  u <- liftIO $ getCurrentTime
  runSeldaAppT (getPool env) (createLusher uuid first second (nanosSinceEpoch u))

postPHQ9 :: PHQ9Info -> AppM (Response Bool)
postPHQ9 phq9Info = do
  env <- ask
  u <- liftIO $ getCurrentTime
  x <- runSeldaAppT (getPool env) (createPHQ9 phq9Info (nanosSinceEpoch u))
  (runSeldaAppT (getPool env) (createScore x (phq9UUID phq9Info)))

postPHQ15 :: PHQ15Info -> AppM (Response Bool)
postPHQ15 phq15Info = do
  env <- ask
  u <- liftIO $ getCurrentTime
  x <- runSeldaAppT (getPool env) (createPHQ15 phq15Info (nanosSinceEpoch u))
  (runSeldaAppT (getPool env) (createScore x (phq15UUID phq15Info)))

postGAD7 :: GAD7Info -> AppM (Response Bool)
postGAD7 gad7Info = do
  env <- ask
  u <- liftIO $ getCurrentTime
  x <- runSeldaAppT (getPool env) (createGAD7 gad7Info (nanosSinceEpoch u))
  (runSeldaAppT (getPool env) (createScore x (gad7UUID gad7Info)))

postSubject5 :: Subjective5Info -> AppM (Response Bool)
postSubject5 subject5Info = do
  env <- ask
  u <- liftIO $ getCurrentTime
  x <- runSeldaAppT (getPool env) (createSubject5 subject5Info (nanosSinceEpoch u))
  (runSeldaAppT (getPool env) (createScore x (subUUID subject5Info)))

postActivity :: ActivityInfo -> AppM (Response (ID Activity))
postActivity (ActivityInfo uuid activity) = do
  env <- ask
  u <- liftIO $ getCurrentTime
  runSeldaAppT (getPool env) (createActivity uuid activity (nanosSinceEpoch u))

deleteActivity :: Int -> AppM (NoContent)
deleteActivity int = do
  env <- ask
  int <- runSeldaAppT (getPool env) (deleteActivityDB id)
  if int == 0
    then throwError $ err404 {errBody = ""}
    else pure (NoContent)
  where
    id :: Int64
    id = fromIntegral int

deleteActivityDB :: Int64 -> SeldaT PG AppM (Int)
deleteActivityDB id =
  deleteFrom activities (\a -> a ! #activityId .== literal (toId id))

postSettings :: SettingsInfo -> AppM (Response (ID Settings))
postSettings (SettingsInfo uuid name phone) = do
  env <- ask
  u <- liftIO $ getCurrentTime
  runSeldaAppT (getPool env) (createSettings uuid name phone (nanosSinceEpoch u))

putSettings :: Int -> SettingsInfo -> AppM (Response Bool)
putSettings id (SettingsInfo uuid name phone) = do
  env <- ask
  u <- liftIO $ getCurrentTime
  runSeldaAppT (getPool env) (updateSettings id uuid name phone (nanosSinceEpoch u))

postReport :: MinimalInfo -> AppM (Response (ID Report))
postReport (MinimalInfo uuid) = do
  env <- ask
  u <- liftIO $ getCurrentTime
  runSeldaAppT (getPool env) (createReport uuid (nanosSinceEpoch u))

postProgram :: MinimalInfo -> AppM (Response (ID Program))
postProgram (MinimalInfo uuid) = do
  env <- ask
  u <- liftIO $ getCurrentTime
  runSeldaAppT (getPool env) (createProgram uuid (nanosSinceEpoch u))

postPlans :: PlanInfo -> AppM (Response Plan)
postPlans (PlanInfo uuid planType_) = do
  planUUID <- liftIO $ createUuid
  env <- ask
  u <- liftIO $ getCurrentTime
  runSeldaAppT (getPool env) (insertPlan uuid planUUID planType_ (nanosSinceEpoch u))

getPlans :: String -> AppM [Plan]
getPlans uuid = getPlans' (UUID.fromString uuid)

getPlans' :: Maybe UUID -> AppM [Plan]
getPlans' Nothing = pure []
getPlans' (Just uuid) = do
  env <- ask
  runSeldaAppT (getPool env) (getPlansDB uuid)

postYKPayments :: PlanInfo -> AppM (Response YKPayment)
postYKPayments (PlanInfo uuid planType_) = do
  ykPaymentUUID <- liftIO $ createUuid
  env <- ask
  u <- liftIO $ getCurrentTime
  yooKassaResp <- liftIO $ yooKassaClientM (yooKassaCreatePayment (appYooKassaId (getAppConfig env)) (appYooKassaKey (getAppConfig env)) (yooKassaPaymentInfo ykPaymentUUID))
  case yooKassaResp of
    Left err -> return $ RespFailure (T.pack (Text.Show.show err))
    Right yooKassaRespSuccess -> do
      ykp <- runSeldaAppT (getPool env) (insertYKPayment ykPaymentUUID uuid (nanosSinceEpoch u) yooKassaRespSuccess)

      return (ykp)
  where
    yooKassaPaymentInfo indempotenceKey = YooKassaPaymentInfo indempotenceKey price "RUB" "redirect" "https://mentalscore.xyz/app/#/subscription" desc
    price = planTypeToPrice planType_
    desc = planTypeToDesc planType_

planTypeToPrice :: PlanType  -> Text
planTypeToPrice planType_ =
  case planType_ of
    MonthlyRecurring -> "500.00"
    Annual -> "3000.00"

planTypeToDesc :: PlanType  -> Text
planTypeToDesc planType_ =
  case planType_ of
    MonthlyRecurring -> "Ежемесячная подписка на MentalScore"
    Annual -> "Годовая подписка на MentalScore"


getYKPayment :: String -> AppM (Response YKPayment)
getYKPayment uuid = do
  case UUID.fromString uuid of
    Just x -> getYKPaymentUUID x
    Nothing -> return (RespFailure "UUID is not valid")

getYKPaymentUUID :: UUID -> AppM (Response YKPayment)
getYKPaymentUUID uuid = do
  env <- ask
  mp <- runSeldaAppT' (getYKPayment' uuid)
  case mp of
    Just ykp -> do
      yooKassaResp <- liftIO $ yooKassaClientM (yooKassaPayments (appYooKassaId (getAppConfig env)) (appYooKassaKey (getAppConfig env)) (ykPaymentResponseId ykp))

      case yooKassaResp of
        Left err -> return (RespFailure (T.pack $ Text.Show.show err))
        Right yooKassPayment -> do
          _ <- liftIO $ print yooKassPayment
          _ <- runSeldaAppT' (updateYKPayment' uuid yooKassPayment)
          updatedPayment <- runSeldaAppT' (getYKPayment' uuid)
          return (maybeToReponse "Couldn't update YKPayment" updatedPayment)
    Nothing ->
      return (RespFailure "Couldn't find YKPayment")

maybeToReponse :: Text -> Maybe a -> Response a
maybeToReponse str Nothing = RespFailure str
maybeToReponse _ (Just x) = RespSuccess x

getYKPayment' :: UUID -> SeldaT PG AppM (Maybe YKPayment)
getYKPayment' uuid =
  listToMaybe <$> getYKPaymentDB uuid

updateYKPayment' ::  UUID -> YooKassaPayment -> SeldaT PG AppM (Int)
updateYKPayment' uuid yooKassaPayment =
  updateYKPaymentDB uuid yooKassaPayment


getYKPayments :: String -> AppM [YKPayment]
getYKPayments uuid = getYKPayments' (UUID.fromString uuid)

getYKPayments' :: Maybe UUID -> AppM [YKPayment]
getYKPayments' Nothing = pure []
getYKPayments' (Just uuid) = do
  env <- ask
  runSeldaAppT (getPool env) (getYKPaymentsDB uuid)

nanosSinceEpoch :: UTCTime -> Int
nanosSinceEpoch =
  timeNanos . nominalDiffTimeToSeconds . utcTimeToPOSIXSeconds

timeNanos = floor . (1e9 *)

createLusher :: UUID -> Text -> Text -> Int -> SeldaT PG AppM (Response Bool)
createLusher uuid first second createdAt = do
  bool <- tryInsert lushers [Lusher def uuid first second createdAt]
  pure (RespSuccess bool)

createPHQ9 :: PHQ9Info -> Int -> SeldaT PG AppM (Bool)
createPHQ9 (PHQ9Info uuid a b c d e f g h i) createdAt = do
  tryInsert phq9 [PHQ9 def uuid a b c d e f g h i createdAt]

createPHQ15 :: PHQ15Info -> Int -> SeldaT PG AppM (Bool)
createPHQ15 (PHQ15Info uuid a b c d e f g h i j k l m n o) createdAt = do
  tryInsert phq15 [PHQ15 def uuid a b c d e f g h i j k l m n o createdAt]

createGAD7 :: GAD7Info -> Int -> SeldaT PG AppM (Bool)
createGAD7 (GAD7Info uuid a b c d e f g) createdAt = do
  tryInsert gad7 [GAD7 def uuid a b c d e f g createdAt]

createSubject5 :: Subjective5Info -> Int -> SeldaT PG AppM (Bool)
createSubject5 (Subjective5Info uuid family social work eating physical) createdAt = do
  tryInsert subjective5 [Subjective5 def uuid family social work eating physical createdAt]

createScore :: Bool -> UUID -> SeldaT PG AppM (Response Bool)
createScore False _ = pure (RespFailure "Can't save PHQ9")
createScore True uuid = do
  lastPHQ9 <- getLastPHQ9DB uuid
  lastPHQ15 <- getLastPHQ15DB uuid
  lastGAD7 <- getLastGAD7DB uuid
  lastSubjective5 <- getLastSubjective5DB uuid
  u <- liftIO $ getCurrentTime
  bool <-
    tryInsert
      scores
      [ score
          u
          (depressionScale lastPHQ9)
          (anxietyScale lastPHQ15)
          (somaticScale lastGAD7)
          (subjectiveScale lastSubjective5)
      ]
  pure (RespSuccess bool)
  where
    score :: UTCTime -> Maybe Int -> Maybe Int -> Maybe Int -> Maybe Int -> Score
    score u phq9Scale phq15scale gad7scale subjective5Scale =
      Score
        { scoreId = def,
          scoreAccountUUID = uuid,
          score =
            calculateScore'
              [ calculateScore (concatScales [phq9Scale, phq15scale, gad7scale]),
                calculateScore (concatScales [subjective5Scale])
              ],
          scoreDepressionScale = phq9Scale,
          scoreAnxietyScale = gad7scale,
          scoreSomaticScale = phq15scale,
          scoreSubjectiveScale = subjective5Scale,
          scoreCreatedAt = (nanosSinceEpoch u)
        }

concatScales :: [Maybe a] -> [a]
concatScales =
  join . map maybeToList

calculateScore' :: [Int] -> Int
calculateScore' [] = 0
calculateScore' xs =
  round $
    (fromIntegral $ sum xs)
      / (fromIntegral $ List.length $ filter (\x -> x > 0) xs)

calculateScore :: [Int] -> Int
calculateScore [] = 0
calculateScore xs =
  round $ (fromIntegral $ sum $ map (\x -> 100 - x) xs) / (fromIntegral $ List.length xs)

depressionScale :: [PHQ9] -> Maybe Int
depressionScale ((PHQ9 uuid aUUID a b c d e f g h i _) : xs) =
  Just $ round $ (fromIntegral points * 100) / 27
  where
    points :: Int
    points = a + b + c + d + e + f + g + h + i
depressionScale _ = Nothing

anxietyScale :: [PHQ15] -> Maybe Int
anxietyScale ((PHQ15 uuid aUUID a b c d e f g h i j k l m n o _) : xs) =
  Just $ round $ (fromIntegral points * 100) / 30
  where
    points :: Int
    points = a + b + c + d + e + f + g + h + i + k + l + m + n + o
anxietyScale _ = Nothing

somaticScale :: [GAD7] -> Maybe Int
somaticScale ((GAD7 uuid aUUID a b c d e f g _) : xs) =
  Just $ round $ (fromIntegral points * 100) / 21
  where
    points :: Int
    points = a + b + c + d + e + f + g
somaticScale _ = Nothing

subjectiveScale :: [Subjective5] -> Maybe Int
subjectiveScale [] = Nothing
subjectiveScale ((Subjective5 _ _ family social work eating physical _) : xs) =
  Just $ 100 - (round $ (fromIntegral points * 100) / 20)
  where
    points :: Int
    points = family + social + work + eating + physical

createActivity :: UUID -> ActivityType -> Int -> SeldaT PG AppM (Response (ID Activity))
createActivity uuid activity createdAt = do
  int <- insertWithPK activities [Activity def uuid activity createdAt]
  pure (RespSuccess int)

createSettings :: UUID -> Text -> Text -> Int -> SeldaT PG AppM (Response (ID Settings))
createSettings uuid name phone createdAt = do
  int <- insertWithPK settings [Settings def uuid name phone createdAt createdAt]
  pure (RespSuccess int)

updateSettings :: Int -> UUID -> Text -> Text -> Int -> SeldaT PG AppM (Response Bool)
updateSettings id uuid name phone updatedAt = do
  -- int <- insertWithPK settings [Settings def uuid name phone updatedAt updatedAt]
  int <- updateSettingsDB id uuid name phone updatedAt
  pure (RespSuccess (int == 1))

updateSettingsDB :: Int -> UUID -> Text -> Text -> Int -> SeldaT PG AppM (Int)
updateSettingsDB id uuid name phone updatedAt =
  update
    settings
    (\s -> (s ! #setId .== literal (toId (fromIntegral id))))
    ( \s ->
        s
          `with` [ #setName := (literal name),
                   #setPhone := (literal phone),
                   #setUpdatedAt := (literal updatedAt)
                 ]
    )

createReport :: UUID -> Int -> SeldaT PG AppM (Response (ID Report))
createReport uuid createdAt = do
  int <- insertWithPK reports [Report def uuid createdAt]
  pure (RespSuccess int)

createProgram :: UUID -> Int -> SeldaT PG AppM (Response (ID Program))
createProgram uuid createdAt = do
  int <- insertWithPK programs [Program def uuid createdAt]
  pure (RespSuccess int)

insertPlan :: UUID -> UUID -> PlanType -> Int -> SeldaT PG AppM (Response Plan)
insertPlan accountUUID planUUID planType_ createdAt = do
  isCreated <- tryInsert plans [plan]
  if isCreated
    then return $ RespSuccess plan
    else return $ RespFailure (T.pack (Text.Show.show isCreated))
  where
    plan = createPlan planUUID accountUUID planType_ createdAt

createPlan :: UUID -> UUID -> PlanType -> Int -> Plan
createPlan planUUID accountUUID planType_ createdAt =
  Plan planUUID accountUUID planType_ price createdAt createdAt
  where
    price = 
      case planType_ of
        MonthlyRecurring -> 500
        Annual -> 3000


insertYKPayment :: UUID -> UUID -> Int -> YooKassaPayment -> SeldaT PG AppM (Response YKPayment)
insertYKPayment ykPaymentUUID planUUID createdAt yooKassaPayment = do
  isCreated <- tryInsert yk_payments [ykPayment]
  if isCreated
    then return $ RespSuccess ykPayment
    else return $ RespFailure (T.pack (Text.Show.show yooKassaPayment))
  where
    ykPayment = createYKPaymentMonthlyRecurring ykPaymentUUID planUUID createdAt yooKassaPayment

createYKPaymentMonthlyRecurring :: UUID -> UUID -> Int -> YooKassaPayment -> YKPayment
createYKPaymentMonthlyRecurring ykPaymentUUID planUUID createdAt yooKassaPayment =
  YKPayment ykPaymentUUID planUUID Pending createdAt confirmationURL (yooKassaId yooKassaPayment) (T.pack $ Text.Show.show yooKassaPayment)
      where
        confirmationURL = fromMaybe "" $ yooKassaConfirmationUrl <$> yooKassaConfirmation yooKassaPayment


postIntroduction :: IntroductionInfo -> AppM (Response Bool)
postIntroduction (IntroductionInfo uuid needHelp treatment anxiety gender age name) = do
  env <- ask
  u <- liftIO $ getCurrentTime
  runSeldaAppT (getPool env) (createIntroduction uuid needHelp treatment anxiety gender age name (nanosSinceEpoch u))

createIntroduction :: UUID -> Text -> Text -> Text -> Text -> Text -> Text -> Int -> SeldaT PG AppM (Response Bool)
createIntroduction uuid needHelp treatment anxiety gender age name createdAt = do
  bool <- tryInsert introductions [Introduction def uuid needHelp treatment anxiety gender age name createdAt]
  pure (RespSuccess bool)

postGuests :: LanguageInfo -> AppM (Account)
postGuests (LanguageInfo language) = do
  uuid <- liftIO $ createUuid
  env <- ask
  runSeldaAppT (getPool env) (insertGuest uuid language)

postSignin :: SigninInfo -> AppM (Response Account)
postSignin s = do
  uuid <- liftIO $ createUuid
  env <- ask
  runSeldaAppT (getPool env) (checkEmail (signinEmail s) (signinPassword s))

checkEmail :: Text -> Text -> SeldaT PG AppM (Response Account)
checkEmail txt pwd = do
  a <- query $ getAccountDB txt
  return (checkPwd a pwd)

checkPwd :: [Account] -> Text -> Response Account
checkPwd [] pwd = RespFailure "No account with this email"
checkPwd (a : as) (pwd) =
  case checkDBPwd (password a) pwd of
    PasswordCheckFail ->
      RespFailure "Password is incorrect"
    PasswordCheckSuccess ->
      RespSuccess (a)

checkDBPwd :: Maybe (PasswordHash Bcrypt) -> Text -> PasswordCheck
checkDBPwd Nothing _ = PasswordCheckFail
checkDBPwd (Just passHash) pwd =
  checkPassword (mkPassword pwd) passHash

-- DATABASE REQUESTS

getAccountDB :: Text -> Query PG (Row PG Account)
getAccountDB txt = do
  account <- select (accounts)
  restrict (account ! #email .== literal (Just txt))
  return account

getAccountsDB :: SeldaT PG AppM [Account]
getAccountsDB =
  query (select (accounts))

getLushersDB :: UUID -> SeldaT PG AppM [Lusher]
getLushersDB uuid =
  query $ do
    ls <- select lushers
    restrict (ls ! #lAccountUUID .== literal uuid)
    return (ls)

getIntroductionsDB :: UUID -> SeldaT PG AppM [Introduction]
getIntroductionsDB uuid =
  query $ do
    ls <- select introductions
    restrict (ls ! #intAccountUUID .== literal uuid)
    return (ls)

getScoresDB :: UUID -> SeldaT PG AppM [Score]
getScoresDB uuid =
  query $ do
    ls <- select scores
    restrict (ls ! #scoreAccountUUID .== literal uuid)
    order (ls ! #scoreCreatedAt) descending
    return (ls)

getActivitiesDB :: UUID -> UTCTime -> SeldaT PG AppM [Activity]
getActivitiesDB uuid now =
  -- 1 hour before
  let from = (nanosSinceEpoch now) - (timeNanos (10 * 60 * 60))
   in query $ do
        ls <- select activities
        restrict (ls ! #activityAccountUUID .== literal uuid)
        restrict (ls ! #activityCreatedAt .>= int from)
        order (ls ! #activityCreatedAt) descending
        return (ls)

getSettingsDB :: UUID -> SeldaT PG AppM [Settings]
getSettingsDB uuid =
  query $ do
    ls <- select settings
    restrict (ls ! #setAccountUUID .== literal uuid)
    return (ls)

getPHQ9DB :: UUID -> SeldaT PG AppM [PHQ9]
getPHQ9DB uuid =
  query $ do
    ls <- select phq9
    restrict (ls ! #phq9AccountUUID .== literal uuid)
    return (ls)

getLastPHQ9DB :: UUID -> SeldaT PG AppM [PHQ9]
getLastPHQ9DB uuid =
  query $
    limit 0 1 $ do
      tests <- select phq9
      restrict (tests ! #phq9AccountUUID .== literal uuid)
      order (tests ! #phq9CreatedAt) descending
      return (tests)

getLastPHQ15DB :: UUID -> SeldaT PG AppM [PHQ15]
getLastPHQ15DB uuid =
  query $
    limit 0 1 $ do
      tests <- select phq15
      restrict (tests ! #phq15AccountUUID .== literal uuid)
      order (tests ! #phq15CreatedAt) descending
      return (tests)

getLastGAD7DB :: UUID -> SeldaT PG AppM [GAD7]
getLastGAD7DB uuid =
  query $
    limit 0 1 $ do
      tests <- select gad7
      restrict (tests ! #gad7AccountUUID .== literal uuid)
      order (tests ! #gad7CreatedAt) descending
      return (tests)

getLastSubjective5DB :: UUID -> SeldaT PG AppM [Subjective5]
getLastSubjective5DB uuid =
  query $
    limit 0 1 $ do
      tests <- select subjective5
      restrict (tests ! #subAccountUUID .== literal uuid)
      order (tests ! #subCreatedAt) descending
      return (tests)

getPlansDB :: UUID -> SeldaT PG AppM [Plan]
getPlansDB uuid =
  query $ do
    ls <- select plans
    restrict (ls ! #planAccountUUID .== literal uuid)
    return (ls)

getYKPaymentsDB :: UUID -> SeldaT PG AppM [YKPayment]
getYKPaymentsDB uuid =
  query $ do
    ls <- select yk_payments
    restrict (ls ! #ykPaymentPlanUUID .== literal uuid)
    return (ls)

getYKPaymentDB :: UUID -> SeldaT PG AppM [YKPayment]
getYKPaymentDB uuid =
  query $ do
    ls <- select yk_payments
    restrict (ls ! #ykPaymentUUID .== literal uuid)
    return (ls)

updateYKPaymentDB :: UUID -> YooKassaPayment -> SeldaT PG AppM (Int)
updateYKPaymentDB uuid yooKassaPayment = do
  update
    yk_payments
    (\ykPayment -> (ykPayment ! #ykPaymentUUID) .== (literal uuid))
    (\ykPayment -> ykPayment `with` [#ykPaymentStatus := (literal status), #ykPaymentResponse := (literal respRaw)])
  where
    status = yooKassaStatus yooKassaPayment
    respRaw = T.pack $ Text.Show.show yooKassaPayment

toResponse :: Maybe a -> Response a
toResponse (Just a) = RespSuccess a
toResponse Nothing = RespFailure "General error"

confirmEmail :: Confirmation -> SeldaT PG AppM (Response AccountConfirmation)
confirmEmail confirm = do
  (cs) <- findConfirmation confirm
  (as) <- findAccount confirm
  a <- confirmAccount (getAccount cs as)
  return (toResponse (fmap Confirm (a >>= email)))
  where
    getAccount :: [Confirmation] -> [Account] -> Maybe Account
    getAccount ((Confirmation uuid aUuid) : []) (account : []) = Just account
    getAccount _ _ = Nothing

confirmAccount :: Maybe Account -> SeldaT PG AppM (Maybe Account)
confirmAccount account =
  -- TODO: confirm account
  return account

findConfirmation :: Confirmation -> SeldaT PG AppM ([Confirmation])
findConfirmation (Confirmation uuid _) =
  query $ do
    cs <- select confirmations
    restrict (cs ! #confirmationUUID .== literal uuid)
    return (cs)

findAccount :: Confirmation -> SeldaT PG AppM ([Account])
findAccount (Confirmation _ aUuid) =
  query $ do
    as <- select accounts
    restrict (as ! #accountUUID .== literal aUuid)
    return (as)

findAccountByEmail :: Text -> SeldaT PG AppM ([Account])
findAccountByEmail email =
  query $ do
    as <- select accounts
    restrict (as ! #email .== literal (Just email))
    return (as)

acceptAccountDisclaimer :: UUID -> SeldaT PG AppM (Int)
acceptAccountDisclaimer uuid = do
  update
    accounts
    (\account -> (account ! #accountUUID) .== (literal uuid))
    (\account -> account `with` [#accDisclaimer := (literal True)])

updateAccountPassword :: UUID -> PasswordHash Bcrypt -> SeldaT PG AppM (Int)
updateAccountPassword uuid pwd = do
  update
    accounts
    (\account -> (account ! #accountUUID) .== (literal uuid))
    (\account -> account `with` [#password := (literal (Just pwd))])

insertAccount :: UUID -> PasswordHash Bcrypt -> AccountInfo -> SeldaT PG AppM (Response Account)
insertAccount uuid passwordHash a = do
  isCreated <- tryInsert accounts [account]
  if isCreated
    then return $ RespSuccess account
    else return $ RespFailure ("The user with " <> accountEmail a <> " is already registered")
  where
    account = createAccount uuid (accountEmail a) passwordHash (accountLanguage a)

insertGuest :: UUID -> Language -> SeldaT PG AppM (Account)
insertGuest uuid language = do
  created <- tryInsert accounts [createGuest uuid language]
  return (createGuest uuid language)

insertConfirmation :: UUID -> UUID -> SeldaT PG AppM (Either String UUID)
insertConfirmation uuid accountUUID = do
  isCreated <- tryInsert confirmations [createConfirmation uuid accountUUID]
  if isCreated
    then return $ Right uuid
    else return $ Left "Something went wrong"

createAccount :: UUID -> Text -> PasswordHash Bcrypt -> Language -> Account
createAccount uuid email password language =
  Account uuid (Just email) (Just password) False False language

createGuest :: UUID -> Language -> Account
createGuest uuid language =
  Account uuid Nothing Nothing False False language

createConfirmation :: UUID -> UUID -> Confirmation
createConfirmation uuid accountUUID =
  Confirmation uuid accountUUID

initConnectionPool :: PGConnectInfo -> IO (Pool (SeldaConnection PG))
initConnectionPool pgConnectInfo =
  createPool
    (pgOpen pgConnectInfo)
    seldaClose
    2 -- stripes
    60 -- unused connections are kept open for a minute
    10 -- max. 10 connections open per stripe

nt :: Env -> AppM a -> Handler a
nt s x = runReaderT x s

runApp :: Env -> Application
runApp env = corsPolicy $ serve api $ hoistServer api (nt env) server

corsPolicy = cors (const $ Just policy)
  where
    policy =
      simpleCorsResourcePolicy
        { corsMethods = ["GET", "POST", "PUT", "OPTIONS"],
          corsOrigins =
            Just
              ( [ "https://mentalscore.xyz",
                  "http://192.168.2.3:34101",
                  "http://localhost",
                  "http://localhost:34101"
                ],
                True
              ),
          corsRequestHeaders = ["authorization", "content-type"]
        }

main :: IO ()
main = do
  config <- Conf.mkConfig "mentalscore"
  appConfig :: AppConfig <- Conf.fetch config
  pool <- initConnectionPool (getPGConnectInfo appConfig)
  _ <- print $ appConfig
  -- _ <- sendBasicEmail
  gen <- getStdGen
  run 8090 $ runApp (Env pool gen appConfig)

createUuid :: IO UUID
createUuid =
  V4.nextRandom

-- Email templates
sendVerifyEmail :: Either String UUID -> Text -> AppM ()
sendVerifyEmail (Right uuid) email = do
  Env _ _ config <- ask
  sendBasicEmail
    "Welcome to MentalScore, please verify your email address account"
    (encodeUtf8 $ verifyEmailBody $ generateLink config uuid)
    (encodeUtf8 email)
sendVerifyEmail (Left error) email =
  liftIO $ print error

generateLink :: AppConfig -> UUID -> Text
generateLink config uuid =
  ((appFrontendProtocol config) <> "://" <> (appFrontendDomain config) <> ":" <> (appFrontendPort config) <> "/verify/") <> (UUID.toText uuid)

verifyEmailBody :: Text -> Text
verifyEmailBody link =
  ( "Hello\n\
    \n\
    \You registered an account on MentalScore, before being able to use your account you need to verify that this is your email address by clicking here: "
  )
    <> link
    <> ( "\n\
         \Kind Regards, MentalScore"
       )

-- Email templates
sendEmailResetPassword :: UUID -> Text -> AppM ()
sendEmailResetPassword uuid email = do
  Env _ _ config <- ask
  sendBasicEmail
    "Did you make a request to reset your pasword?"
    (encodeUtf8 $ "Here is a link to reset your password \n" <> generateResetLink config uuid)
    (encodeUtf8 email)

generateResetLink :: AppConfig -> UUID -> Text
generateResetLink config uuid =
  ("https://mentalscore.xyz/app/#/update-password/") <> (UUID.toText uuid)

-- sendVerifyEmail (Left error) email =
--   liftIO $ print error

-- sendEmail :: Text -> Lazy.Text -> Text -> AppM ()
-- sendEmail subject body emailRecipient = do
-- Env _ _ config <- ask
-- when (appMode config == "dev") $
--   liftIO (mapM_ print [show subject, show body])
-- when (appMode config == "production") $
--   liftIO $ SMTP.doSMTPSTARTTLS (show $ smtpServer (getSmtpCredentials config)) $ \c -> do
--       authSucceed <- SMTP.authenticate Auth.LOGIN (username config) (show $ smtpPassword (getSmtpCredentials config)) c
--       if authSucceed
--         then SMTP.sendPlainTextMail (show emailRecipient) (username config) (show subject) body c
--         else print "Authentication error."
-- where
--   username :: AppConfig -> String
--   username cfg =
--     show $ smtpUsername $ getSmtpCredentials cfg
-- pure ()

sendBasicEmail :: Text -> ByteString -> ByteString -> AppM ()
sendBasicEmail subject body email = do
  env <- ask
  let appConfig = getAppConfig env
  liftIO $ sendHGEmail (hailgunContext appConfig) subject body [email]

sendHGEmail :: HG.HailgunContext -> Text -> ByteString -> [HG.UnverifiedEmailAddress] -> IO ()
sendHGEmail context subject body toAddress =
  case mkMessage subject body toAddress of
    Left err -> putStrLn ("Making Hailgun Message failed: " ++ err)
    Right msg -> do
      result <- HG.sendEmail context msg -- << Send Email Here!
      case result of
        Left err -> putStrLn ("Sending failed: " ++ show err)
        Right resp -> putStrLn ("Sending succeeded: " ++ show resp)

hailgunContext :: AppConfig -> HG.HailgunContext
hailgunContext cfg =
  HG.HailgunContext (T.unpack (appMailgunDomain cfg)) (T.unpack (appMailgunAPIKey cfg)) Nothing

mkMessage :: Text -> ByteString -> [HG.UnverifiedEmailAddress] -> Either HG.HailgunErrorMessage HG.HailgunMessage
mkMessage subject body toAddress =
  HG.hailgunMessage
    subject
    (HG.TextOnly body)
    "Mental Score Support Team <mailgun@mail.mentalscore.xyz>"
    HG.emptyMessageRecipients {HG.recipientsTo = toAddress}
    []

{- App configuration -}

data AppConfig = AppConfig
  { appConnectUser :: Text,
    appConnectHost :: Text,
    appConnectDB :: Text,
    appConnectPassword :: Text,
    appSmtpServer :: Text,
    appSmtpUser :: Text,
    appSmtpPassword :: Text,
    appMode :: Text,
    appProtocol :: Text,
    appDomain :: Text,
    appPort :: Text,
    appFrontendProtocol :: Text,
    appFrontendDomain :: Text,
    appFrontendPort :: Text,
    appMailgunDomain :: Text,
    appMailgunAPIKey :: Text,
    -- appMailgunReplyAddress :: Text,
    -- appMailgunUserAddress :: Text
    appYooKassaId :: ByteString,
    appYooKassaKey :: ByteString
  }
  deriving (Generic, Show)

instance Conf.FromConfig AppConfig

instance Conf.DefaultConfig AppConfig where
  configDef =
    AppConfig
      { appConnectUser = "postgres",
        appConnectHost = "127.0.0.1",
        appConnectDB = "coddeys",
        appConnectPassword = "",
        appSmtpServer = "smtp.yandex.ru",
        appSmtpUser = "support@mentalscore.xyz",
        appSmtpPassword = "mentalscore2020",
        appMode = "dev",
        appProtocol = "http",
        appDomain = "localhost",
        appPort = "8090",
        appFrontendProtocol = "http",
        appFrontendDomain = "localhost",
        appFrontendPort = "34101",
        appMailgunDomain = "mg.mentalscore.xyz",
        appMailgunAPIKey = "1ff3b3423264ee147b5a73a58e3e2405f-29561299-0f9265e8",
        -- appMailgunReplyAddress = "dima@mentalscore.xyz",
        -- appMailgunUserAddress = "dima@mentalscore.xyz"
        appYooKassaId = "936879",
        appYooKassaKey = "test_K9zTGS7tS0XToBwg_z-VuU0GODJrKygzkLVNeU58Lwk"
      }

getPGConnectInfo :: AppConfig -> PGConnectInfo
getPGConnectInfo appConfig =
  ((appConnectDB appConfig) `on` (appConnectHost appConfig) `auth` (appConnectUser appConfig, appConnectPassword appConfig))

getSmtpCredentials :: AppConfig -> SmtpCredentials
getSmtpCredentials appConfig =
  SmtpCredentials (appSmtpServer appConfig) (appSmtpUser appConfig) (appSmtpPassword appConfig)

{- YooKassaPayment -}

data YooKassaPayment = YooKassaPayment
  { yooKassaId :: Text,
    yooKassaStatus :: D.PaymentStatus,
    yooKassaConfirmation :: Maybe YooKassaConfirmation,
    yooKassaResponse :: Value
  }
  deriving (Generic, Show)

instance ToJSON YooKassaPayment

instance ToJSON (Response YooKassaPayment) where
  toJSON (RespSuccess yooKassaPayment) =
    toJSON yooKassaPayment
  toJSON (RespFailure error) =
    object ["error" .= error]

instance FromJSON YooKassaPayment where
  parseJSON (Object v) = do
    id <- v .: "id"
    status <- v .: "status"
    confirmation <- v .:? "confirmation" -- when the status is canceled there is no confirmation field
    value <- fn (Object v)
    return (YooKassaPayment id status confirmation value)
    where
      fn value = pure (value)

data YooKassaConfirmation = YooKassaConfirmation
  {yooKassaConfirmationUrl :: Text}
  deriving (Generic, Show)

instance ToJSON YooKassaConfirmation

instance FromJSON YooKassaConfirmation where
  parseJSON (Object v) = YooKassaConfirmation <$> v .: "confirmation_url"

data YooKassaPaymentInfo = YooKassaPaymentInfo
  { yooKassaYKPaymentUUID :: UUID,
    yooKassaAmountValue :: Text,
    yooKassaAmountCurrency :: Text,
    yooKassaPaymentType :: Text,
    yooKassaReturnUrl :: Text,
    yooKassaDescription :: Text
  }
  deriving (Generic, Show)

instance ToJSON YooKassaPaymentInfo where
  toJSON (YooKassaPaymentInfo _ value currency type_ url description) =
    object
      [ ("amount", object ["value" .= value, "currency" .= currency]),
        ("capture", Bool True),
        ( "confirmation",
          object [("type", "redirect"), "return_url" .= url]
        ),
        "description" .= description
      ]

instance FromJSON YooKassaPaymentInfo where
  parseJSON (Object v) =
    YooKassaPaymentInfo
      <$> v .: "id"
      <*> v .: "value"
      <*> v .: "currency"
      <*> v .: "type"
      <*> v .: "return_url"
      <*> v .: "description"

instance FromJSON (Response YooKassaPayment) where
  parseJSON obj = parseJSON obj

instance FromJSON (Response Bool) where
  parseJSON obj = parseJSON obj

yooKassaPayments' :<|> yooKassaCreatePayment' = client yooKassaApi'

yooKassaApi' :: Proxy YooKassaAPI'
yooKassaApi' = Proxy

data Credentials = Credentials
  { cUser :: Text,
    cPassword :: Text
  }
  deriving (Eq, Show)

type YooKassaAPI' =
  ( BasicAuth "Secret" Credentials
      :> "payments"
      :> Capture "uuid" String
      :> Get '[JSON] (YooKassaPayment)
  )
    :<|> ( BasicAuth "Secret" Credentials
             :> "payments"
             :> Header' '[Required, Strict] "Idempotence-Key" String
             :> ReqBody '[JSON] YooKassaPaymentInfo
             :> Post '[JSON] (YooKassaPayment)
         )

yooKassaCreatePayment :: ByteString -> ByteString -> YooKassaPaymentInfo -> ClientM (YooKassaPayment)
yooKassaCreatePayment kassaId kassaKey info =
  yooKassaCreatePayment'
    (BasicAuthData kassaId kassaKey)
    (UUID.toString $ yooKassaYKPaymentUUID info)
    info

yooKassaPayments :: ByteString -> ByteString -> Text -> ClientM YooKassaPayment
yooKassaPayments kassaId kassaKey uuid =
  ( yooKassaPayments'
      (BasicAuthData kassaId kassaKey)
      (T.unpack uuid)
  )

yooKassaClientM :: ClientM (a) -> IO (Either ClientError (a))
yooKassaClientM fn = do
  mn <- newManager NT.tlsManagerSettings
  url <- parseBaseUrl yooKassApiEndpoint
  runClientM fn (mkClientEnv mn url)

yooKassApiEndpoint :: String
yooKassApiEndpoint = "https://api.yookassa.ru/v3"
