{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}

module Main where

import Control.Monad
import Data.Char
import Data.List
import Database.Selda
import Database.Selda.PostgreSQL
import GMH.Data
import System.Console.GetOpt
import System.Environment
import System.Exit
import System.IO
import Text.Printf

pgConnectInfo :: PGConnectInfo
pgConnectInfo =
    ("coddeys" `on` "127.0.0.1" `auth` ("postgres", "password"))

main :: IO ()
main = do
  (flags) <- getArgs >>= parse
  mapM_ action flags

action :: Flag -> IO ()
action Create = withPostgreSQL pgConnectInfo $ do
  -- createTable accounts
  -- createTable confirmations
  -- createTable lushers
  -- createTable introductions
  -- createTable scores
  -- createTable phq9
  -- createTable phq15
  -- createTable gad7
  -- createTable subjective5
  -- createTable activities
  -- createTable settings
  -- createTable reports
  -- createTable programs
  -- createTable resetPasswords
  -- createTable plans
  createTable yk_payments
  liftIO $ print "Create table Successfullly"
action Drop = withPostgreSQL pgConnectInfo $ do
  -- dropTable accounts
  -- dropTable confirmations
  -- dropTable lushers
  -- dropTable introductions
  -- dropTable scores
  -- dropTable phq9
  -- dropTable phq15
  -- dropTable gad7
  -- dropTable subjective5
  -- dropTable activities
  -- dropTable settings
  -- dropTable reports
  -- dropTable programs
  -- dropTable resetPasswords
  -- dropTable plans
  dropTable yk_payments
  liftIO $ print "Drop table Successfullly"
-- action Insert = withPostgreSQL pgConnectInfo $ do
--   insert_
--     accounts
--     [ Account def (Just "googleidhash") Nothing,
--       Account def (Just "email@email.com") Nothing,
--       Account def (Just "email@email.com") Nothing
--     ]
--   liftIO $ print "Insert data Successfullly"
action Help = print "help"

parse :: [String] -> IO ([Flag])
parse argv = case getOpt Permute flags argv of
  (args, fs, []) -> do
    let files = if null fs then ["-"] else fs
    if Help `elem` args
      then do
        hPutStrLn stderr (usageInfo header flags)
        exitWith ExitSuccess
      else return (nub (concatMap set args))
  (_, _, errs) -> do
    hPutStrLn stderr (concat errs ++ usageInfo header flags)
    exitWith (ExitFailure 1)
  where
    header = "Usage: scripts [-cih] "
    set f = [f]

data Flag
  = Help --     -h --help
  | Create --     -c --create
  | Drop --     -d --drop
  | Insert --     -i --insert
  deriving (Eq, Ord, Enum, Show, Bounded)

flags =
  [ Option
      ['c']
      ["create"]
      (NoArg Create)
      "Create table",
    Option
      ['d']
      ["drop"]
      (NoArg Drop)
      "Drop table",
    Option
      ['i']
      ["insert"]
      (NoArg Insert)
      "Insert data",
    Option
      ['h']
      ["help"]
      (NoArg Help)
      "Print this help message"
  ]

-- main :: IO ()
-- main = withSQLite "accounts.sqlite" $ do
--   createTable accounts
--   liftIO $ print "initDb"
--   -- insert_ accounts
--   --   [ Person "Velvet"    19 (Just Dog)
--   --   , Person "Kobayashi" 23 (Just Dragon)
--   --   , Person "Miyu"      10 Nothing
--   --   ]
