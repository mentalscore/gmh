{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE StandaloneDeriving #-}

module GMH.Data where

import Data.Aeson
import Data.Aeson.Types
import Data.Password.Bcrypt
import qualified Data.Text as T
import Data.Typeable
import Data.UUID
import Database.Selda
import Database.Selda.JSON
import Database.Selda.SqlType

data Language = English | Russian | Dutch
  deriving (Show, Read, Bounded, Enum)

instance SqlType Language

instance ToJSON Language where
  toJSON (English) = "english"
  toJSON (Russian) = "russian"
  toJSON (Dutch) = "dutch"

data Account = Account
  { accountUUID :: UUID,
    email :: Maybe Text,
    password :: Maybe (PasswordHash Bcrypt),
    accIsConfirmed :: Bool,
    accDisclaimer :: Bool,
    accLanguage :: Language
  }
  deriving (Generic, Show)

instance SqlRow Account

instance ToJSON Account where
  toJSON (Account accountUUID email password isConfirmed disclaimer language) =
    object ["id" .= show accountUUID, "email" .= email, "isConfirmed" .= isConfirmed, "disclaimer" .= disclaimer, "language" .= language]

instance (Typeable a) => SqlType (PasswordHash a) where
  mkLit = LCustom TText . LText . unPasswordHash
  sqlType _ = TText
  fromSql (SqlString x) = PasswordHash x
  fromSql v = error $ "fromSql: text column with non-text value: " ++ show v
  defaultValue = LCustom TText (LText "")

accounts :: Table Account
accounts = table "accounts" [#accountUUID :- primary, #email :- unique]

--  Confirmation Token

data Confirmation = Confirmation
  { confirmationUUID :: UUID,
    confirmationAccountUUID :: UUID
  }
  deriving (Generic, Show)

instance SqlRow Confirmation

instance FromJSON Confirmation

instance ToJSON Confirmation

confirmations :: Table Confirmation
confirmations = table "confirmations" [#confirmationUUID :- primary]

--  Reset Token

data ResetPassword = ResetPassword
  { resetPasswordUUID :: UUID,
    resetPasswordCreatedAt :: Int,
    resetPasswordAccountUUID :: UUID
  }
  deriving (Generic, Show)

instance SqlRow ResetPassword

instance FromJSON ResetPassword

instance ToJSON ResetPassword

resetPasswords :: Table ResetPassword
resetPasswords = table "resetPasswords" [#resetPasswordUUID :- primary]

-- LUSHER

data Lusher = Lusher
  { lId :: ID Lusher,
    lAccountUUID :: UUID,
    first :: Text,
    second :: Text,
    createdAt :: Int
  }
  deriving (Generic, Show)

instance SqlRow Lusher

instance FromJSON Lusher

instance ToJSON Lusher

lushers :: Table Lusher
lushers = table "lushers" [#lId :- autoPrimary]

-- SURVEYS
-- INTRODUCTIONS

data Introduction = Introduction
  { intId :: ID Introduction,
    intAccountUUID :: UUID,
    intNeedHelp :: Text,
    intTreatment :: Text,
    intAnxiety :: Text,
    intGender :: Text,
    intAge :: Text,
    intName :: Text,
    intCreatedAt :: Int
  }
  deriving (Generic, Show)

instance SqlRow Introduction

instance FromJSON Introduction

instance ToJSON Introduction

introductions :: Table Introduction
introductions = table "introductions" [#intId :- autoPrimary]

-- MAIN SCORE

data Score = Score
  { scoreId :: ID Score,
    scoreAccountUUID :: UUID,
    score :: Int,
    scoreDepressionScale :: Maybe Int,
    scoreAnxietyScale :: Maybe Int,
    scoreSomaticScale :: Maybe Int,
    scoreSubjectiveScale :: Maybe Int,
    scoreCreatedAt :: Int
  }
  deriving (Generic, Show)

instance SqlRow Score

instance FromJSON Score

instance ToJSON Score

scores :: Table Score
scores = table "scores" [#scoreId :- autoPrimary]

--  PHQ9

data PHQ9 = PHQ9
  { phq9Id :: ID PHQ9,
    phq9AccountUUID :: UUID,
    phq9a :: Int,
    phq9b :: Int,
    phq9c :: Int,
    phq9d :: Int,
    phq9e :: Int,
    phq9f :: Int,
    phq9g :: Int,
    phq9h :: Int,
    phq9i :: Int,
    phq9CreatedAt :: Int
  }
  deriving (Generic, Show)

instance SqlRow PHQ9

instance FromJSON PHQ9

instance ToJSON PHQ9

phq9 :: Table PHQ9
phq9 = table "phq9" [#phq9Id :- autoPrimary]

--  PHQ15

data PHQ15 = PHQ15
  { phq15Id :: ID PHQ15,
    phq15AccountUUID :: UUID,
    phq15a :: Int,
    phq15b :: Int,
    phq15c :: Int,
    phq15d :: Int,
    phq15e :: Int,
    phq15f :: Int,
    phq15g :: Int,
    phq15h :: Int,
    phq15i :: Int,
    phq15j :: Int,
    phq15k :: Int,
    phq15l :: Int,
    phq15m :: Int,
    phq15n :: Int,
    phq15o :: Int,
    phq15CreatedAt :: Int
  }
  deriving (Generic, Show)

instance SqlRow PHQ15

instance FromJSON PHQ15

instance ToJSON PHQ15

phq15 :: Table PHQ15
phq15 = table "phq15" [#phq15Id :- autoPrimary]

--  GAD7

data GAD7 = GAD7
  { gad7Id :: ID GAD7,
    gad7AccountUUID :: UUID,
    gad7a :: Int,
    gad7b :: Int,
    gad7c :: Int,
    gad7d :: Int,
    gad7e :: Int,
    gad7f :: Int,
    gad7g :: Int,
    gad7CreatedAt :: Int
  }
  deriving (Generic, Show)

instance SqlRow GAD7

instance FromJSON GAD7

instance ToJSON GAD7

gad7 :: Table GAD7
gad7 = table "gad7" [#gad7Id :- autoPrimary]

--  Subjective5

data Subjective5 = Subjective5
  { subId :: ID Subjective5,
    subAccountUUID :: UUID,
    subHome :: Int,
    subSocial :: Int,
    subWork :: Int,
    subEating :: Int,
    subPhysical :: Int,
    subCreatedAt :: Int
  }
  deriving (Generic, Show)

instance SqlRow Subjective5

instance FromJSON Subjective5

instance ToJSON Subjective5

subjective5 :: Table Subjective5
subjective5 = table "subjective5" [#subId :- autoPrimary]

--  Activity

data ActivityType
  = Alcohol
  | Beer
  | ChattingFriends
  | Gym
  | Joint
  | JunkFood
  | MeetFriends
  | Reading
  | Running
  | Shopping
  | Smoking
  | StayingHome
  | Vacation
  | Walking
  | WatchTV
  | PlayVideoGame
  deriving (Show, Read, Bounded, Enum)

instance SqlType ActivityType

instance ToJSON ActivityType where
  toJSON (Alcohol) = "alcohol"
  toJSON (Beer) = "beer"
  toJSON (ChattingFriends) = "chattingFriends"
  toJSON (Gym) = "gym"
  toJSON (Joint) = "joint"
  toJSON (JunkFood) = "junkFood"
  toJSON (MeetFriends) = "meetFriends"
  toJSON (Reading) = "reading"
  toJSON (Running) = "running"
  toJSON (Shopping) = "shopping"
  toJSON (Smoking) = "smoking"
  toJSON (StayingHome) = "stayingHome"
  toJSON (Vacation) = "vacation"
  toJSON (Walking) = "walking"
  toJSON (WatchTV) = "watchTV"
  toJSON (PlayVideoGame) = "playVideoGame"

instance FromJSON ActivityType where
  parseJSON (String t) = (fromString $ T.unpack t)
    where
      fromString "alcohol" = pure Alcohol
      fromString "beer" = pure Beer
      fromString "chattingFriends" = pure ChattingFriends
      fromString "gym" = pure Gym
      fromString "joint" = pure Joint
      fromString "junkFood" = pure JunkFood
      fromString "meetFriends" = pure MeetFriends
      fromString "reading" = pure Reading
      fromString "running" = pure Running
      fromString "shopping" = pure Shopping
      fromString "smoking" = pure Smoking
      fromString "stayingHome" = pure StayingHome
      fromString "vacation" = pure Vacation
      fromString "walking" = pure Walking
      fromString "watchTV" = pure WatchTV
      fromString "playVideoGame" = pure PlayVideoGame
      fromString _ = pure Reading

data Activity = Activity
  { activityId :: ID Activity,
    activityAccountUUID :: UUID,
    activityType :: ActivityType,
    activityCreatedAt :: Int
  }
  deriving (Generic, Show)

instance SqlRow Activity

instance FromJSON Activity

instance ToJSON Activity

activities :: Table Activity
activities = table "activities" [#activityId :- autoPrimary]

-- SETTINGS

data Settings = Settings
  { setId :: ID Settings,
    setAccountUUID :: UUID,
    setName :: Text,
    setPhone :: Text,
    setCreatedAt :: Int,
    setUpdatedAt :: Int
  }
  deriving (Generic, Show)

instance SqlRow Settings

instance FromJSON Settings

instance ToJSON Settings

settings :: Table Settings
settings = table "settings" [#setId :- autoPrimary]

-- Report

data Report = Report
  { reportId :: ID Report,
    reportAccountUUID :: UUID,
    reportCreatedAt :: Int
  }
  deriving (Generic, Show)

instance SqlRow Report

instance FromJSON Report

instance ToJSON Report

reports :: Table Report
reports = table "reports" [#reportId :- autoPrimary]

-- Program

data Program = Program
  { programId :: ID Program,
    programAccountUUID :: UUID,
    programCreatedAt :: Int
  }
  deriving (Generic, Show)

instance SqlRow Program

instance FromJSON Program

instance ToJSON Program

programs :: Table Program
programs = table "programs" [#programId :- autoPrimary]

-- Plan

data PlanType
  = MonthlyRecurring
  | Annual
  deriving (Show, Read, Bounded, Enum)

instance SqlType PlanType

instance ToJSON PlanType where
  toJSON (MonthlyRecurring) = "monthlyRecurring"
  toJSON (Annual) = "annual"

instance FromJSON ( PlanType) where
  parseJSON (String t) = (fromString $ T.unpack t)
    where
      fromString "monthlyRecurring" = pure MonthlyRecurring
      fromString "annual" = pure Annual
      fromString _ = pure MonthlyRecurring


data Plan = Plan
  { planUUID :: UUID,
    planAccountUUID :: UUID,
    planType :: PlanType,
    planPrice :: Int,
    planCreatedAt :: Int,
    planEndAt :: Int
  }
  deriving (Generic, Show)

instance SqlRow Plan

instance FromJSON Plan

instance ToJSON Plan

plans :: Table Plan
plans = table "plans" [#planUUID :- primary]

-- YKPayment

data PaymentStatus
  = Pending
  | WaitingForCapture
  | Succeeded
  | Canceled
  deriving (Show, Read, Bounded, Enum)

instance SqlType PaymentStatus

instance ToJSON PaymentStatus where
  toJSON (Pending) = "pending"
  toJSON (WaitingForCapture) = "waiting_for_capture"
  toJSON (Succeeded) = "succeeded"
  toJSON (Canceled) = "canceled"

instance FromJSON (PaymentStatus) where
  parseJSON (String t) = (fromString $ T.unpack t)
    where
      fromString "pending" = pure Pending
      fromString "waiting_for_capture" = pure WaitingForCapture
      fromString "succeeded" = pure Succeeded
      fromString "canceled" = pure Canceled
      fromString _ = pure Canceled


data YKPayment = YKPayment
  { ykPaymentUUID :: UUID,
    ykPaymentPlanUUID :: UUID,
    ykPaymentStatus :: PaymentStatus,
    ykPaymentCreatedAt :: Int,
    ykPaymentConfirmationURL :: Text,
    ykPaymentResponseId :: Text,
    ykPaymentResponse :: Text
  }
  deriving (Generic, Show)

instance SqlRow YKPayment

instance FromJSON YKPayment

instance ToJSON YKPayment

yk_payments :: Table YKPayment
yk_payments = table "yk_payments" [#ykPaymentUUID :- primary]
