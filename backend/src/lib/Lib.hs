data YooKassaPayment = YooKassaPayment
  { youKassaId :: UUID
  , youKassaStatus :: Text
  }
  deriving (Generic, Show)

instance ToJSON YooKassaPayment

instance FromJSON YooKassaPayment where
  parseJSON (Object v) =
    YooKassaPayment
      <$> v .: "id"
      <*> v .: "status"


payments' :: BasicAuthData -> ClientM YooKassaPayment

payments' = client yooKassaApi'

yooKassaApi' :: Proxy YooKassaAPI'
yooKassaApi' = Proxy

data Credentials = Credentials
  { cUser :: Text
  , cPassword :: Text
  } deriving (Eq, Show)

type YooKassaAPI' = BasicAuth "Secret" Credentials :> "payments" :> "2b2ae41e-000f-5000-9000-113517e2610d" :> Get '[JSON] (YooKassaPayment)


queries :: ClientM YooKassaPayment
queries = do
  pos <- payments' (BasicAuthData "936879" "test_K9zTGS7tS0XToBwg_z-VuU0GODJrKygzkLVNeU58Lwk")
  -- message <- hello (Just "servant")
  -- em  <- marketing (ClientInfo "Alp" "alp@foo.com" 26 ["haskell", "mathematics"])
  return (pos)

main :: IO ()
main = do
  manager' <- newManager tlsManagerSettings
  url <- parseBaseUrl "https://api.yookassa.ru/v3"
  print (BaseUrl Https "api.yookassa.ru/v3" 443 "")
  res <- runClientM queries (mkClientEnv manager' url)
  -- case res of
  --   Left err -> putStrLn $ "Error: " ++ show err
  --   Right (pos, message, em) -> do
  --     print pos
  --     print message
  --     print em
  case res of
    Left err -> putStrLn $ "Error: " ++ show err
    Right payment -> print payment
