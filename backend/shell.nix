{ pkgs ? import <nixpkgs> { }}:

let
  haskellPackages = pkgs.haskell.packages.ghc8107;
  backend = haskellPackages.callPackage ./release.nix { };
in
backend.project.env
