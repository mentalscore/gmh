{ compiler ? "ghc8107" }:

let
  config = {
    packageOverrides = pkgs: rec {
      haskellPackages = pkgs.haskell.packages."${compiler}".override {
        overrides = haskellPackagesNew: haskellPackagesOld: rec {
          attoparsec =
            haskellPackagesNew.callPackage ./attoparsec.nix { };

          http-client =
            haskellPackagesNew.callPackage ./http-client.nix { };

          http-client-tls =
            haskellPackagesNew.callPackage ./http-client-tls.nix { };


          time-compat =
            haskellPackagesNew.callPackage ./time-compat.nix { };

          hashable-time =
            haskellPackagesNew.callPackage ./hashable-time.nix { };

          aeson =
            haskellPackagesNew.callPackage ./aeson.nix { };


          hailgun =
            haskellPackagesNew.callPackage ./hailgun.nix { };

          # attoparsec-iso8601 =
          #   haskellPackagesNew.callPackage ./attoparsec-iso8601.nix { };

          # aeson-pretty =
          #   haskellPackagesNew.callPackage ./aeson-pretty.nix { };

          # http2 =
          #   haskellPackagesNew.callPackage ./http2.nix { };

          project1 =
            haskellPackagesNew.callPackage ./project1.nix { };
        };
      };
    };
  };

  pkgs = import <nixpkgs> { inherit config; };

in
  { project1 = pkgs.haskellPackages.project1;
    hailgun = pkgs.haskellPackages.hailgun; 
    attoparsec_0_13_2_5 = pkgs.haskellPackages.attoparsec_0_13_2_5;
    http-client_0_6_4_1 = pkgs.haskellPackages.http-client_0_6_4_1;
    aeson_1_5_6_0 = pkgs.haskellPackages.aeson_1_5_6_0;
    hashable-time_0_2_1 = pkgs.haskellPackages.hashable-time_0_2_1;
    time-compat_1_9_5 =  pkgs.haskellPackages.time-compat_1_9_5;
  }
