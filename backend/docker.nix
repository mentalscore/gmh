{ pkgs ? import <nixpkgs> { }}:

let
  haskellPackages = pkgs.haskell.packages.ghc8107;
  backend = haskellPackages.callPackage ./release.nix { };

in pkgs.dockerTools.buildImage {
  name = "mentalscorexyz/backend";
  tag = "latest";
  contents = [ pkgs.cacert backend.project-minimal ];

  config = {
    Cmd = ["${backend.project-minimal}/bin/app"];
    ExposedPorts = { "8090/tcp" = { }; };
  };
}
