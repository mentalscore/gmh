"use strict";(self.webpackChunkmental_score=self.webpackChunkmental_score||[]).push([[682],{4633:function(e,a,t){t.r(a),t.d(a,{default:function(){return m}});var c=t(7294),l=(t(1597),t(8781)),s=t(6296);function m(){return c.createElement(l.ZP,null,c.createElement("div",{className:"w-full h-full flex flex-col justify-start items-center"},c.createElement("div",{className:"w-full flex justify-center mx-8 bg-darkGreen text-pureGray"},c.createElement("div",{className:"h-full w-full flex flex-col-reverse justify-center max-w-screen-lg mx-4"},c.createElement("div",{className:"-mb-40 "},c.createElement(s.S,{src:"../images/maria.jpg",alt:"Healhty Face",placeholder:"darkGreen",className:"w-full lg:w-56 ",width:500,__imageData:t(9704)})),c.createElement("div",{className:"flex flex-col my-8 pb-8 gap-4"},c.createElement("h3",{className:"text-lg"}," Мы переосмыслили систему"),c.createElement("h1",{className:"text-4xl"},"Сделайте ментальное здоровье своим приоритетом"),c.createElement("p",{className:"text-sm"},"Важность поддержания психологического здоровья часто отодвигается на второй план. Кажется, что это может подождать. Ценник на сессии со специалистом кусается, симптомы вроде не так страшны, а признаваться в ментальных проблемах до сих пор очень многим стыдно. Но психологическая помощь необходима всем. Вот поэтому мы создали MentalScore - слияние опыта психологии и технологий, которое всегда будет у Вас под рукой.")))),c.createElement("div",{className:"w-full flex justify-center mx-8 bg-pureGray text-darkGreen"},c.createElement("div",{className:"h-full w-full flex flex-col-reverse justify-center max-w-screen-lg mx-4"},c.createElement("div",{className:"w-full flex flex-col my-8 pt-40 pb-8 gap-4"},c.createElement("h1",{className:"text-xl font-medium text-darkGray "},"Новый подход"),c.createElement("p",{className:"text-sm font-medium text-darkGray"},"Мы в MentalScore считаем, что психологическая помощь - постоянный процесс развития более прочного и гармоничного отношения с самим собой, который выстроит прочный личностный фундамент, позволяющий пресекать или уверено преодолевать кризисы. Забота о своём ментальном здоровье - залог качественно нового уровня жизни.")))),c.createElement("div",{className:"w-full flex justify-center mx-8 bg-lightGreen text-darkGreen"},c.createElement("div",{className:"h-full w-full flex flex-col justify-center max-w-screen-lg mx-4 py-16 gap-4"},c.createElement("h1",{className:"text-4xl font-medium text-pureGray text-center "},"Познакомьтесь с командой"),c.createElement("p",{className:"text-sm font-medium text-darkGray"},"Наша миссия - нормализовать заботу о психологическом здоровье. Сделать процесс проще, удобнее и доступнее для каждого."),c.createElement("div",{className:" m-auto md:m-0 flex flex-col lg:flex-row justify-center gap-4"},c.createElement("div",{className:"mx-4"},c.createElement(s.S,{src:"../images/face_2.jpg",alt:"Maria",placeholder:"darkGreen",className:"rounded-lg my-4 lg:w-64",__imageData:t(7277)}),c.createElement("p",{className:"text-bold text-4xl text-pureGray"},"Мария"),c.createElement("p",{className:""},"Основатель & Врач-Психотерапевт")),c.createElement("div",{className:"mx-4"},c.createElement(s.S,{src:"../images/dima_s.jpg",alt:"Dima",placeholder:"darkGreen",className:"rounded-lg my-4 lg:w-64",__imageData:t(5064)}),c.createElement("p",{className:"text-bold text-4xl text-pureGray"},"Дима"),c.createElement("p",{className:""},"Основатель & CTO"))))),c.createElement("div",{className:"w-full flex justify-center mx-8 bg-pureGray text-darkGreen"},c.createElement("div",{className:"h-full w-full flex flex-col justify-center max-w-screen-lg mx-4 py-16 gap-4"},c.createElement("div",{className:"flex flex-col my-8 pb-8 gap-4 "},c.createElement("div",{className:"flex flex-col pb-8"},c.createElement("h1",{className:"text-xl font-medium text-darkGray text-center"},"Наша команда"),c.createElement("p",{className:"text-sm font-medium text-darkGray"},"Наша миссия - нормализовать заботу о психологическом здоровье. Сделать процесс проще, удобнее и доступнее для каждого.")))))))}},9704:function(e){e.exports=JSON.parse('{"layout":"constrained","images":{"fallback":{"src":"/static/07e09e15f402465cbd71cd12abb4d57a/27697/maria.jpg","srcSet":"/static/07e09e15f402465cbd71cd12abb4d57a/f726c/maria.jpg 125w,\\n/static/07e09e15f402465cbd71cd12abb4d57a/562b9/maria.jpg 250w,\\n/static/07e09e15f402465cbd71cd12abb4d57a/27697/maria.jpg 500w","sizes":"(min-width: 500px) 500px, 100vw"},"sources":[{"srcSet":"/static/07e09e15f402465cbd71cd12abb4d57a/33105/maria.webp 125w,\\n/static/07e09e15f402465cbd71cd12abb4d57a/31860/maria.webp 250w,\\n/static/07e09e15f402465cbd71cd12abb4d57a/d7111/maria.webp 500w","type":"image/webp","sizes":"(min-width: 500px) 500px, 100vw"}]},"width":500,"height":667}')},7277:function(e){e.exports=JSON.parse('{"layout":"constrained","images":{"fallback":{"src":"/static/21b423e7a66c3be4fe563146fdbfaba3/be5ed/face_2.jpg","srcSet":"/static/21b423e7a66c3be4fe563146fdbfaba3/8bb35/face_2.jpg 125w,\\n/static/21b423e7a66c3be4fe563146fdbfaba3/f505e/face_2.jpg 250w,\\n/static/21b423e7a66c3be4fe563146fdbfaba3/be5ed/face_2.jpg 500w","sizes":"(min-width: 500px) 500px, 100vw"},"sources":[{"srcSet":"/static/21b423e7a66c3be4fe563146fdbfaba3/d66e1/face_2.webp 125w,\\n/static/21b423e7a66c3be4fe563146fdbfaba3/e7160/face_2.webp 250w,\\n/static/21b423e7a66c3be4fe563146fdbfaba3/5f169/face_2.webp 500w","type":"image/webp","sizes":"(min-width: 500px) 500px, 100vw"}]},"width":500,"height":500}')},5064:function(e){e.exports=JSON.parse('{"layout":"constrained","images":{"fallback":{"src":"/static/5a39609f2259bb346aba3c574d9ec304/24f4c/dima_s.jpg","srcSet":"/static/5a39609f2259bb346aba3c574d9ec304/68974/dima_s.jpg 256w,\\n/static/5a39609f2259bb346aba3c574d9ec304/3c367/dima_s.jpg 512w,\\n/static/5a39609f2259bb346aba3c574d9ec304/24f4c/dima_s.jpg 1024w","sizes":"(min-width: 1024px) 1024px, 100vw"},"sources":[{"srcSet":"/static/5a39609f2259bb346aba3c574d9ec304/22bfc/dima_s.webp 256w,\\n/static/5a39609f2259bb346aba3c574d9ec304/d689f/dima_s.webp 512w,\\n/static/5a39609f2259bb346aba3c574d9ec304/67ded/dima_s.webp 1024w","type":"image/webp","sizes":"(min-width: 1024px) 1024px, 100vw"}]},"width":1024,"height":1024}')}}]);
//# sourceMappingURL=component---src-pages-about-js-8ee2c948a1d22077f559.js.map