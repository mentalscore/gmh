import * as React from "react";
import { Link, useStaticQuery, graphql } from "gatsby";
import Layout from "../components/layout";
import { StaticImage } from "gatsby-plugin-image";

export default function IndexPage() {
  return (
    <Layout>
      <HowItWorks />
    </Layout>
  );
}

function HowItWorks() {
  return(
    <div className="w-full h-full bg-pureGray px-8 flex flex-col justify-start items-center">
      <div className="h-full w-full max-w-screen-lg flex justify-center mb-32">
        <div className="h-full w-full flex flex-col justify-center ">
          <div>
            <h1 className="text-xl font-medium text-darkGray my-8">Как это работает</h1>
            <p className="text-sm font-medium text-darkGray mb-8">Mentalscore - Ваш персональный помощник в заботе о своём психологическом здоровье. Наши специалисты с опытом клинической работы интегрируют в приложение современные инструменты доказательной психологии, чтобы достичь максимального эффекта.</p>
          </div>
          <Panels />
        </div>
      </div>
    </div>
  )
}

function Panels() {
  return(
    <div className="flex flex-col lg:flex-row gap-16">
      <div className="w-full lg:w-72 ">
        <div className="w-full bg-lightGreen flex flex-col justify-center items-center rounded-lg">
          <div className="m-8">
            <StaticImage
              src="../images/score.svg"
              alt="Healhty Face"
              placeholder="darkGreen"
              className="w-36 lg:w-56 "
              height={500}
            />
          </div>
          <h2 className="text-xl">Анализ</h2>
          <p className="text-sm mt-4 mb-8 mx-4 ">Анализируйте своё самочувствие с помощью удобных инструментов в mentalscore. Оцените свой прогресс за день, неделю или месяц</p>
        </div>
      </div>

      <div className="w-full lg:w-72 ">
        <div className="w-full bg-lightGreen flex flex-col justify-center items-center rounded-lg">
          <div className="m-8">
            <StaticImage
              src="../images/pathway.svg"
              alt="Healhty Face"
              placeholder="darkGreen"
              className="w-42 m-8 lg:w-50 "
              height={500}
            />
          </div>
          <h2 className="text-xl">План</h2>
          <p className="text-sm mt-4 mb-8 mx-4">Выберете свой индивидуальный план из вариантов, разработанных нашими специалистами. Начните с того блока, который для Вас актуален в текущий момент</p>
        </div>
      </div>

      <div className="w-full lg:w-72 ">
        <div className="w-full bg-lightGreen flex flex-col justify-center items-center rounded-lg">
          <div className="m-8">
            <StaticImage
              src="../images/workshop.svg"
              alt="Healhty Face"
              placeholder="darkGreen"
              className="w-42 m-8 lg:w-52"
              height={500}
            />
          </div>
          <h2 className="text-xl">Воркшопы</h2>
          <p className="text-sm mt-4 mb-8 mx-4">Наши специалисты регулярно проводят воркшопы на разные темы: от синдрома самозванца до открытых отношений.</p>
        </div>
      </div>
    </div>
  )
}
