import * as React from "react";
import { Link, useStaticQuery, graphql } from "gatsby";
import Layout from "../components/layout";
import { StaticImage } from "gatsby-plugin-image";

export default function IndexPage() {
  let isOpen = false;
  return (
    <Layout >
      <Hero />
    </Layout>
  );
}

function Hero() {
  return(
    <div className="w-full h-full bg-darkGreen px-8 flex justify-center hero">
      <div className="h-full w-full max-w-screen-lg flex justify-center mx-8">
        <div className="w-full h-full grid grid-cols-2 grid-rows-[1fr_auto_auto_auto_auto_1fr] gap-2 text-pureGray">
          <div className="flex justify-start items-center">
            <StaticImage
              src="../images/face_1.jpg"
              alt="Healhty Face"
              placeholder="dominantColor"
              className="w-24 lg:w-64"
              height={500}
            />
          </div>
          <div className="flex justify-end items-center ">
            <StaticImage
              src="../images/face_2.jpg"
              alt="Healhty Face"
              placeholder="dominantColor"
              className="w-24 lg:w-64"
              height={500}
            />
          </div>
          <div className="col-span-2 flex justify-center items-center">
            <h3 className="text-lg lg:text-2xl">Мы заботимся о вашем</h3>
          </div>
          <div className="col-span-2 flex justify-center items-center">
            <h1 className="text-5xl lg:text-7xl">МЕНТАЛЬНОМ</h1>
          </div>
          <div className="col-span-2 flex justify-center items-center">
            <h1 className="text-5xl lg:text-7xl">ЗДОРОВЬЕ</h1>
          </div>
          <div className="col-span-2 flex justify-center items-center my-4">
            <Link to="/join">
              <button className="bg-green text-darkGray font-bold py-2 px-4 rounded lg:py-8 lg:px-16 lg:rounded-lg">
                Присоединяйтесь
              </button>
            </Link>
          </div>
          <div className="col-span-2 flex justify-end items-center">
            <StaticImage
              src="../images/face_3.jpg"
              alt="Healhty Face"
              placeholder="dominantColor"
              className="w-32 lg:w-96"
              width={300}
            />
          </div>
        </div>
      </div>
    </div>
  )
}
