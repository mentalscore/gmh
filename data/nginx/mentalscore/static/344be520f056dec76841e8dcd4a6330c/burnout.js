import * as React from "react";
import { Link, useStaticQuery, graphql } from "gatsby";
import Layout from "../../components/layout";
import { StaticImage } from "gatsby-plugin-image";

export default function IndexPage() {
  let isOpen = false;
  return (
    <Layout >
      <div className="bg-pureGray">
        <StaticImage
          src="../images/burnout.jpg"
          alt="Healhty Face"
          placeholder="dominantColor"
          className="w-full lg:w-124 "
          height={500}
        />
        <Article />
      </div>
    </Layout>
  );
}

export function Article() {
  return(
    <article className="prose lg:prose-lg mx-4 py-8">
      <h1>Эмоциональное выгорание</h1>
      <p>Все чаще в СМИ поднимается тема эмоционального выгорания. Если раньше люди стыдливо замалчивали свое переутомление и проблемы, с которыми сталкивается каждый, кто вовлечен в трудовые отношения, то сейчас люди охотнее делятся своим опытом эмоционального выгорания. Итак, что же это такое?</p>
      <p>Эмоциональное выгорание - синдром, при котором у человека развивается хроническое эмоциональное истощение. Чаще всего возникает у людей, занятых в сферах, где трудовая деятельность тесно связана с общением с людьми, а также с жестким, напряженным темпом работы. Но конечно, эмоциональному выгоранию подвержены люди любой специальности и рода деятельности.</p>
      <p>В 2021 году исследовательский холдинг Romir провел исследование, в ходе которого были опрошены 2500 респондентов из крупных российских городов. По данным опросов, около 64% россиян признались, что испытывали эмоциональное выгорание за последний год. Каждый пятый признался, что не испытывал этой проблемы. А на вопрос: &quot;Как Вы справляетесь с эмоциональным выгоранием?&quot;, респонденты ответили, что проводят время с друзьями или родственниками(48%), едят больше сладкого(33%) и пьют успокоительные препараты(25%). </p>
      <p>Как видно, выгорание - это не какая-то мелкая проблема. На самом деле, выгорание очень распространенно. ВОЗ считает синдром эмоционального выгорания официальным диагнозом и классифицировала его в МКБ-11(Международной классификации болезней)  под кодом QD85.</p>
      <h3>Как возникает выгорание?</h3>
      <ul>
        <li><strong>Переутомление.</strong> Одна из самых распространенных причин. Бесконечные часы напряженного труда, слишком много обязанностей, отсутствие сил и времени на другие сферы своей жизни - незамысловатый рецепт эмоционального истощения.</li>
        <li><strong>Отсутствие карьерных перспектив.</strong> Чувствуете, что переросли свою должность, что организация стоит на месте? Мало вероятно, что в таких условиях профессиональная деятельность будет приносить удовлетворение.</li>
        <li><strong>Токсичная рабочая среда.</strong> Несовпадение мировозренния, склоки, грубость коллег, сознательная травля и прочие &quot;радости&quot; такого коллектива еще никогда не были чем-то замечательным. Работать в таком месте очень тяжело и пагубно для ментального здоровья. Не говоря уже о продуктивности.</li>
        <li><strong>Однообразие.</strong> Убийца любого творческого начала. Выполнение однотипных задач каждую неделю, каждый день. Может легко заставить чувствовать себя беспомощным и ненужным.</li>
        <li><strong>Отсутствие результатов.</strong> Очень перекликается с предыдущим пунктом. Когда прикладываемые усилия не окупаются и не приводят к какому-либо результату, уверенность в своем профессионализме улетучивается.</li>
        <li><strong>Нарушение баланса</strong> между работой и личной жизнью. Мысленно жить на работе ничуть не лучше, чем жить на ней физически. Постоянное психологическое напряжение один из многих &quot;бонусов&quot; дисбаланса рабочего и личного.</li>
      </ul>
      <p>Симптомы выгорания.
      Как и любой официальный диагноз, выгорание имеет свои симптомы:</p>
      <ol>
        <li>Эмоциональное/физическое истощение или их комбинация;</li>
        <li>Головные боли и боли в животе, проблемы с кишечником;</li>
        <li>Отчуждение от работы и всего, что с ней связано (область деятельности, коллектив);</li>
        <li>Цинизм в отношении коллег, клиентов, работы в целом;</li>
        <li>Боли в груди;</li>
        <li>Трудности со сном или питанием;</li>
        <li>Снижение креативности;</li>
        <li>Депрессия;</li>
        <li>Общее снижение иммунитета.</li>
      </ol>
      <p>Выгорание отличается от стресса. Как депрессия, эмоциональное выгорание - всепоглощающее чувство. Человек ощущает апатию и опустошение. Теряется интерес к жизни. Привычные дела кажутся непосильной ношей, а ощущение бесполезности всего происходящего возникает все чаще и чаще.</p>
      <h3>Как помочь себе, если Вы чувствуете выгорание?</h3>
      <p>Несколько советом тем, кто с толкнулся с эмоциональным выгоранием, которые помогу смягчить состояние:</p>
      <p>Найдите смысл вне работы. Банально, но иногда нужно попробовать переключиться. Трудоголизм - не лучший соратник в борьбе за гармоничное и стабильное психологическое состояние. Попробуйте чаще встречаться с друзьями, найдите новое хобби или посвятите время старому. Даже начать читать книгу, которую долго откладывал - хорошая идея. Главное, не переусердствуйте. Не нужно бросать все силы на новое занятие. Достаточно небольших, но постоянных подвижек, которые помогут переключится. Наша цель - найти ценность за пределами Вашей трудовой деятельности.</p>
      <p>Заручитесь социальной поддержкой. Очень важно преодолеть изоляцию. Положительный отклик и дружеское плечо, помогут понять, что Вы не так одиноки, как Вам может казаться. Попробуйте поддержать разговор коллег, соберитесь с друзьями. Обсудите свои чувства и переживания с близким человеком.</p>
      <p>Смените работу. Да, это крайне радикальный совет и применять его сгоряча не стоит. Он подойдет тем, чьё выгорание связано с внешними причинами, вроде токсичного коллектива или отсутствия развития компании. Но и здесь много нюансов. Стоит выписать все плюсы и минусы вашей должности и понять, что превалирует. А также постараться трезво посмотреть на себя: хватит ли сейчас у Вас эмоциональных и физических сил на такой шаг? Если истоки выгорания лежат в человеке, то смена места, к сожалению, не поможет. Проблема вскоре вернется вновь.</p>
    <p>Обратитесь к специалисту. Грамотный псилого поможет соорентировать Вас, и дать необходимую поддержку в этот непростой для человека эпизод.</p>
    <p><strong>Эмоциональное выгорание</strong> - глобальная проблема, которой подвержены миллионы людей. Не стоит относиться к ней легкомысленно. Если Вы обнаружили у себя симптомы выгорания, то не пускайте это состояние на самотек.</p>
    </article>
  );
}
