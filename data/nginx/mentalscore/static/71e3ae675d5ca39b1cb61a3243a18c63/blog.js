import * as React from "react";
import { Link, useStaticQuery, graphql } from "gatsby";
import Layout from "../components/layout";
import { StaticImage } from "gatsby-plugin-image";

export default function IndexPage() {
  let isOpen = false;

  return (
    <Layout >
      <div className="w-full h-full bg-pureGray px-8 flex flex-col justify-start items-center">
        <div className="h-full w-full max-w-screen-lg flex justify-center mb-32">
          <div className="h-full w-full flex flex-col justify-center">
            <div className="flex flex-col border-b border-lightGreen my-8 pb-8 gap-8">
              <h1 className="text-xl font-medium text-darkGray ">Блог MentalScore</h1>
              <p className="text-sm font-medium text-darkGray">это блог о самосовершенствовании, посвященный психическому здоровью, в котором особое внимание уделяется личной продуктивности, мотивации и образованию. Блог охватывает любые материалы, которые помогут людям жить более счастливой и здоровой жизнью.</p>
            </div>

            <div className="flex flex-col gap-4">

              <Link to="/blog/sindrom-samozvanca">
                <div className="flex flex-col gap-4">
                  <StaticImage
                    src="../images/sindrom.jpg"
                    alt="Healhty Face"
                    placeholder="dominantColor"
                    className="w-full rounded-lg lg:w-124 "
                    height={500}
                  />
                  <h3 className="text-darkGreen text-4xl">Синдром самозванца </h3>
                  <p className="text-darkGreen text-sm">Бывало ли у Вас чувство, что Вы недостаточно компетентны в профессии? Или Ваши близкие и партнер будто бы совсем Вас не знают? Сомневались ли Вы в своих навыках и достижениях? Если Вы ответили для себя "да", то тогда ...</p>
                  <p className="text-lightGreen">ЧИТАТЬ ДАЛЕЕ </p>
                </div>
              </Link>
            </div>

            <div className="flex flex-col border-t border-lightGreeg ap-4 pt-8 mt-8">
              <Link to="/blog/burnout">
                <div className="flex flex-col gap-4">
                  <StaticImage
                    src="../images/burnout.jpg"
                    alt="Burn out"
                    placeholder="dominantColor"
                    className="w-full rounded-lg lg:w-124 "
                    height={500}
                  />
                  <h3 className="text-darkGreen text-4xl">Эмоциональное выгорание</h3>
                  <p className="text-darkGreen text-sm">Все чаще в СМИ поднимается тема эмоционального выгорания. Если раньше люди стыдливо замалчивали свое переутомление и проблемы, с которыми сталкивается каждый, кто ...</p>
                  <p className="text-lightGreen">ЧИТАТЬ ДАЛЕЕ </p>
                </div>
              </Link>
            </div>

          </div>
        </div>
      </div>
    </Layout>
  );
}
