"use strict";(self.webpackChunkmental_score=self.webpackChunkmental_score||[]).push([[985],{1966:function(e,l,t){t.r(l),t.d(l,{default:function(){return c}});var a=t(7294),r=(t(1597),t(8781));var n=a.forwardRef((function(e,l){return a.createElement("svg",Object.assign({xmlns:"http://www.w3.org/2000/svg",viewBox:"0 0 20 20",fill:"currentColor","aria-hidden":"true",ref:l},e),a.createElement("path",{fillRule:"evenodd",d:"M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z",clipRule:"evenodd"}))}));function c(){return a.createElement(r.ZP,null,a.createElement(m,null))}var s=["Гибкие программы психологического оздоровления, созданные экспертами MentalScore","Неограниченный доступ к мероприятиям, которые проводят наши специалисты","Постоянный трекинг психического здоровья","Обширная библиотека инструментов для изучения вашего психического здоровья"];function m(){return a.createElement("div",{className:"w-full h-full bg-darkGreen px-8 flex justify-center hero"},a.createElement("div",{className:"h-full w-full max-w-screen-lg flex justify-center"},a.createElement("div",{className:"w-full h-full flex flex-col gap-8 items-center text-pureGray mt-12"},a.createElement("div",{className:"flex flex-col gap-4"},a.createElement("div",null,a.createElement("h1",{className:"text-2xl text-center"},"Воспользуйтесь расширенными возможностями")),a.createElement("div",null,a.createElement("p",{className:"text-sm text-center"},"Сделайте душевное благополучие неотъемлемой частью своей жизни"))),a.createElement("div",{className:"mb-4 w-full flex flex-col gap-4 pb-8 border-b border-lightGreen"},a.createElement("div",{className:"w-full "},a.createElement("a",{href:"/app/#/register"},a.createElement("button",{className:"w-full border border-pureGray text-pureGray font-bold py-4 px-4 rounded-lg lg:py-8 lg:px-16 lg:rounded-lg"},"500 ₽ в месяц"))),a.createElement("div",{className:"w-full flex flex-col gap-2"},a.createElement("a",{href:"/app/#/register"},a.createElement("button",{className:"w-full bg-green text-darkGray font-bold py-4 px-4 rounded-lg lg:py-8 lg:px-16 lg:rounded-lg"},"3000 ₽ в год")),a.createElement("p",{className:"text-sm text-center"},"6 месяцев выгода"))),a.createElement(u,{benefitList:s}),a.createElement("div",{className:"my-8"},a.createElement("p",{classNmae:"text-xs "},"Отмена подписки в любое время")),a.createElement("div",null))))}function u(e){e.benifits;var l=s.map((function(e){return a.createElement("div",{className:"flex gap-2 text-sm"},a.createElement("div",{className:"h-6 w-6"},a.createElement(n,{className:"h-6 w-6"})),a.createElement("p",null,e))}));return a.createElement("div",{className:"flex flex-col gap-4 text-sm"},l)}}}]);
//# sourceMappingURL=component---src-pages-join-js-86a14d91db72df182ac2.js.map